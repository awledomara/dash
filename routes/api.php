<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
//Addresses


Route::get('practices/{practice}/addresses','PracticeAddressController@index');
Route::get('practices/{practice}/addresses/{address}','PracticeAddressController@show');
Route::post('practices/{practice}/addresses','PracticeAddressController@store');
Route::post('practices/{practice}/addresses/{address}','PracticeAddressController@update');
Route::delete('practices/{practice}/addresses/{address}','PracticeAddressController@destroy');


Route::get('practices/{practice}/documents','PracticeDocumentController@index');
Route::post('practices/{practice}/documents','PracticeDocumentController@store');
Route::post('practices/{practice}/documents/{document}','PracticeDocumentController@update');
Route::delete('practices/{practice}/documents/{document}','PracticeDocumentController@destroy');

Route::get('doctors/{doctor}/documents','DoctorDocumentController@index');
Route::post('doctors/{doctor}/documents','DoctorDocumentController@store');
Route::post('doctors/{doctor}/documents/{document}','DoctorDocumentController@update');
Route::delete('doctors/{doctor}/documents/{document}','DoctorDocumentController@destroy');


Route::get('jobs/{job}/fees','JobFeeController@index');
Route::post('jobs/{job}/fees','JobFeeController@store');
Route::put('jobs/{job}/fees/{job_fee}','JobFeeController@update');
Route::put('jobs/{job}/fees/{fee}/status','JobFeeController@updateStatus');
Route::resource('events','Api\EventController');


//dataTables
Route::get('datatables/practices','DataTables\PracticeDataTableController@index')->name('datatable.practices.index');
Route::get('datatables/practices/{practice}/jobs','DataTables\PracticeDataTableController@jobs')->name('datatable.practices.jobs');
Route::get('datatables/doctors','DataTables\DoctorDataTableController@index')->name('datatable.doctors.index');
Route::get('datatables/services','DataTables\ServiceAreaDataTableController@index')->name('datatable.services.index');
Route::get('datatables/jobs','DataTables\JobsDataTableController@index')->name('datatable.jobs.index');


//MicroServices

Route::get('search/practices','MicroServices\PracticeController@search')->name('microservices.practices.search');
Route::get('search/doctors','MicroServices\DoctorsSearchController@search')->name('microservices.doctors.search');
Route::get('search/jobs','MicroServices\JobsSearchController@search')->name('microservices.jobs.search');

Route::get('search/practices/{practice}/services','MicroServices\PracticeController@services');

Route::get('address_books','MicroServices\AddressBookController@index')->name('microservices.address_books.index');
Route::post('address_books','MicroServices\AddressBookController@store')->name('microservices.address_books.store');
Route::post('address_books/{address_book}','MicroServices\AddressBookController@update')->name('microservices.address_books.update');
Route::get('address_books/{address_book}/delete','MicroServices\AddressBookController@destroy')->name('microservices.address_books.destroy');

//Email Templates

Route::get('templates','MicroServices\EmailTemplateController@search')->name('microservices.email_templates.search');
Route::get('templates/{template}','MicroServices\EmailTemplateController@show')->name('microservices.email_templates.show');



Route::get('recipients-2nd','MicroServices\Recipient2ndController@search')->name('microservices.recipients-2nd.search');

Route::get('recipients','MicroServices\RecipientController@search')->name('microservices.recipients.search');
Route::post('recipients','MicroServices\RecipientController@store')->name('microservices.recipients.store');

//Ajax Views
Route::get('ajax_jobs/{job}/applications','AjaxViews\JobApplicationController@index')->name('jobs.applications');
Route::get('ajax_practices','AjaxViews\PracticeController@search')->name('practices.jobs_link');
Route::group(['prefix' => 'calendar'], function () {
    Route::get('/', 'Api\CalendarAPIController@list');
    Route::patch('/{id}', 'Api\CalendarAPIController@update');
    Route::delete('/{id}', 'Api\CalendarAPIController@delete');
    Route::post('/{id}', 'Api\CalendarAPIController@duplicate');
});


Route::post('file-upload','MicroServices\FileUploadController@upload')->name('microservices.fileupload');

Route::get('/applications/{job_application}/accept','JobApplicationController@acceptApplication')->name('application.accept');

Route::get('jobs/{job}/apply','JobApplicationController@emailApply')->name('email.job.apply');
