<?php


use App\User;
use Illuminate\Support\Facades\Notification;
use Spatie\CalendarLinks\Link;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('locale')->group(function(){

    Route::get('welcome/{lang}','LocalController@index');
    Route::get('/', "WelcomeController@index");



    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/dashboard', function(){
        return view("layouts.dashboard");
    });

    Route::post('temp','FileUploaderController@store')->name('files.store');
    Route::get('files/{document}','FileDownloaderController@download')->name('files.download');
    Auth::routes(['verify' => true]);
    Route::group(["prefix"=>"admin_102019_xyz"],function(){
        Auth::routes(['verify' => true]);
    });

    Route::get('jobs/{job}/apply_form','JobApplicationController@show')->name('jobs.apply_form_for_doctors');
    Route::post('jobs/{job}/apply','JobApplicationController@apply')->name('jobs.apply_for_doctors');

    Route::group(["middleware"=>["auth"]],function (){
        //For doctor portal




    });
    Route::group(['middleware'=>['auth','Admin'], "prefix"=>"admin_102019_xyz"],function(){

        Route::get('/',"HomeController@index");//TODO: Check this route
        Route::get('/dash',"AdminDashboardController@index")->name('admin.dashboard');

        //jobs

        Route::get('jobs/{job}/notifications_form','JobNotificationController@show')->name('jobs.notifications_form');

        Route::get('job_applications','JobApplicationController@index')->name('jobs.applications');

        Route::get('jobs/{job}/apply_form','JobApplicationController@show')->name('jobs.apply_form');
        Route::resource('jobs','JobController');
        Route::get('create_jobs','JobController@create')->name('jobs.create');
        Route::get('list_jobs','JobController@index')->name('jobs.list');
        Route::get('overview_jobs','JobController@index')->name('jobs.overview');//TODO: check if we need this route, or change it

        Route::post('jobs/{job}/send_notification','JobNotificationController@notifyDoctors')->name('jobs.send_notifications');
        Route::post('practices/{practice}/send_notification','JobNotificationController@sendVerificationToPractice')->name('jobs.send_notification_to_practice');


        //practices
        route::get('practice_jobs_view','JobNotificationController@verif_form')->name('practices.jobs_view');

        route::get('practices/{practice}/jobs/{job}/confirm','PracticeMissionConfirmController@confirm_mission')->name('practices.confirm_mission');
        route::get('practices/{practice}/confirm_many_missions','PracticeMissionConfirmController@confirm_missions')->name('practices.confirm_missions');

        Route::resource('practices','PracticeController');
        route::get('create_practice','PracticeController@create')->name('practices.create');
        route::get('list_practices','PracticeController@index')->name('practices.list');




        //doctors

        Route::get('doctors_notifications','DoctorsNotificationController@notifications')->name('doctors.notifications.view');
        Route::post('doctors_notifications','DoctorsNotificationController@sendNotifications')->name('doctors.notifications.send');


        Route::resource('doctors','DoctorController');
        route::get('create_doctor','DoctorController@create')->name('doctors.create');
        //services
        Route::post('services/{service}/clone','ServiceAreaController@clone')->name('services.clone');

        Route::resource('services','ServiceAreaController');
        route::get('create_service','ServiceAreaController@create')->name('services.create');
        route::get('list_services','ServiceAreaController@index')->name('services.list');
        Route::resource('users','UserManagerController');


        Route::resource('specialities','SpecialityController');

        Route::resource('recipients','RecipientsController');
        Route::resource('recipients-2nd','Recipients2ndController');

        Route::resource('hotels','HotelController');

        Route::resource('facilities','FacilityController');


        Route::get('calendar','CalendarController@index')->name('calendar.view');

        Route::get('templates','EmailTemplateController@index')->name('templates.index');
        Route::get('create_template','EmailTemplateController@create')->name('templates.create');
        Route::get('edit_template/{template}','EmailTemplateController@edit')->name('templates.edit');
        Route::post('store_template','EmailTemplateController@store')->name('templates.store');
        Route::post('update_template','EmailTemplateController@update')->name('templates.update');
        Route::get('templates/{template}/files/{template_file}/delete','EmailTemplateController@deleteFile')->name('templates.files.delete');


        Route::get('send_email_form','SendEmailController@show')->name('email.show');
        Route::post('send_email','SendEmailController@send')->name('email.send');

        Route::get('send_service_email_form','SendServiceEmailController@show')->name('service_email.show');
        Route::post('send_service_email','SendServiceEmailController@send')->name('service_email.send');


        Route::get('jobs-fulfillment','JobFulfillmentController@index')->name('jobs.fulfillment');
        Route::get('jobs-fulfillment/{job}','JobFulfillmentController@show')->name('jobs.fulfillment.show');
        Route::get('jobs-fulfillment/{job}/send-pdf','JobFulfillmentController@sendPDF')->name('jobs.fulfillment.send-pdf');

        Route::get('sent-emails','SentEmailsController@index')->name('sent-emails.index');

        Route::get('practice_jobs_new','JobNotificationController@verificationNew')->name('practices.verification_new');
        Route::post('practice_jobs_new','JobNotificationController@verificationNewSend')->name('practices.verification_new.send');

    });




});


Route::get('links',function (){

return \App\Models\Job::with(["applications"])->paginate(10);

    $from = DateTime::createFromFormat('Y-m-d H:i', '2018-02-01 09:00');
    $to = DateTime::createFromFormat('Y-m-d H:i', '2018-02-01 18:00');

    $link = Link::create('Sebastian\'s birthday', $from, $to)
        ->description('Cookies & cocktails!')
        ->address('Samberstraat 69D, 2060 Antwerpen');

// Generate a link to create an event on Google calendar
    $google= $link->google();

// Generate a link to create an event on Yahoo calendar
    $yahoo= $link->yahoo();

// Generate a link to create an event on outlook.com calendar
    $outlook= $link->webOutlook();

// Generate a data uri for an ics file (for iCal & Outlook)
    $ics= $link->ics();
    return compact('google','yahoo','outlook','ics');
});
