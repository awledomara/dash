<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{
   public $fillable=[
       'email','first_name','last_name'
   ];
}
