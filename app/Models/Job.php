<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{
    use SoftDeletes;
    protected $table="missions";

    const PENDING=0;//verification was send to Practice and wait the confirmation
    const Verified=1;// Practice verified the mission
    const Assigned=2;// Job Was Assigned to doctor
    protected $fillable=[
        'start_date',
        'end_date',
        'time_frames',
        'tags',
        'created_by',
        'practice_id',
        'service_area_id',
        'notes',
        'additional_remuneration',
        'ab',
        'status',
        'verified_at',
        'verification_sent_at',
        'order'
    ];
    protected $casts = [
        'tags' => 'array',
        'time_frames'=>'array',
        'start_date'=>"datetime",
        "end_date"=>'datetime',
        'verified_at'=>"datetime",
        'verification_sent_at'=>"datetime"

    ];



    public function service_area(){
        return $this->belongsTo(ServiceArea::class);
    }


    public function practice(){
        return $this->belongsTo(Practice::class);
    }
    public function doctor(){
        return $this->belongsTo(Doctor::class);
    }
    public function fees(){
        return $this->hasMany(JobFee::class);
    }
    public function applications(){
        return $this->hasMany(JobApplication::class,'mission_id');
    }
    public function total_hours(){
        return $this->start_date->diffInHours($this->end_date);
    }
}
