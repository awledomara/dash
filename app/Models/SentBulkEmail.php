<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SentBulkEmail extends Model
{

    public function template()
    {
        return $this->belongsTo(EmailTemplate::class, 'template_id');
    }
}
