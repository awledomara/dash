<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobFee extends Model
{
    const PENDING=0;
    const ACCEPTED=1;
    const REJECTED=3;
    protected $fillable=[
        'from','per_hour','job_id','status'
    ];
    public function job(){
        return $this->belongsTo(Job::class);
    }


}
