<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $fillable=[
        'content','subject','name'
    ];


    public function files(){
        return $this->hasMany(EmailTemplateFile::class);
    }
}
