<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplateFile extends Model
{
    protected $fillable = [
        'email_template_id',
        'path',
        'comment',
        'original_name'
    ];
}
