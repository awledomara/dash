<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorDocument extends Model
{

   public $timestamps=false;
    public function document(){
        return $this->belongsTo(Document::class);
    }
}
