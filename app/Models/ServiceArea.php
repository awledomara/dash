<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceArea extends Model
{
    use SoftDeletes;
    protected $fillable=[
        'name','designation','notes','internal_name','type','internal_name_lst',
        'phone','phone_contact_person','contact_person','email_lst','phone_lst','fax_lst',
        'email_kv','phone_kv','fax_kv','post_code'
    ];

    protected $casts=[
        'type'=>"int"
    ];
    /*public function contactPerson(){
        return $this->belongsTo(Person::class);
    }
    public function address(){
        return $this->belongsTo(Address::class);
    }
    */
}
