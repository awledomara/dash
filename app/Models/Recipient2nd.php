<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipient2nd extends Model
{
   public $fillable=[
       'email','first_name','last_name'
   ];
}
