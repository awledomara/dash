<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class JobApplication extends Model
{
     protected $fillable=[
         "doctor_id","mission_id"
     ];

     public function mission(){
         return $this->belongsTo(Job::class,'mission_id');
     }
    public function doctor(){
        return $this->belongsTo(Doctor::class);
    }
     public $timestamps=true;
}
