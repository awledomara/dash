<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PracticeServiceArea extends Model
{

     protected $table="practice_service_areas";
     protected $fillable=[
         "practice_id","service_area_id"
     ];

     public function service_area(){
         return $this->belongsTo(ServiceArea::class);
     }

     public $timestamps=false;
}
