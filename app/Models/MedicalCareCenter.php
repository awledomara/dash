<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedicalCareCenter extends Model
{
    protected $fillable=[
            "mcc_name",'mcc_contact_name','mcc_phone','mcc_street_number','mcc_city','mcc_post_office','practice_id','mcc_fax','mcc_email'
    ];
}
