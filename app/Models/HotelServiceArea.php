<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelServiceArea extends Model
{
   public $timestamps=false;

   public function service(){
       return $this->belongsTo(ServiceArea::class,'service_area_id');
   }
    public function hotel(){
        return $this->belongsTo(Hotel::class);
    }

}
