<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HotelAddress extends Model
{
    public $timestamps=false;
    public function address(){
        return $this->belongsTo(Address::class);
    }
    public function hotel(){
        return $this->belongsTo(Hotel::class);
    }
}
