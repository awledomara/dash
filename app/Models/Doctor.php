<?php

namespace App\Models;

use App\Interfaces\DataTable;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Doctor extends Model implements DataTable
{
    use SoftDeletes;

    protected $fillable=[
            'title','grade','secondary_email','phone1','phone2','user_id','service_area_id','speciality_id','notes','address_id'
    ];

    public function address(){
        return $this->belongsTo(Address::class,'address_id');
    }
    public function addresses(){
        return $this->hasMany(DoctorAddress::class);
    }
    public function documents(){
        return $this->hasMany(DoctorDocument::class);
    }
    public function speciality(){
        return $this->belongsTo(Speciality::class);
    }
    public function facility(){
        return $this->belongsTo(Facility::class);
    }
    public function serviceArea(){
        return $this->belongsTo(ServiceArea::class,'service_area_id');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }


    public function columns()
    {
        // 'title','grade','secondary_email','phone1',
        //'phone2','user_id','service_area_id','speciality_id','facility_id','notes','address_id'
        return [
            [
                "data"=>"user.first_name"
            ],
            [
                "data"=>"user.last_name"
            ],
            [
                "data"=>"user.email"
            ],
            [
                "data"=>"user.role.name","display"=>"Role"
            ],
            [
                "data"=>"user.first_name"
            ]

        ];
    }

    public function sortable()
    {
        // TODO: Implement sortable() method.
    }
}
