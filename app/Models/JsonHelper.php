<?php

namespace App\Models;


use Psy\Util\Json;

class JsonHelper
{
    public static function entityNotFound($message){
         return [
             'success'=>false,
             'statusCode'=>404,
             'message'=>$message,
             'data'=>[]
         ];
    }

    public static function data($data,$message=''){
        return [
            'success'=>true,
            'statusCode'=>200,
            'message'=>$message,
            'data'=>$data
        ];
    }

}
