<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PracticeDocument extends Model
{
    protected $fillable=[
        "document_id","practice_id"
    ];
  public $timestamps=false;
  public function practice(){
      return $this->belongsTo(Practice::class);
  }
    public function document(){
        return $this->belongsTo(Document::class);
    }
}
