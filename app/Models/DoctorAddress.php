<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorAddress extends Model
{
    public $timestamps=false;
    protected $fillable=[
        'doctor_id','address_id'
    ];

    public function address(){
        return $this->belongsTo(Address::class);
    }
}
