<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
   protected $fillable=[
       'value'
   ];
   public $timestamps=false;
}
