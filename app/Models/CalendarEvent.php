<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
//CalendarEvent
class CalendarEvent extends Model
{
    protected $fillable=[
      'title','start','end','description','allDay','backgroundColor','textColor'
    ];
  protected $hidden=[
    'created_at','updated_at','mission_id'
  ];
    protected $casts=[

        'editable'=>"boolean",
        'allDay'=>"boolean"
    ];


    public function job(){
        return $this->belongsTo(Job::class);
    }
    public function doctor(){
        return $this->belongsTo(Doctor::class);
    }

}
