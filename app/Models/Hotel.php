<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Model
{
    use SoftDeletes;
    protected $fillable=[
           'name', 'website','email','primary_phone','secondary_phone','fax'
   ];

    public function addresses(){
        return $this->hasMany(HotelAddress::class);

    }
    public function services(){
        return $this->hasMany(HotelServiceArea::class);

    }
}
