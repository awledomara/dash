<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PracticeAddress extends Model
{
    public $timestamps=false;
    protected $fillable=[
            'practice_id','address_id'
    ];

    public function address(){
        return $this->belongsTo(Address::class);
    }
    public function format(){
        return $this->address->format();
    }
}
