<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable=[
        'company_name','postal_code','city','country','street'
    ];

    public function format(){
        return $this->street.' '.$this->city.', '.$this->postal_code;
    }

}
