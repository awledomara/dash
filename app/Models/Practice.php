<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Practice extends Model
{
    use SoftDeletes;
    protected $fillable=[
           'pay_period','iban','monthly_fee','owner','debit_authorization', 'name','notes',

           'secondary_phone','primary_phone','fax','primary_email','secondary_email','service_area_id','facility_id','speciality_id','in_mvz',

          'first_name','last_name','title','grade'
    ];

    public function fullName(){
        return $this->first_name.' '.$this->last_name;
    }
    public function mcc(){
        return $this->hasOne(MedicalCareCenter::class);
    }
    public function addresses(){
        return $this->hasMany(PracticeAddress::class);
    }
    public function documents(){
        return $this->hasMany(PracticeDocument::class);
    }

    public function contact_person(){
        return $this->belongsTo(Person::class);
    }
    //TODO : DELETE THIS UNUSED METHOD

    public function serviceArea(){
        return $this->belongsTo(ServiceArea::class);
    }
    public function speciality(){
        return $this->belongsTo(Speciality::class);
    }
    public function facility(){
        return $this->belongsTo(Facility::class);
    }

    public function service_areas(){
        return $this->belongsToMany(ServiceArea::class,"practice_service_areas",'practice_id','service_area_id');
    }
    //TODO: will replace the Person
    public function responsible(){
        return $this->belongsTo(Doctor::class);
    }
}
