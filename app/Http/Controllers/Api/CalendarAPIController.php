<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Models\Job;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CalendarAPIController extends Controller
{
    public function list(Request $request)
    {
        $from = date('Y-m-d', $request->get('from'));
        $to = date('Y-m-d', $request->get('to'));
//        die(json_encode([$from, $to]));

        /** @var Collection $missions */
        $missions = Job
            ::where(function ($query) use ($from, $to) {
                $query->where(DB::raw('date (`start_date`)'), '>=', $from);
                $query->where(DB::raw('date (`start_date`)'), '<=', $to);
            })
            ->orWhere(function ($query) use ($from, $to) {
                $query->where(DB::raw('date (`end_date`)'), '>=', $from);
                $query->where(DB::raw('date (`end_date`)'), '<=', $to);
            })
//            ::orWhere('start_date', '>=', $from)
//            ->orWhere('end_date', '>=', $from)
//            ->orWhere('start_date', '<=', $to)
//            ->orWhere('end_date', '<=', $to)
            ->get();

        $missions->each(function ($item){
            $item->practice ;
        });

        return response()->json($missions);
    }

    public function update(Request $request, $id)
    {
        $order = $request->query('order');
        $startDate = date('Y-m-d', $request->query('start-time'));
        $endDate = date('Y-m-d', $request->query('end-time'));

        try {
            $existing = Job::where(DB::raw('date (`start_date`)'), $startDate)->where('order', $order)->count();
            if ($existing > 0)
                return response()->json(['success' => false, 'error' => 'existing mission in the target order']);

            $existing = Job::where(DB::raw('date (`end_date`)'), $startDate)->where('order', $order)->count();
            if ($existing > 0)
                return response()->json(['success' => false, 'error' => 'existing mission in the target order']);

            $existing = Job::where(DB::raw('date (`start_date`)'), $endDate)->where('order', $order)->count();
            if ($existing > 0)
                return response()->json(['success' => false, 'error' => 'existing mission in the target order']);


            Job::where('id', $id)->update(['order' => $order]);
            return response()->json(['success' => true]);
        } catch (\Exception $exception) {
            return response()->json(['success' => false, 'error' => $exception->getMessage()]);
        }

    }

    public function delete(Request $request, $id)
    {
        Job::where('id', $id)->update(['deleted_at' => date('Y-m-d H:i:s')]);
        return response()->json(['success' => true]);
    }

    public function duplicate(Request $request, $id)
    {
        $mission = Job::find($id);
        $order = $request->query('order');

        $newMission = $mission->replicate();
        $newMission->order = $order;
        $newMission->save();

        return response()->json(['success' => true]);
    }
}
