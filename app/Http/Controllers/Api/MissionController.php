<?php

namespace App\Http\Controllers\Api;

use App\Models\Job;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MissionController extends Controller
{
    public function index(Request $request){
        $from = date('Y-m-d', $request->get('from'));
        $to = date('Y-m-d', $request->get('to'));
//        die(json_encode([$from, $to]));

        /** @var Collection $missions */
        $missions = Job
            ::where(function ($query) use ($from, $to) {
                $query->where(DB::raw('date (`start_date`)'), '>=', $from);
                $query->where(DB::raw('date (`start_date`)'), '<=', $to);
            })
            ->orWhere(function ($query) use ($from, $to) {
                $query->where(DB::raw('date (`end_date`)'), '>=', $from);
                $query->where(DB::raw('date (`end_date`)'), '<=', $to);
            })
            ->get();

        return $missions;
    }
}
