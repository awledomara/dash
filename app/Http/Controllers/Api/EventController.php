<?php

namespace App\Http\Controllers\Api;

use App\Models\CalendarEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    public function index(Request $request){

        return CalendarEvent::whereBetween('created_at',[$request->input('start'),$request->input('end')])->get();
    }
    public function store(Request $request){
        $attributes=$request->only('title','description','start','end','allDay','backgroundColor','textColor');
        $event=CalendarEvent::create($attributes);
        return $event;
    }
    public function update(Request $request,CalendarEvent $event){
        $attributes=$request->only('title','description','start','end','allDay','backgroundColor','textColor');
        $event->update($attributes);
        return $event;
    }
}
