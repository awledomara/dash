<?php

namespace App\Http\Controllers;

use App\Jobs\NotifyDoctors;
use App\Jobs\PracticesVerification;
use App\Mail\MissionVerification;
use App\Models\Job;
use App\Models\Practice;
use App\Models\PracticeServiceArea;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Queue\Queue;
use Illuminate\Support\Facades\Mail;
use PDF;

class JobNotificationController extends Controller
{
    public function show(Job $job, Request $request)
    {

        return view("jobs.notifications.show", compact('job'));


    }

    public function verif_form(Request $request)
    {

        $practices = Practice::all(['id', 'first_name', 'last_name', 'primary_email']);
        $jobs = Job::where('verified', '=', 0);
        $practice = null;
        if ($request->input("p_id", null)) {
            $jobs = $jobs->where("practice_id", $request->input("p_id"));

            $practice = Practice::find($request->input('p_id'));
        }
        if ($request->input("s_id", null)) {
            $jobs = $jobs->where("service_area_id", $request->input("s_id"));
        }

        $jobs = $jobs->with(["service_area" => function ($q) {
            $q->select('id', 'name');
        }])->paginate(15)
            ->appends($request->except("page"));

        return view("jobs.verify", compact('practices', 'jobs', 'practice'));
    }


    public function sendVerificationToPractice(Practice $practice, Request $request)
    {
        $this->validate($request, [
            'jobs_ids' => "array|required",
            "jobs.*" => "exists:missions,id"
        ]);


        Mail::to($practice->primary_email)->send(new MissionVerification($practice, $request->input("jobs_ids")));
        if (Mail::failures()) {
            return redirect()->back()->with(['message' => "We are not able to send the email at this time"]);
        }
        Job::whereIn('id', $request->input("jobs_ids"))->update([
            'verification_sent_at' => Carbon::now()
        ]);

        return redirect()->back()->with(['message' => "Verification was sent successfully to practice $practice->primary_email"]);
    }

    public function notifyDoctors(Job $job, Request $request)
    {


        NotifyDoctors::dispatch($job, $request->input('doctors_ids', []));
        return redirect()->back()->with(['message' => 'Emails was sent successfully to doctors']);
    }

    public function verificationNew()
    {
        return view('jobs.verification-new');
    }

    public function verificationNewSend(Request $request)
    {
        $this->validate($request,[
            'content'=>"required"
        ]);

        $name = date('Y-m-d_H-i-s_') . rand(0,9999) . '_verification_new.pdf';
        $path = storage_path('app/public/pdf/practices/' . $name);

        $pdf= PDF::loadHTML($request->get('content'));

        $pdf->save($path);

        PracticesVerification::dispatch($path);

        return redirect()->back()->with(['message' => "The invoice has been sent."]);
    }
}

