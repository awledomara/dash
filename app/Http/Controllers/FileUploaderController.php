<?php

namespace App\Http\Controllers;

use App\TempFile;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Psy\Util\Json;

class FileUploaderController extends Controller
{
    public function store(Request $request){
        if($request->hasFile('file')){

            $file=$request->file('file');
            $path= $file->store('temp');

            $temp_file = TempFile::create([
               'path'=>storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.str_replace(["/","\\"],DIRECTORY_SEPARATOR,$path),
               'expires'=>Carbon::now()->addMinutes(20)
               ]);

            return $temp_file;

        }

        return Json::encode($request->hasFile('file'));
    }
}
