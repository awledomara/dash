<?php

namespace App\Http\Controllers;

use App\Models\Speciality;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SpecialityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $specialities = Speciality::paginate(10);

        return view('specialities.index', compact('specialities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('specialities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>"required",
        ]);
        Speciality::create($request->only('name'));
        return redirect()->route('specialities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return view('specialities.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Speciality $speciality
     * @return Response
     */
    public function edit(Speciality $speciality)
    {
        return view('specialities.edit',compact('speciality'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Speciality $speciality,Request $request)
    {

        $this->validate($request,[
            'name'=>"required",
        ]);
//        $speciality=Speciality::findOrFail($request->input("speciality_id"));
        $speciality->name = $request->input("name");
        $speciality->save();

        return redirect()->route('specialities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Speciality $speciality
     * @return void
     */
    public function destroy(Speciality $speciality)
    {
        $speciality->delete();
        return redirect()->back()->with(['message'=>"Speciality deleted"]);
    }
}
