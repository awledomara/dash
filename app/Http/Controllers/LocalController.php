<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\App;
use League\Flysystem\Config;

class LocalController extends Controller
{
   public function index($lang){
     session(['locale'=>$lang]);

     return redirect()->back();
   }
}
