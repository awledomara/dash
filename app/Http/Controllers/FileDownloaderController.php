<?php

namespace App\Http\Controllers;

use App\Models\Document;

use Illuminate\Http\Request;


class FileDownloaderController extends Controller
{
   public function download(Request $request,Document $document){


       $key=$request->input('k');//we will configure that later to  protect files

       if(file_exists($document->path)){
           return response()->download($document->path);
       }

       return redirect()->back()->with(['message'=>"File Not Found",'type'=>'error']);
   }
}
