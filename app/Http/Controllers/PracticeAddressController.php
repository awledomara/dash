<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\JsonHelper;
use App\Models\Practice;
use Illuminate\Http\Request;

class PracticeAddressController extends Controller
{
    public function index(Practice $practice){
        return $practice->addresses()->with('address')->get();
    }
    public function show(Practice $practice,Address $address){
         if($practice->addresses()->where('address_id',$address->id)->count('*')){
             return $address;
         }
         return JsonHelper::entityNotFound('Address Not Found');
    }
    public function store(Practice $practice,Request $request){

        $address=Address::create($request->all());
        $practice->addresses()->create(['address_id'=>$address->id]);
        return $address;

    }
    public function update(Practice $practice,Address $address,Request $request){

            $address->update($request->all());
            return $address;
      
    }
    public function destroy(Practice $practice,Address $address){

       $practice->addresses()->where('address_id','=',$address->id)->delete();
       $address->delete();
        return $address;

    }
}
