<?php

namespace App\Http\Controllers\Microservices;

use App\Models\AddressBook;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddressBookController extends Controller
{
    public function index(Request $request){

        $query=AddressBook::where('content','LIKE',"%".$request->input("q")."%");

        return $query->paginate(15);
    }

    public function store(Request $request){

        $this->validate($request,[
            'content'=>"max:255|required"
        ]);
        $address_book=AddressBook::create(['content'=>$request->input("content")]);

        return $address_book;


    }
    public function update(AddressBook $address_book,Request $request){

        $this->validate($request,[
            'content'=>"max:255|required"
        ]);
        $address_book->update(['content'=>$request->input("content")]);

        return $address_book;


    }
    public function destroy(AddressBook $address_book,Request $request){

        $address_book->delete();

        return response('ok',200);


    }
}
