<?php

namespace App\Http\Controllers\Microservices;

use App\Models\EmailTemplate;
use App\Models\Recipient2nd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Recipient2ndController extends Controller
{
    public function search(Request $request)
    {

        return Recipient2nd::where("email", 'like', '%' . $request->input('q') . '%')
            ->orWhere("first_name", 'like', '%' . $request->input('q') . '%')
            ->orWhere("last_name", 'like', '%' . $request->input('q') . '%')
            ->take(20)
            ->orderby("email", "asc")
            ->get(['id', "first_name", "last_name", 'email']);
    }

}
