<?php


namespace App\Http\Controllers\MicroServices;

use Illuminate\Support\Facades\Storage;
use Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class FileUploadController extends Controller
{
    public function upload(Request $request)
    {
        $filename = Str::random(16).'_'.$request->all()['file_name'];
        Storage::disk('public')->put( 'etc/' . $filename, base64_decode($request->all()['value']));
        return response(url('/storage/etc/'.$filename));
    }
}