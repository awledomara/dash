<?php

namespace App\Http\Controllers\Microservices;

use App\Models\Doctor;
use App\Models\EmailTemplate;
use App\Models\Recipient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoctorsSearchController extends Controller
{
    public function search(Request $request)
    {

        return Doctor::join('users','users.id','user_id')
            ->where("email", 'like', '%' . $request->input('q') . '%')
            ->orWhere("first_name", 'like', '%' . $request->input('q') . '%')
            ->orWhere("last_name", 'like', '%' . $request->input('q') . '%')
            ->take(20)
            ->get(['doctors.id', "first_name", "last_name"]);
    }

    public function show(EmailTemplate $template)
    {

        return $template;
    }
}
