<?php

namespace App\Http\Controllers\Microservices;

use App\Models\EmailTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailTemplateController extends Controller
{
    public function search(Request $request){

        return EmailTemplate::where("name", 'like', '%' . $request->input('q') . '%')
            ->take(20)
            ->orderby("name","asc")
            ->get(['id', 'name']);
    }
    public function show(EmailTemplate $template){

        return $template;
    }
}
