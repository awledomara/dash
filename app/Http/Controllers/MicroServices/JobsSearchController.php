<?php

namespace App\Http\Controllers\Microservices;

use App\Models\Doctor;
use App\Models\EmailTemplate;
use App\Models\Job;
use App\Models\Recipient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobsSearchController extends Controller
{
    public function search(Request $request)
    {

        // Job -> doctors , practices , servicearea , startdate

        return Job::leftJoin('doctors', 'doctors.id', 'doctor_id')
            ->leftJoin('users', 'users.id', 'doctors.user_id')
            ->leftJoin('practices', 'practices.id', 'practice_id')
            ->leftJoin('service_areas', 'service_areas.id', 'missions.service_area_id')
            ->where("users.email", 'like', '%' . $request->input('q') . '%')
            ->orWhere("users.first_name", 'like', '%' . $request->input('q') . '%')
            ->orWhere("users.last_name", 'like', '%' . $request->input('q') . '%')
            ->orWhere("practices.name", 'like', '%' . $request->input('q') . '%')
            ->orWhere("practices.primary_email", 'like', '%' . $request->input('q') . '%')
            ->orWhere("practices.first_name", 'like', '%' . $request->input('q') . '%')
            ->orWhere("practices.last_name", 'like', '%' . $request->input('q') . '%')
            ->orWhere("service_areas.name", 'like', '%' . $request->input('q') . '%')
            ->orWhere("service_areas.designation", 'like', '%' . $request->input('q') . '%')
            ->orWhere("missions.start_date", 'like', '%' . $request->input('q') . '%')
            ->take(20)
            ->get(['missions.id',
                "users.first_name as doctor_first_name",
                "users.last_name as doctor_last_name",
                "practices.name as practice_name",
                "practices.first_name as practice_first_name",
                "practices.last_name as practice_last_name",
                "service_areas.name as service_area_name",
                "missions.start_date"]);
    }

    public function show(EmailTemplate $template)
    {

        return $template;
    }
}
