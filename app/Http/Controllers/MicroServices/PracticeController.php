<?php

namespace App\Http\Controllers\MicroServices;

use App\Models\ServiceArea;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Practice;

use Illuminate\Support\Facades\DB;

class PracticeController extends Controller
{
    public function search(Request $request)
    {

        return Practice::where(DB::raw("CONCAT(first_name,' ',last_name)"), 'like', '%' . $request->input('q') . '%')
            ->take(20)
            ->orderby(DB::raw("CONCAT(first_name,' ',last_name)"),"asc")
            ->get(['id', 'first_name', 'last_name', 'service_area_id','primary_email']);
    }

    public function services(Practice $practice, Request $request)
    {

        return ServiceArea::whereIn("id",$practice->service_areas()->pluck("service_area_id"))
            ->where('name','like','%'.$request->input('q').'%')
            ->take(10)
            ->get(["id","name"]);


    }
}
