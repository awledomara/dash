<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\Practice;
use Carbon\Carbon;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PracticeMissionConfirmController extends Controller
{
    public function confirm_mission(Practice $practice,Job $job){


       if($job->practice_id!=$practice->id){
           return redirect()->back()->with(['message'=>"Job or mission doesn't match "]);
       }

        $job->verified=1;
        $job->verified_at=Carbon::now();
        $job->verified_by=Auth::user()->id;
        $job->update();

        return redirect()->back()->with(['message'=>"Mission Confirmed"]);
    }
    public function confirm_many(Practice $practice,Request $request){

        Job::whereIn('id',$request->input('jobs_ids'))
            ->where('practice_id',$practice->id)
            ->update([
                "verified"=>"true",
                "verified_at"=>Carbon::now(),
                "verified_by"=>Auth::user()->id
            ]);


        return redirect()->back()->with(['message'=>"Mission Confirmed"]);
    }
}
