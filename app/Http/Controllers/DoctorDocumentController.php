<?php

namespace App\Http\Controllers;

use App\Helpers\DocumentHelper;
use App\Http\Requests\UploadDocumentRequest;

use App\Models\Document;
use App\Models\JsonHelper;
use App\Models\Doctor;


class DoctorDocumentController extends Controller
{
    public function index(Doctor $doctor){
        return $doctor->documents()->with('document')->get();
    }
    public function show(Doctor $doctor,Document $document){
        if($doctor->documents()->where('document_id',$document->id)->count('*')){
            return $document;
        }
        return JsonHelper::entityNotFound('Document Not Found');
    }
    public function store(Doctor $doctor,UploadDocumentRequest $request){

        $document = DocumentHelper::save($request);
        $doctor->documents()->create(['document_id'=>$document->id]);

        return $document;

    }
    public function update(Doctor $doctor,Document $document,UploadDocumentRequest $request){


        return $document;

    }
    public function destroy(Doctor $doctor,Document $document){

        $doctor->documents()->where('document_id','=',$document->id)->delete();
        //delete uploaded file
        // Storage::delete($document->path);
        $document->delete();
        return $document;

    }
}
