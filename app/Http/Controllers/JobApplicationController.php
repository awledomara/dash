<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Job;
use App\Models\JobApplication;
use App\User;
use function foo\func;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class JobApplicationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->only("apply");
    }

    public function index()
    {
        $jobs = Job::whereHas("applications")
            ->whereDoesntHave('doctor')
            ->orWhereNotNull('notification_sent_at')
            ->with([
                "applications",
                "applications.doctor" => function ($q) {
                    $q->select('id', 'user_id', 'phone1');
                },
                "applications.doctor.user" => function ($q) {
                    $q->select('id', 'first_name', 'last_name', 'email');
                }
            ])->paginate(10);

        return view("applications.index", compact("jobs"));
    }

    /**
     *
     * @param Job $job
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function apply(Job $job, Request $request)
    {


        $user = Auth::user();
        if (Auth::user()->hasRole('Doctor')) {
            $doctor = Doctor::where('user_id', "=", $user->id)->first();
            if ($job->applications()->where('doctor_id', "=", $doctor->id)->count() > 0) {
                return redirect()->to(route("jobs.apply_form_for_doctors", $job))->with(['message' => __("You have already done!")]);
            }
            JobApplication::create([
                "mission_id" => $job->id,
                "doctor_id" => $doctor->id
            ]);
            return redirect()->to(route("jobs.apply_form_for_doctors", $job))->with(['message' => "Thank you for your Application!"]);
        }

        return redirect()->to(route("jobs.apply_form_for_doctors", $job))->with(['message' => __("You don't have permission")]);

    }

    public function show(Job $job, Request $request)
    {

        return view("jobs.application_form", compact("job"));


        //
    }

    public function acceptApplication($id)
    {
        $application = JobApplication::find($id);

        $application->mission->doctor_id = $application->doctor->id;
        $application->mission->save();

        return response('ok');
    }

    public function emailApply(Job $job, Request $request)
    {
        $doctor = Doctor::find($request->get('dr_id'));
        if ($job->applications()->where('doctor_id', "=", $doctor->id)->count() > 0) {
            return response("You have already done!");
        }
        JobApplication::create([
            "mission_id" => $job->id,
            "doctor_id" => $doctor->id
        ]);
        return response("Successful!");
    }
}
