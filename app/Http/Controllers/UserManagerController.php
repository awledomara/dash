<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserManagerController extends Controller
{

    /**
     * @param Request $request
     * returns the users with agent role only
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request){

        $users=User::paginate(10);
        return view('users.index',compact('users'));
    }

    public function create(){
        $roles=Role::all();
        $permissions=Permission::all();
        return view('users.create',compact('roles','permissions'));
    }
    public  function store(StoreUserRequest $request){
        DB::transaction(function () use($request){
            $user= User::create([
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'email'=>$request->email,
                'password'=> Hash::make($request->password),
                'date_of_birth'=>$request
            ]);

            $user->assignRole($request->role);
            $user->givePermissionTo($request->permissions);
        });




        return redirect(route('users.index'));
    }
    public function edit(User $user){
        $roles=Role::all();
        $permissions=Permission::all();
        $user_permissions= $user->permissions()->pluck('id')->toArray();
        return view('users.edit',compact('roles','permissions','user','user_permissions'));
    }

    public function update(User $user,UpdateUserRequest $request){
        DB::beginTransaction();
        $user->update($request->only('first_name','last_name','email','date_of_birth'));
        if($request->input('password')){
            $user->update(['password'=>Hash::make($request->input('password'))]);
        }
        $user->syncRoles($request->input('role'));
        $user->syncPermissions($request->input('permissions'));
        DB::commit();
        return redirect()->route('users.edit',$user);
    }
}
