<?php

namespace App\Http\Controllers;


use App\Models\EmailTemplate;
use App\Models\EmailTemplateFile;
use App\TempFile;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmailTemplateController extends Controller
{

    public function index(Request $request)
    {

        $templates = EmailTemplate::paginate(10);

        return view("crm.templates.index", compact('templates'));

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => "required",
            "subject" => "required",
            "content" => "required"
        ]);
//        $content=htmlentities($request->input('content'));
        $content = $request->input('content');
        $subject = $request->input('subject');
        $name = $request->input('name');
        $template = EmailTemplate::create(compact('content', 'subject', 'name'));

        $this->uploadFiles($template, $request);

        return redirect()->action('EmailTemplateController@edit', $template);


    }

    public function create(Request $request)
    {

        return view("crm.templates.create");

    }

    public function edit(EmailTemplate $template)
    {

        return view("crm.templates.edit", ['template' => $template, 'files' => $template->files]);

    }

    public function update(Request $request)
    {

        $this->validate($request, [
            'name' => "required",
            "subject" => "required",
            "content" => "required"
        ]);
//        $content=htmlentities($request->input('content'));
        $content = $request->input('content');
        $subject = $request->input('subject');
        $name = $request->input('name');

        $template = EmailTemplate::findOrFail($request->input("template_id"));


        $template->update(compact('subject', 'name', 'content'));

        $this->uploadFiles($template, $request);

        return view("crm.templates.edit", ['template' => $template, 'files' => $template->files]);

    }

    private function uploadFiles(EmailTemplate $emailTemplate, Request $request)
    {
        if ($request->hasFile('files')) {

            $files = $request->file('files');
            foreach ($files as $file) {

                $path = $file->store('email_files');

                $_file = EmailTemplateFile::create([
                    'path' => str_replace(["/", "\\"], DIRECTORY_SEPARATOR, $path),//storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.
                    'email_template_id' => $emailTemplate->id,
                    'original_name' => $file->getClientOriginalName()
                ]);

            }
        }
    }

    public function deleteFile(Request $request, $templateID, $templateFileID)
    {
        EmailTemplateFile::find($templateFileID)->delete();
        return redirect()->route('templates.edit', [$templateID])->with(['message' => 'File deleted successfully']);
    }
}
