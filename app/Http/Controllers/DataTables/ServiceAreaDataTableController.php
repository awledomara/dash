<?php

namespace App\Http\Controllers\DataTables;

use App\Models\ServiceArea;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceAreaDataTableController extends Controller
{
   public function index(Request $request){
       $query= ServiceArea::query();


       return datatables($query)->make(true);
   }
}
