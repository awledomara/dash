<?php

namespace App\Http\Controllers\DataTables;

use App\Models\Job;
use App\Models\Practice;
use Illuminate\Filesystem\Cache;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PracticeDataTableController extends Controller
{
   public function index(Request $request){

       $query= Practice::with(['facility','speciality']);
       if($request->input("service_area",null)){
           $query=$query->whereHas("serviceArea",function ($q) use($request){

               $q->where('id',$request->input('service_area'));
           });
       }
      return datatables($query)->make(true);

   }
    public function jobs(Practice $practice,Request $request){

        $query = Job::with([
            'service_area' => function ($q) {
                $q->select('id', 'name');
            }
        ])->where("practice_id", "=", $practice->id);


        return datatables($query)->make(true);

    }

}
