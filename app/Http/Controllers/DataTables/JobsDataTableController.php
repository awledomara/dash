<?php

namespace App\Http\Controllers\DataTables;

use App\Models\Job;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobsDataTableController extends Controller
{

    public function index(Request $request){
        $status=$request->input("status",null);

        $practice_id=$request->input("p_id");
        $service_id=$request->input('s_id');
        $query=Job::query();
        if(!is_null($practice_id)){
            $query=Job::where('practice_id','=',$practice_id);
        }

        if(!is_null($service_id)){
            $query=$query->whereHas("service_area",function ($q) use($practice_id,$service_id){
                $q->where('id','=',$service_id);
            });
        }

         if(!is_null($status)){
             $query=$query->where("status",'=',$status);
         }



        $query= $query->with([
           "service_area"=>function($q){

           return $q->select('id','name');
           },
           "practice"=>function($q){

               return $q->select('id','first_name','last_name');
           }

       ]);


       return datatables($query)->make(true);
   }
}
