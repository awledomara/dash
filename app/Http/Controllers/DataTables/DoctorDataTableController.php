<?php

namespace App\Http\Controllers\DataTables;

use App\Models\Doctor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoctorDataTableController extends Controller
{
   public function index(Request $request){


       $query = Doctor::with(
           ["address",
           "user" => function ($q) {
                    $q->select('email','first_name','last_name','id');
           },
           'facility',
           'speciality',
           "serviceArea"
       ])->select(['doctors.id','doctors.user_id','doctors.speciality_id','doctors.facility_id','service_area_id','address_id']);



       return datatables($query)->make(true);
   }
}
