<?php

namespace App\Http\Controllers;

use App\Helpers\DocumentHelper;
use App\Http\Requests\UploadDocumentRequest;

use App\Models\Document;
use App\Models\JsonHelper;
use App\Models\Practice;


class PracticeDocumentController extends Controller
{
    public function index(Practice $practice){
        return $practice->documents()->with('document')->get();
    }
    public function show(Practice $practice,Document $document){
        if($practice->documents()->where('document_id',$document->id)->count('*')){
            return $document;
        }
        return JsonHelper::entityNotFound('Document Not Found');
    }
    public function store(Practice $practice,UploadDocumentRequest $request){

            $document = DocumentHelper::save($request);
            $practice->documents()->create(['document_id'=>$document->id]);

        return $document;

    }
    public function update(Practice $practice,Document $document,UploadDocumentRequest $request){


        return $document;

    }
    public function destroy(Practice $practice,Document $document){

        $practice->documents()->where('document_id','=',$document->id)->delete();
        //delete uploaded file
        // Storage::delete($document->path);
        $document->delete();
        return $document;

    }
}
