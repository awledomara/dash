<?php

namespace App\Http\Controllers\AjaxViews;

use App\Models\Practice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PracticeController extends Controller
{
     public function search(Request $request){
         $q=$request->input('q',null);
         $practices=Practice::where(DB::raw("CONCAT(first_name,' ',last_name)"), 'like', '%' . $q . '%')
             ->take(20)
             ->orderby(DB::raw("CONCAT(first_name,' ',last_name)"),"asc")
             ->get(['id', 'first_name', 'last_name', 'service_area_id','primary_email']);
         return view("ajax.practices.jobs_link",compact('practices'));

     }
}
