<?php

namespace App\Http\Controllers\AjaxViews;

use App\Models\Job;
use App\Models\JobApplication;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class JobApplicationController extends Controller
{
    public function index(Job $job,Request $request){
        $doctor_name=$request->input('doctor_name',null);
        $query=JobApplication::query();
        if($doctor_name){
            $query=$query->whereHas("doctor.user",function($q) use($doctor_name){
                    return $q->where(DB::raw("CONCAT(first_name,' ',last_name)"),'like',"%".$doctor_name."%");
            });
        }
       $jobApplications= $query->with([
           'doctor'=>function($q){
                  $q->select('id','user_id','phone1');
        },
           'doctor.user'=>function($q){
               $q->select('id','email','first_name','last_name');
           }])
           ->where('mission_id',$job->id)
         ->get();

       return view("ajax.doctors.applied_for",compact("jobApplications",'job'));
    }
}
