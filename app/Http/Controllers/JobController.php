<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreJobRequest;
use App\Models\Doctor;
use App\Models\Job;
use App\Models\JobFee;
use App\Models\Practice;
use App\Models\ServiceArea;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MongoDB\Driver\Exception\ExecutionTimeoutException;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Job[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        $practice=Practice::find($request->input("p_id"));
        $service=ServiceArea::find($request->input("s_id"));
        $status=$request->input('status',null);
        return view('jobs.index',compact('practice','status','service'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $tags=Tag::all();
        return view('jobs.create',compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Job
     */
    public function store(StoreJobRequest $request)
    {

       // return Carbon::createFromFormat("d.m.Y H:m",$request->input("start_date"))->toDateTimeString();

        $tags=json_decode($request->input('tags'));



        if(is_array($tags)){
            foreach ($tags as $tag){
                if(!array_key_exists('id',$tag)){
                    Tag::firstOrCreate(['value'=>$tag->value]);
                }

            }
        }
        $job=new Job;
        $job->fill($request->only('practice_id','service_area_id','notes','additional_remuneration','ab'));
        $job->start_date=Carbon::createFromFormat("d.m.Y H:i",$request->input("start_date"));
        $job->end_date=Carbon::createFromFormat("d.m.Y H:i",$request->input("end_date"));

        $job->tags=$tags;
        $time_frames=[];
        try{
            $to=$request->input("to",[]);
            $notes=$request->input("notes",[]);
            $per_hours=$request->input("per_hour",[]);

            foreach ($request->input('from',[]) as $index=>$from){
                    $time_frames[]=[
                        "from"=>Carbon::createFromFormat("d.m.Y H:i",$from),
                        "to"=>Carbon::createFromFormat("d.m.Y H:i",$to[$index]),
                        "per_hour"=>$per_hours[$index],
                        "notes"=>$notes[$index]
                    ];
            }

        }catch (\Exception $ex){

        }
        $job->time_frames=$time_frames;

        $job->created_by=Auth::user()->id;

        $job->save();

        return redirect()->route('jobs.show',$job)->with(['message'=>"job successfully created"]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {


        $doctors = Doctor::with([
            'user' => function ($q) {
                $q->select('id', 'first_name', 'last_name', 'email');
            }])
            ->get(["id", "user_id"]);

        return view('jobs.show',compact('job','doctors'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {


        $tags=Tag::all();
        return view('jobs.edit',compact('tags','job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Job $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {



        $tags=json_decode($request->input('tags'));

        $time_frames=json_decode($request->input('frames'));
        if(is_array($tags)){
            foreach ($tags as $tag){
                Tag::firstOrCreate(['value'=>$tag->value]);
            }
        }

        $job->update($request->only('practice_id','service_area_id','notes','additional_remuneration','ab'));
        $job->start_date=$request->input("start_date").":00";
        $job->end_date=$request->input("end_date").":00";
        $job->tags=$tags;
        $job->time_frames=$time_frames;
        $job->save();

        return redirect()->route('jobs.show',$job)->with(['message'=>"job successfully updated"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
