<?php


namespace App\Http\Controllers;


use App\Jobs\DoctorsNotification;
use App\Models\Doctor;
use App\Models\Job;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DoctorsNotificationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function notifications()
    {
        return view('doctors-notifications.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function sendNotifications(Request $request)
    {
        $this->validate($request,[
            'jobs'=>"required"
        ]);

        foreach ($request->get('jobs',[]) as $jobID){
            $job=Job::find($jobID);
            DoctorsNotification::dispatch($request->get('doctors',[]),$job->start_date,$job->end_date,$job->service_area->name,$job->id);
            $job->notification_sent_at = Carbon::now();
            $job->save();
        }


        return redirect()->back()->with(['message'=>"Email(s) has been sent"]);
    }
}
