<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\JsonHelper;
use App\Models\Doctor;
use Illuminate\Http\Request;

class DoctorAddressController extends Controller
{
    public function index(Doctor $Doctor){
        return $Doctor->addresses()->with('address')->get();
    }
    public function show(Doctor $Doctor,Address $address){
        if($Doctor->addresses()->where('address_id',$address->id)->count('*')){
            return $address;
        }
        return JsonHelper::entityNotFound('Address Not Found');
    }
    public function store(Doctor $Doctor,Request $request){

        $address=Address::create($request->all());
        $Doctor->addresses()->create(['address_id'=>$address->id]);
        return $address;

    }
    public function update(Doctor $Doctor,Address $address,Request $request){

        $address->update($request->all());
        return $address;

    }
    public function destroy(Doctor $Doctor,Address $address){

        $Doctor->addresses()->where('address_id','=',$address->id)->delete();
        $address->delete();
        return $address;

    }
}
