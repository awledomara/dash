<?php


namespace App\Http\Controllers;


use App\Models\Recipient;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RecipientsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $recipients = Recipient::paginate(10);

        return view('recipients.index', compact('recipients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('recipients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => "required",
            'last_name' => "required",
            'email' => "required|email",
        ]);
        Recipient::create($request->only(['first_name', 'last_name', 'email']));
        return redirect()->route('recipients.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Recipient $recipient
     * @return void
     * @throws \Exception
     */
    public function destroy(Recipient $recipient)
    {
        $recipient->delete();
        return redirect()->back()->with(['message' => "Recipient deleted"]);
    }
}