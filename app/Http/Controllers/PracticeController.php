<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Http\Requests\StorePracticeRequest;
use App\Http\Requests\UpdatePracticeRequest;
use App\Models\Address;
use App\Models\Doctor;
use App\Models\Facility;
use App\Models\MedicalCareCenter;
use App\Models\Person;
use App\Models\Practice;
use App\Models\ServiceArea;
use App\Models\Speciality;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PracticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $practices=Practice::with(['addresses.address','speciality'])
                   ->paginate(10);

        return view('practices.index',compact('practices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('practices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePracticeRequest $request)
    {


        $service_areas=$request->input('service_area_ids',[]);

        DB::transaction(function ()use($request,&$practice,$service_areas){

          //  $person=Person::create($request->all());
            $address= Address::create($request->only('street','city','postal_code'));

            $practice=Practice::create($request->merge([
                //'contact_person_id'=>$person->id,
                'debit_authorization'=>$request->input('debit_authorization')=='on',
                'in_mvz'=>$request->input('in_mvz')=='on',

            ])->all());

            $practice->service_areas()->attach($service_areas);

            $practice->addresses()->create(['address_id'=>$address->id]);

            MedicalCareCenter::create(
                $request->merge(['practice_id'=>$practice->id])
                        ->only('mcc_name','mcc_contact_name','mcc_phone','mcc_street_number','mcc_post_office','practice_id','mcc_city','mcc_fax','mcc_email')
            );
        });
        return redirect(route('practices.edit',['id'=>$practice->id]))->with(['message'=>"$practice->name was successfully saved"]);
    }

    /**
     * Display the specified resource.
     *
     * @param Practice $practice
     * @return Practice|\Illuminate\Database\Eloquent\Builder
     */
    public function show(Practice $practice)
    {

        return $practice->load("mcc",'contact_person');


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Practice $practice
     * @return \Illuminate\Http\Response
     */
    public function edit(Practice $practice)
    {
        $specialities=Speciality::all(['id','name']);
        $facilities=Facility::all(['id','name']);
        $services=ServiceArea::all(['id','name']);
        $grades=Grade::all(['id','name']);

        $practice->load('addresses.address');
        $practice->load('documents.document');
        return view('practices.edit',compact('practice','specialities','services','facilities','grades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePracticeRequest $request
     * @param Practice $practice
     * @return array
     */
    public function update(UpdatePracticeRequest $request, Practice $practice)
    {



        $mcc_fields=$request->only('mcc_name','mcc_contact_name','mcc_phone','mcc_street_number','mcc_post_office','mcc_city','mcc_fax','mcc_email');

        $mcc_fields['practice_id']=$practice->id;

        $service_areas=$request->input('service_area_ids',[]);

        DB::transaction(function ()use($request,&$practice,$mcc_fields,$service_areas){


            //$practice->contact_person->update($request->only('first_name','last_name','email','phone','fax','title','grade'));

            if($practice->mcc){
                $practice->mcc()->update($mcc_fields);
            }else{
                MedicalCareCenter::create($mcc_fields);

            }


           $practice->service_areas()->sync($service_areas);


            $request->merge(['debit_authorization'=>$request->input('debit_authorization')=='on',  'in_mvz'=>$request->input('in_mvz')=='on']);
            $practice->update($request->all());

        });
       return redirect()->back()->with(['message'=>"Practice information was updated successfully"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Practice $practice)
    {
        $practice->delete();
        return redirect()->back()->with(['message'=>"Practice information was trashed"]);
    }
}
