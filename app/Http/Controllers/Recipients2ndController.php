<?php


namespace App\Http\Controllers;


use App\Models\Recipient2nd;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Recipients2ndController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $recipients = Recipient2nd::paginate(10);

        return view('recipients-2nd.index', compact('recipients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('recipients-2nd.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => "required",
            'last_name' => "required",
            'email' => "required|email",
        ]);
        Recipient2nd::create($request->only(['first_name', 'last_name', 'email']));
        return redirect()->route('recipients-2nd.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Recipient2nd $recipient
     * @return void
     * @throws \Exception
     */
    public function destroy( $recipientID)
    {
        $recipient=Recipient2nd::find($recipientID);
        $recipient->delete();
        return redirect()->back()->with(['message' => "Recipient deleted"]);
    }
}