<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Http\Requests\StoreServiceAreaRequest;
use App\Http\Requests\UpdateServiceAreaRequest;
use App\Models\Address;
use App\Models\Person;
use App\Models\ServiceArea;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ServiceAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
          $services=ServiceArea::paginate(10);


        return view('serviceArea.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $grades=Grade::all();
        return view('serviceArea.create',compact('grades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(StoreServiceAreaRequest $request)
    {



        DB::transaction(function () use($request,&$service){

            $service= ServiceArea::create($request->all());

            $service->save();

        });


        return redirect(route("services.edit",$service))->with(['message'=>"Service Area was saved successfully"]);

    }

    /**
     * Display the specified resource.
     *
     * @param ServiceArea $service
     * @return ServiceArea
     */
    public function show(ServiceArea $service)
    {

        return $service;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  ServiceArea $service
     * @return Response
     */
    public function edit(ServiceArea $service)
    {
        $grades=Grade::all();
        return view('serviceArea.edit',compact('service','grades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param ServiceArea $service
     * @return void
     */
    public function update(UpdateServiceAreaRequest $request, ServiceArea $service)
    {


        DB::transaction(function () use(&$service,$request){

           // $service->contactPerson()->update($request->only('first_name','last_name','email','phone','title','grade'));
            //$service->address()->update($request->only('city','street','postal_code'));
            $service->update($request->all());

        });


        return redirect()->back()->with(['message'=>"Service Area was updated successfully"]);


    }


    public function clone(ServiceArea $service){

        DB::transaction(function () use(&$service,&$newservice){


            $newservice= $service->replicate();
            $newservice->name=$service->name."_##copy";
            $newservice->push();

        });


        return redirect(route("services.edit",$newservice))->with(['message'=>"Service Area was cloned successfully"]);


    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(ServiceArea $service)
    {
        $service->delete();
        return redirect()->back()->with(['message'=>"Service Area was trashed"]);

    }
}
