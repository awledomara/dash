<?php


namespace App\Http\Controllers;


use App\Models\SentBulkEmail;

class SentEmailsController
{

    public function index()
    {

        $emails=SentBulkEmail::paginate(15);

        return view('crm.sent-emails',compact('emails'));
    }

}