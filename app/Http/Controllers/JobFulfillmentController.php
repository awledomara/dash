<?php


namespace App\Http\Controllers;

use App\Mail\GenericEmail;
use App\Mail\InvoiceMail;
use DateTime;
use Illuminate\Support\Facades\Mail;
use PDF;
use App\Models\Job;
use Illuminate\Http\Request;

class JobFulfillmentController extends Controller
{
    public function index()
    {
        $jobs = Job::whereHas('doctor')->with([
            "doctor" => function ($q) {
                $q->select('id', 'user_id', 'phone1');
            },
            "doctor.user" => function ($q) {
                $q->select('id', 'first_name', 'last_name', 'email');
            },
            "practice" => function ($q) {
                $q->select('id', 'first_name', 'last_name');
            },
            "service_area" => function ($q) {
                $q->select('id', 'name');
            },
        ])->paginate(10);

        return view("jobs.fulfillment", compact("jobs"));
    }

    public function show(Job $job, Request $request)
    {
        return view("jobs.fulfillment-details", compact("job"));
    }

    public function sendPDF(Job $job)
    {
        $date = (new DateTime())->format('d.m.Y');
        $name = date('Y-m-d_H:i:s_') . 'job_id_' . $job->id . '.pdf';
        $path = storage_path('app/public/pdf/invoices/' . $name);

        $pdf = PDF::loadView('pdf.invoice',compact("job"));
        $pdf->save($path);

        Mail::to($job->doctor->user)->send(new InvoiceMail($job->doctor->user->email,$path, $date));
        Mail::to($job->practice->primary_email)->send(new InvoiceMail($job->practice->primary_email,$path, $date));

        return redirect()->back()->with(['message' => "The invoice has been sent."]);

//        return view('pdf.invoice',compact("job"));
    }
}