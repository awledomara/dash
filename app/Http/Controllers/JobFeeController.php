<?php

namespace App\Http\Controllers;

use App\Http\Requests\JobFeeRequest;
use App\Models\Job;
use App\Models\JobFee;
use Illuminate\Http\Request;

class JobFeeController extends Controller
{
    /**
     * @param Job $job
     */
    public function index(Job $job){

        return $job->fees;
    }

    /**
     * @param Job $job
     * @param Request $request
     */
    public function store(Job $job, JobFeeRequest $request){


        $fee=JobFee::create([
            'from'=>$request->input('from'),
            'per_hour'=>$request->input('per_hour'),
            'job_id'=>$job->id,
            'status'=>JobFee::PENDING
        ]);

        return $fee;
    }

    /**
     * @param Job $job
     * @param JobFee $fee
     * @param JobFeeRequest $request
     * @return JobFee
     */
    public function update(Job $job, JobFee $fee, JobFeeRequest $request){

        if($fee->job_id==$job->id){
            $fee->update([
                'from'=>$request->input('from'),
                'per_hour'=>$request->input('per_hour'),
                'status'=>JobFee::PENDING
            ]);
        }


        return $fee;
    }


    /**
     * @param Job $job
     * @param JobFee $fee
     * @param JobFeeRequest $request
     * @return JobFee
     */
    public function updateStatus(Job $job, JobFee $fee, Request $request){

        $this->validate($request,[
            'status'=>"in:0,1,2"
        ]);
        if($fee->job_id==$job->id){
            $fee->update([
                'status'=>$request->input('status')
            ]);
        }

        return $fee;
    }
}
