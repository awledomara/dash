<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Http\Requests\StoreDoctorRequest;
use App\Http\Requests\UpdateDoctorRequest;
use App\Models\Address;
use App\Models\Doctor;
use App\Models\Facility;
use App\Models\ServiceArea;
use App\Models\Speciality;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $doctors= Doctor::with([
            'serviceArea','addresses.address','speciality',
            'user'=>function($query){
                        return $query->select('first_name','last_name','email','active','id');
            },
        ])->paginate(10);


        return view('doctors.index',compact('doctors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {   $specialities=Speciality::all();

        $services=ServiceArea::all();
        $grades=Grade::all();
        return view('doctors.create',compact('specialities','facilities','services','grades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(StoreDoctorRequest $request)
    {
        $service_area=$request->input('service_area_ids',[])[0];

        DB::transaction(function ()use($request,&$doctor,$service_area){

            $address= Address::create($request->only('street','city','postal_code'));
            $user=User::create($request->merge(['password'=>bcrypt(uniqid())])->all());
            $user->assignrole('Doctor');

            $doctor=Doctor::create($request->merge(['user_id'=>$user->id,'address_id'=>$address->id,'service_area_id'=>$service_area])->all());

//            dd($service_area);
//            $doctor->serviceArea()->attach($service_area);
        });


        return redirect(route("doctors.edit",['id'=>$doctor->id]))->with(['message'=>'Success ']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Doctor $doctor)
    {

        return view('doctors.show',compact('doctor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Doctor $doctor
     * @return Response
     */
    public function edit(Doctor $doctor)
    {

        $specialities=Speciality::all();
        //$facilities=Facility::all();
        $services=ServiceArea::all();

        $grades=Grade::all();
        $doctor->load('addresses.address');
        $doctor->load('documents.document');
        return view('doctors.edit',compact('doctor','specialities','services','grades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDoctorRequest $request
     * @param Doctor $doctor
     * @return Response
     */
    public function update(UpdateDoctorRequest $request, Doctor $doctor)
    {
        DB::transaction(function ()use($request,&$doctor){

            $doctor->update($request->all());
            if($doctor->address){
                $doctor->address()->update($request->only('street','city','postal_code'));

            }else{

                $address=Address::create($request->only('street','city','postal_code'));
                $doctor->update(['address_id'=>$address->id]);
            }

            $doctor->user()->update($request->all());
        });
        return redirect()->back()->with(['message'=>__('Doctor information was updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Doctor $doctor)
    {
        $doctor->delete();

        return redirect()->back()->with(['message'=>__('Doctor account was trashed')]);
    }
}
