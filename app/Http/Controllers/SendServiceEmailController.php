<?php

namespace App\Http\Controllers;

use App\Jobs\BulkEmailRecipient2nd;
use App\Models\EmailTemplate;
use App\Models\Recipient;
use App\Models\Recipient2nd;
use App\Models\SentBulkEmail;
use Illuminate\Http\Request;
use PharIo\Manifest\Email;

class SendServiceEmailController extends Controller
{
    public function show(){

        return view("crm.templates.send_service_email");
    }


    public function send(Request $request){


        $this->validate($request,[
            'template_id'=>"required",
            "content"=>'required',
            "subject"=>"required",
            "recipients"=>"array|required"
        ]);

        $template=EmailTemplate::find($request->input('template_id'));

        //BulkEmailRecipient2nd::dispatch($template,$request->input('recipients',[]),$request->input('subject'),$request->input('content'));

        $this->saveRecord($template,$request);


        return redirect()->to(route('templates.index'))->with(['message'=>'Email(s) sent successfully']);

    }

    private function saveRecord($template,$request)
    {
        $sentEmail = new SentBulkEmail();
        $sentEmail->form = 'Dienste ausschreiben';
        $sentEmail->template_id = $template->id;
        $sentEmail->subject = $request->input('subject');

        $recipients = '';
        foreach ($request->input('recipients', []) as $item) {
            $recipient = Recipient2nd::find($item);
            $recipients .= ",{$recipient->id}:$recipient->email: {$recipient->first_name} {$recipient->last_name} ";
        }

        $sentEmail->recipients = $recipients.',';
        $sentEmail->save();
    }
}
