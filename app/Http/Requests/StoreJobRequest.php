<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'additional_remuneration'=>"nullable|numeric",
            "start_date"=>"required|date",
            "practice_id"=>"required",
            "service_area_id"=>"required",
            "end_date"=>"required|date",

        ];
    }
    public function messages()
    {
        return [

            "practice_id"=>"practice field is required",
            "service_area_id"=>"service area field is required"

        ];
    }
}
