<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed first_name
 * @property mixed last_name
 * @property mixed password
 * @property mixed role
 * @property mixed permissions
 * @property mixed email
 * @property mixed address
 */
class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'first_name'=>'required|max:255',
            'last_name'=>'required|max:255',
            'date_of_birth'=>'nullable|date',
            'email'=>"required|email|unique:users,email",
            'temp_password'=>'nullable|password'
        ];
    }
}
