<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreServiceAreaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>"required|max:255",
            'designation'=>"max:255",
            'notes'=>"max:255",
            'internal_name'=>"max:255",
            'type'=>"",
            'city'=>"max:255",
            'street'=>"max:255",
            'postal_code'=>"max:255",
            "grade"=>"",
            "title"=>"",
            "first_name"=>"",
            'last_name'=>"",
            "phone"=>"",
            "email"=>"nullable|email|max:255"
        ];
    }
}
