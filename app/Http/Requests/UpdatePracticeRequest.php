<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePracticeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"=>"nullable|max:255",
            "speciality_id"=>"",
            "facility_id"=>"",
            "service_area_id"=>"",
            "title"=>"max:255",
            "grade"=>"max:255",
            "first_name"=>"max:255",
            "last_name"=>"max:255",
            "phone"=>"max:255",
            "email"=>"nullable|max:255",

            "primary_email"=>"bail|required|max:255|email",
            "secondary_email"=>"bail|nullable|email|max:255",
            "primary_phone"=>"bail|nullable|required|max:255",
            "secondary_phone"=>"bail|max:255",
            "monthly_fee"=>"nullable",
            "pay_period"=>"",
            "iban"=>"bail|max:255",
            "owner"=>"bail|max:255"

        ];
    }
}
