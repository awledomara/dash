<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        return [

            'first_name'=>['bail','required','max:255'],
            'last_name'=>['bail','required','max:255'],
            'mcc_name'=>"max:255",
            'mcc_contact_name'=>"max:255",
            'mcc_street_number'=>"max:255",
            'mcc_phone'=>"max:255",
            'mcc_post_office'=>"max:255",
            'facility_id'=>"",
            'speciality_id'=>"",
            'service_area_id'=>"",
            'email'=>['bail','required','max:255','email','unique:users,email,'.$this->doctor->user->id],
            'secondary_email'=>['nullable','email','max:255'],
            'phone1'=>['bail','required','max:255'],
            'phone2'=>['bail','max:255'],

        ];
    }
}
