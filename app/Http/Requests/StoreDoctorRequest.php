<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDoctorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
           return [
               'first_name'=>['bail','required','max:255'],
               'last_name'=>['bail','required','max:255'],

//               'facility_id'=>['bail','required'],
               'speciality_id'=>['bail','required'],
               'service_area_ids'=>['bail','required','max:1'],

               'email'=>['bail','required','max:255','email','unique:users,email'],
//               'secondary_email'=>['bail','email','max:255'],
               'phone1'=>['bail','required','max:255'],
               'phone2'=>['bail','max:255'],
           ];

    }


}
