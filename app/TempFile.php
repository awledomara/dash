<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempFile extends Model
{
    protected $fillable=[
        'expires','path'
    ];
    public $timestamps=false;
}
