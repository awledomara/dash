<?php

namespace App\Mail;

use App\Models\EmailTemplate;
use App\Models\Recipient;
use App\Models\Recipient2nd;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SimpleMail extends Mailable
{
    use Queueable, SerializesModels;


    public $subject;
    public $content;
    private $email;

    public function __construct($email , $subject , $content)
    {
        $this->subject = $subject;
        $this->content = $content;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->email)
            ->bcc(['swen.burdack@gmx.de'])
            ->subject($this->subject)
            ->view("emails.generic_email");
    }
}
