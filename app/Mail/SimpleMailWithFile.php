<?php

namespace App\Mail;

use App\Models\EmailTemplate;
use App\Models\Recipient;
use App\Models\Recipient2nd;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SimpleMailWithFile extends Mailable
{
    use Queueable, SerializesModels;


    public $subject;
    public $content;
    private $filePath;
    private $email;

    public function __construct($email, $filePath , $subject , $content)
    {
        $this->subject = $subject;
        $this->content = $content;
        $this->filePath = $filePath;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->to($this->email)
            ->bcc(['swen.burdack@gmx.de'])
            ->subject($this->subject)
            ->view("emails.generic_email");
        $mail->attach($this->filePath);
        return $mail;
    }
}
