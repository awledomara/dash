<?php

namespace App\Mail;

use App\Models\Job;
use App\Models\Practice;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MissionVerification extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $missions;
    public $practice;
    public function __construct(Practice $practice,$missions)
    {

        $this->missions=Job::whereIn('id',$missions)->get();
        $this->practice=$practice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject="Please verify your Jobs/Dienste";

        return $this->view('emails.jobs.verify')
            ->bcc('nizarawledomara@gmail.com')
            ->subject($subject);
    }



}
