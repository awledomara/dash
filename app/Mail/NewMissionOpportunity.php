<?php

namespace App\Mail;

use App\Models\Job;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewMissionOpportunity extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $mission;
    public $user;
    public function __construct(Job $job,User $user)
    {
        $this->mission=$job;
        $this->user=$user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject="BD365: ".optional($this->mission->service_area)->name.", ".$this->mission->start_date->format('d.m.Y H:m')." Uhr, Vertretung KV-Bereitschaftsdienst";

        return $this->view('emails.jobs.new')
            ->bcc('nizarawledomara@gmail.com')
            ->subject($subject);
    }
}
