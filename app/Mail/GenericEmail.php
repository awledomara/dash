<?php

namespace App\Mail;

use App\Models\EmailTemplate;
use App\Models\Recipient;
use App\Models\Recipient2nd;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GenericEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $template;
    /**
     * @var Recipient|Recipient2nd
     */
    public  $user;
    public $subject;
    public $content;

    public function __construct(EmailTemplate $template,$user,$subject,$content)
    {
        $this->user=$user;
        $this->template=$template;
        $this->subject = $subject;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->to($this->user->email)
                    ->bcc(['nizarawledomara@gmail.com','swen.burdack@gmx.de'])
                    ->subject($this->subject)
                    ->view("emails.generic_email");
        foreach ($this->template->files as $file) {
            $mail->attach(storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . $file->path, [
                'as' => $file->original_name,
            ]);
        }
        return $mail;
    }
}
