<?php

namespace App\Mail;

use App\Models\EmailTemplate;
use App\Models\Recipient;
use App\Models\Recipient2nd;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceMail extends Mailable
{
    use Queueable, SerializesModels;


    public $subject;
    public $content;
    private $filePath;
    private $email;

    public function __construct($email, $filePath, $date)
    {
        $this->subject = 'yor invoice from ' . $date;
        $this->content = 'attached: invoice';
        $this->filePath = $filePath;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->to($this->email)
            ->bcc(['swen.burdack@gmx.de'])
            ->subject($this->subject)
            ->view("emails.generic_email");
        $mail->attach($this->filePath, ['as' => 'invoice.pdf']);
        return $mail;
    }
}
