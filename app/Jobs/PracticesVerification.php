<?php

namespace App\Jobs;

use App\Mail\GenericEmail;
use App\Mail\NewJobOpportunity;

use App\Mail\NewMissionOpportunity;
use App\Mail\SimpleMailWithFile;
use App\Models\EmailTemplate;
use App\Models\Job;
use App\Models\Practice;
use App\Models\Recipient;
use App\Notifications\JobMail;
use App\User;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;


class PracticesVerification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $subject;
    private $content;
    private $filePath;

    public function __construct($filePath)
    {
        $this->subject = 'Dienstmeldungen schicken';
        $this->content = 'bitte senden Sie uns Ihre Dienste des kommenden Quartals';
        $this->filePath = $filePath;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Practice::chunk(20, function ($users) {
            foreach ($users as $user) {
                Mail::to($user->primary_email)->send(new SimpleMailWithFile($user->primary_email, $this->filePath, $this->subject, $this->content));
            }
        });
    }
}
