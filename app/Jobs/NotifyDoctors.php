<?php

namespace App\Jobs;

use App\Mail\NewJobOpportunity;

use App\Mail\NewMissionOpportunity;
use App\Models\Job;
use App\Notifications\JobMail;
use App\User;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PhpParser\Node\Expr\Array_;

class NotifyDoctors implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $opportunity;
    public $users_ids;
    public function __construct(Job $op,$users_ids)
    {

        $this->opportunity=$op;
        $this->users_ids=$users_ids;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()


    {

        User::whereIn("id",$this->users_ids)->chunk(20,function ($users){
           foreach ($users as $user){
               Mail::to($user)->send(new NewMissionOpportunity($this->opportunity,$user));
           }


        });
    }
}
