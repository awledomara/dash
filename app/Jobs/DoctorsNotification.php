<?php

namespace App\Jobs;

use App\Mail\GenericEmail;
use App\Mail\NewJobOpportunity;

use App\Mail\NewMissionOpportunity;
use App\Mail\SimpleMail;
use App\Mail\SimpleMailWithFile;
use App\Models\Doctor;
use App\Models\EmailTemplate;
use App\Models\Job;
use App\Models\Practice;
use App\Models\Recipient;
use App\Notifications\JobMail;
use App\User;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;


class DoctorsNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $subject;
    private $content;
    private $doctorIDs;
    private $jobID;

    public function __construct($doctorIDs, $job_start_date, $job_end_date, $job_service_area, $jobID)
    {
        $this->subject = 'job at ' . $job_start_date->format('d.m.Y H:m');
        $this->content = '- ' . $job_start_date->format('d.m.Y H:m')
            . ' - ' . $job_end_date->format('d.m.Y H:m')
            . ' <br>- ' . $job_service_area;

        $this->doctorIDs = $doctorIDs;
        $this->jobID = $jobID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (empty($this->doctorIDs))
            $doctors = Doctor::query();
        else {
            $doctors = Doctor::whereIn('id', $this->doctorIDs);
        }

        $doctors->chunk(20, function ($doctors) {
            foreach ($doctors as $doctor) {
                $content = $this->content
                    . " <br><br><a href='".route('email.job.apply',['job'=>$this->jobID])."?dr_id={$doctor->id}'>Apply to this job! - Job ID: {$this->jobID}</a>";

                Mail::to($doctor->user->email)->send(new SimpleMail($doctor->user->email, $this->subject, $content));
            }
        });
    }
}
