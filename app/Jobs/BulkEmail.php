<?php

namespace App\Jobs;

use App\Mail\GenericEmail;
use App\Mail\NewJobOpportunity;

use App\Mail\NewMissionOpportunity;
use App\Models\EmailTemplate;
use App\Models\Job;
use App\Models\Recipient;
use App\Notifications\JobMail;
use App\User;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;


class BulkEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $template;
    public $recipients_ids;
    private $subject;
    private $content;

    public function __construct(EmailTemplate $template, $recipients_ids, $subject, $content)
    {

        $this->template = $template;
        $this->recipients_ids = $recipients_ids;

        $this->subject = $subject;
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()


    {

        Recipient::whereIn("id", $this->recipients_ids)->chunk(20, function ($users) {
            foreach ($users as $user) {

                // Placeholders
                $this->content = str_replace('##name##', $user->first_name . ' ' . $user->last_name, $this->content);
                $this->content = str_replace('##email##', $user->email, $this->content);

                Mail::to($user)->send(new GenericEmail($this->template, $user, $this->subject, $this->content));
            }


        });
    }
}
