<?php

namespace App\Providers;

use App\Grade;
use App\Models\Facility;
use App\Models\ServiceArea;
use App\Models\Speciality;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       Schema::defaultStringLength(191);


        view()->composer('practices.create', function ($view) {
            $specialities=Speciality::all();
            $facilities=Facility::all();
            $services=ServiceArea::all();
            $grades=Grade::all();
            $view->with(compact('specialities','facilities','services','grades'));
        });



    }
}
