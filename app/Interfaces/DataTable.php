<?php


namespace App\Interfaces;


interface DataTable
{
   public function columns();

   public function sortable();
}
