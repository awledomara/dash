<?php

namespace App\Helpers;



use App\Http\Requests\UploadDocumentRequest;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DocumentHelper
{
    /**
     * @param Request $request
     * @return Document | null
     */
    public static function save(UploadDocumentRequest $request){
        if($request->hasFile('file')) {

            $file = $request->file('file');
            $path = $file->store('documents');

            $document = Document::create([
                'path' => storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . str_replace(["/", "\\"], DIRECTORY_SEPARATOR, $path),
                'description' => $request->input('description'),
                'title' => $request->input('title')
            ]);

            return $document;
        }
        return null;
    }
}
