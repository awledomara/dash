/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

window.moment =require("moment")
/*Vue.directive('datepicker', {
    bind: function (el) {

        console.log("this",this)
        console.log("binding",el)
        var vm = el.$vm;
        var key = el.expression;

        $(el).datetimepicker({
            format:'d.m.Y H:i',
            lang:'de',
            onChangeDateTime: function (date,el) {
                var event = new Event('input', {bubbles: true})
                $(el).val(moment(date).format('DD.MM.Y H:mm'))
                $(el).dispatchEvent(event)

            }
        });
    },
    update: function (val,el) {

        //$(el).val(val)
    }
});
*/
Vue.component('files-zone', require('./components/FilesZone.vue').default);
Vue.component('notifications-dropdown',require('./components/NotificationsDropDown').default)
Vue.component('messages-dropdown',require('./components/MessagesDropDown').default)

Vue.component('addresses', require('./components/AddressComponent.vue').default);

//Tags component


//Calendar component

Vue.component('job', require('./components/JobComponent.vue').default);
Vue.component('datetimepicker',require('./components/DateTimeComponent.vue').default)
Vue.component('time-frames', require('./components/JobTimeFrameComponent.vue').default);

Vue.component('practice-autocomplete', require('./components/PracticeAutoComplete.vue').default);

Vue.component('tags-input', require('./components/TagComponent.vue').default);

Vue.component('job-fee', require('./components/JobFeeComponent.vue').default);


Vue.component('list-services', require('./components/ListServiceAreaComponent.vue').default);
Vue.component('list-doctors', require('./components/ListDoctorsComponent').default);
Vue.component('list-practices', require('./components/ListPracticesComponent').default);
//Vue.component('calendar-component', require('./components/CalendarComponent.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = new Vue({
    el: '#app',
});
