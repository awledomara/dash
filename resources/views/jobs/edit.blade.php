@extends('layouts.dashboard')
@section('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@voerro/vue-tagsinput@2.0.1/dist/style.css">

@endsection
@section('content')
    <form  method="POST" action="{{route('jobs.update',$job)}}" id="form">
        <div class="card">
            <div class="card-body">
                <h4>Add new job</h4>
                {{method_field('PUT')}}
                {{csrf_field()}}
                @if($errors->count())
                    <div class="alert alert-warning">
                        {{$errors->first()}}
                    </div>

                @endif
                <ul class="nav nav-tabs " role="tablist">
                    <li class="nav-item">
                        <a class="nav-link " id="home-tab" data-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">
                            {{__('Job-Data')}}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="fees-tab" data-toggle="tab" href="#fees" role="tab" aria-controls="fees-1" aria-selected="false">
                            {{__('honorarsteigerung')}}
                        </a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="home-1" role="tabpanel" aria-labelledby="home-tab">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label for="">{{__('Start Date')}}</label>
                                            <datetimepicker
                                                    name="start_date"
                                                    placeholder="Start date"
                                                    type="datetime"
                                                    value="{{$job->start_date}}"
                                            ></datetimepicker>

                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="">{{__('End Date')}}</label>
                                            <datetimepicker
                                                    name="end_date"
                                                    placeholder="End date"
                                                    type="datetime"
                                                    value="{{$job->end_date}}"
                                            ></datetimepicker>
                                        </div>

                                    </div>
                                </div>


                                <practice-autocomplete
                                        s-name="{{optional($job->service_area)->name}}"
                                        s-id="{{optional($job->service_area)->id}}"
                                        p-id="{{optional($job->practice)->id}}"
                                        p-name="{{optional($job->practice)->first_name}} {{optional($job->practice)->last_name}}"
                                        s-name="{{optional($job->practice)->name}}"
                                ></practice-autocomplete>

                                <div class="form-group">
                                    <label for="">{{__('Tags')}}</label>
                                    <tags-input element-id="tags"
                                                element-name="tags"
                                                :tags="{{json_encode($tags)}}"
                                                :tags2="{{json_encode($job->tags)}}"
                                                :typeahead="true"

                                                typeahead-style="dropdown"
                                                :typeahead-activation-threshold="1"

                                    ></tags-input>
                                </div>
                                <div class="form-group">
                                    <label for="">{{__('Notes')}}</label>
                                    <textarea type="text" name="notes" class="form-control" rows="10" style="resize: vertical">{{$job->notes}}</textarea>
                                </div>

                            </div>
                            <div class="col-md-9">
                                <div class="text-center" style="background-color: transparent">
                                    <h2> Job Time frames</h2>
                                </div>
                                <div class="card" style="border-radius:10px; border: 2px solid #eee">
                                    <div class="card-body" style="overflow-y: auto;max-height: 540px;scroll-behavior: smooth" >
                                        <time-frames :tframes="{{json_encode($job->time_frames)}}"></time-frames>
                                    </div>
                                    <div class="card-footer text-center"  style="background-color: transparent">
                                        <div class="row">
                                            <div class="col">
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <td><b>{{__('Additional Remuneration')}}</b></td>
                                                        <td>
                                                            <input class="form-control  @error('additional_remuneration') is-invalid @enderror"
                                                                   name="additional_remuneration"
                                                                   value="{{$job->additional_remuneration}}"
                                                                   type="decimal">
                                                        </td>
                                                        <td>
                                                            <b>€ ab</b>
                                                        </td>
                                                        <td>
                                                            <input class="form-control" name="ab" value="{{$job->ab}}">
                                                        </td>
                                                        <td>
                                                            <b>. Patient</b>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="tab-pane  " id="fees" role="tabpanel" aria-labelledby="fees-tab">

                        <job-fee :l-fees="{{json_encode($job->fees)}}" :job-object="{{json_encode($job)}}">

                        </job-fee>

                    </div>
                </div>


            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col grid-margin">
                        <button class="btn btn-github btn-rounded btn-sm float-right">{{__('Save')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('js')


@endsection
