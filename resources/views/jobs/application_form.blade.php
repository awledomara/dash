@extends('layouts.dashboard')

@section('content')

    <div class="row">
        <div class="col grid-margin">

            @guest
                @if(session("message"))
                    <div class="alert alert-warning">
                        {{session("message")}}
                    </div>
                @endif

                <form action="{{route('jobs.apply_for_doctors',$job)}}" method="POST">
                    {{csrf_field()}}
                    <button class="btn btn-sm btn-google float-right btn-rounded" >{{__('Apply For this job')}}</button>
                </form>


            @else
                    @if(Auth::user()->hasAnyRole(['Admin','SuperAdmin','Agent']))

                        <a class="btn btn-sm btn-github btn-rounded float-right" href="{{route('jobs.edit',$job)}}">{{__('Edit')}}</a>
                        @if($job->practice and $job->service_area and $job->verified)

                            <button class="btn btn-sm btn-google float-right btn-rounded" data-toggle="modal" data-target="#notification-form">{{__('Notify Doctors')}}</button>

                        @endif

                    @elseif(Auth::user()->hasRole('Doctor') and $job->doesntHave("doctor"))
                        <form action="{{route('jobs.apply_for_doctors',$job)}}" method="POST">
                            {{csrf_field()}}
                            <button class="btn btn-sm btn-google float-right btn-rounded" >{{__('Apply For this job')}}</button>
                        </form>

                    @endif
            @endguest
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 ">

            <div class="card rounded border mb-2">

                <div class="card-body">
                    <label class="badge badge-info float-right">{{__('Practice')}}</label>
                    @if($job->practice)
                        <h6 class=""><a href="#"> {{optional($job->practice)->first_name}} {{optional($job->practice)->last_name}} </a></h6>
                        <div class="">

                            <p class="text-muted"><i class="mdi mdi-email-outline text-info"></i>&nbsp;&nbsp;{{$job->practice->primary_email}}</p>
                            <p class="text-muted"><i class="mdi mdi-phone text-info"></i>&nbsp;&nbsp;{{$job->practice->primary_phone}}</p>
                            <p class="text-muted"><i class="mdi mdi-fax text-info"></i>&nbsp;&nbsp;{{$job->practice->primary_phone}}</p>

                        </div>
                    @else
                        <div>
                            <br>
                            <p class="alert alert-info border-0">
                                {{__('Not Set')}}
                            </p>
                        </div>

                    @endif

                </div>
            </div>
            <div class="card rounded border mb-2">

                <div class="card-body">
                    <span class="badge-info badge float-right">{{__('Service Area')}}</span>
                    @if($job->service_area)
                        <h6 class=""><a href="#"> {{optional($job->service_area)->name}} </a></h6>
                        <div class="">
                            <p class="text-muted"><i class="mdi mdi-phone text-info"></i>&nbsp;&nbsp;{{$job->service_area->phone}}</p>
                            <p class="text-muted"><i class="mdi mdi-email-outline text-info"></i>&nbsp;&nbsp;{{$job->service_area->email_kv}}</p>
                            <p class="text-muted"><i class="mdi mdi-google-maps text-info"></i>&nbsp;&nbsp;{{$job->service_area->post_code}}</p>
                        </div>
                    @else
                        <div>
                            <br>
                            <p class="alert alert-info border-0">
                                {{__('Not Set')}}
                            </p>
                        </div>

                    @endif

                </div>
            </div>
            <div class="card rounded border mb-2">
                <div class="card-body p-3">
                    <div class="media">
                        <div class="media-body">
                            <h6 class="mb-1">{{__('Notes')}}</h6>


                            <p class="text-muted mb-0 tx-13">
                                {{$job->notes}}
                            </p>


                        </div>
                    </div>
                </div>
            </div>
            <div class="card rounded border mb-2">
                <div class="card-body p-3">
                    <div class="media">
                        <div class="media-body">
                            <h6 class="mb-1">{{__('Tags')}}</h6>
                            @if($job->tags)
                                @foreach($job->tags as $tag)
                                    <span class="badge badge-primary">{{$tag['value']}}</span>
                                @endforeach
                            @else
                                <p>
                                    {{__('No tags')}}
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-8">
            <div class="card rounded border mb-2">
                <div class="card-body">
                    <h4 class="card-title">  {{__('Job Time Frames')}}</h4>
                    <div class="float-right text-facebook">

                        <i class="mdi mdi-redo"></i>
                        {{__('Last Update')}} {{$job->updated_at->diffforhumans()}}
                    </div>
                    <ul class="bullet-line-list pt-2 mb-0">

                        <li>
                            <h6 class="text-google">{{__('Starts On')}}: {{$job->start_date->format('d.m.Y H:m')}}</h6>

                            <p class="text-muted mb-3 tx-12">
                                <i class="mdi mdi-clock-outline text-facebook"></i>
                                {{__('Created')}} {{$job->created_at->diffforhumans()}}
                            </p>
                        </li>
                        @foreach($job->time_frames as $key=>$frame)
                            <li>
                                <h6>
                                    {{\Carbon\Carbon::createFromFormat('Y-m-d\TH:i:s.uP',$frame['from'])->format('d.m.Y H:m')}} /
                                    {{\Carbon\Carbon::createFromFormat('Y-m-d\TH:i:s.uP',$frame['to'])->format('d.m.Y H:m')}}
                                </h6>
                                <p>  {{$frame['notes']}} </p>
                                <p class="text-muted mb-3 tx-12">
                                    <i class="mdi mdi-bank"></i>
                                    {{$frame['per_hour']}}€ {{__('per hour')}}
                                </p>

                            </li>

                        @endforeach

                        <li>
                            <h6 class="text-google">{{__('Ends On')}}: {{$job->end_date->format('d.m.Y H:m')}}</h6>

                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection
