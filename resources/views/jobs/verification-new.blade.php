@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-header">
            {{__('Practice - Verification New')}}
        </div>
        <strong>{{isset($message)?$message:''}}</strong>
        @if($errors->count())
            <div class="alert alert-warning">
                {{$errors->first()}}
            </div>

        @endif
        <div class="card-body">
            <form action="{{route('practices.verification_new.send')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12">

                        <div class="form-group">
                            <label for="">{{__('PDF HTML')}}</label>
                            <textarea name="content" id="pdf-html" cols="30" rows="10"
                                      class="form-control"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-github float-right">{{__('Send PDF')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
        $(document).ready(function () {

            tinymce.init({
                selector: '#pdf-html',
                menubar: 'file edit view insert format tools table tc help',
                plugins: 'code print preview fullpage importcss searchreplace autolink autosave save directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
                toolbar: 'code | undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | numlist bullist checklist | fontselect fontsizeselect formatselect | outdent indent | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | link anchor codesample | a11ycheck ltr rtl | showcomments addcomment', // image
                height: 400,
                image_title: true,

                images_upload_url: 'postAcceptor.php',
                images_upload_handler: function (blobInfo, success, failure) {
                    let file = {
                        file_name: blobInfo.filename(),
                        file_type: blobInfo.filename().split('.').pop(),
                        value: blobInfo.base64(),
                    };
                    $.post('{{route('microservices.fileupload')}}', file)
                        .done(function (res) {
                            console.log("res:", res);
                            success(res);

                        });
                },
            });
        });
    </script>
@endsection