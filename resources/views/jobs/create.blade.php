@extends('layouts.dashboard')
@section('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@voerro/vue-tagsinput@2.0.1/dist/style.css">
    <link rel="stylesheet" href="{{asset("css/jquery.datetimepicker.min.css")}}">
@endsection
@section('content')

    <form method="POST" action="{{route('jobs.store')}}" id="form" autocomplete="off">
        <div class="card">
            <div class="card-body">
                <h4>Add new job</h4>
                <button class="btn btn-github btn-rounded btn-rounded btn-sm float-right">{{__('Save')}}</button>
                {{csrf_field()}}
                @if($errors->count())
                    <div class="alert alert-warning">
                        {{$errors->first()}}
                    </div>

                @endif
                <ul class="nav nav-tabs " role="tablist">
                    <li class="nav-item">
                        <a class="nav-link " id="home-tab" data-toggle="tab" href="#home-1" role="tab"
                           aria-controls="home-1" aria-selected="true">
                            {{__('Job-Data')}}
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="fees-tab" data-toggle="tab" href="#fees" role="tab"
                           aria-controls="fees-1" aria-selected="false">
                            {{__('honorarsteigerung')}}
                        </a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="home-1" role="tabpanel" aria-labelledby="home-tab">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group" style="margin-bottom: 2px">
                                            <label for="">{{__('Start Date')}}</label>
                                            <input type="text" class="form-control dtp"
                                                   autocomplete="off"
                                                   required
                                                   name="start_date"
                                                   id="start_date"
                                                   placeholder="{{__('start date')}}"
                                                   value="{{old("end_date")}}">

                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group" style="margin-bottom: 2px">
                                            <label for="">{{__('End Date')}}</label>
                                            <input type="text" class="form-control dtp"
                                                   required
                                                   autocomplete="off"
                                                   name="end_date" id="end_date"
                                                   placeholder="{{__('end date')}}"
                                                   value="{{old("end_date")}}">
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                      <span class="text-muted text-facebook ">
                                        {{__('Hours')}}: <i id="total-hours"></i>
                                      </span>
                                    </div>
                                </div>

                                <practice-autocomplete></practice-autocomplete>


                                <div class="form-group">
                                    <label for="">{{__('Tags')}}</label>
                                    <tags-input element-id="tags"
                                                element-name="tags"
                                                :tags="{{json_encode($tags)}}"
                                                :tags2="[]"
                                                :typeahead="true"

                                                typeahead-style="dropdown"
                                                :typeahead-activation-threshold="1"

                                    ></tags-input>
                                </div>
                                <div class="form-group">
                                    <label for="">{{__('Notes')}}</label>
                                    <textarea type="text" name="notes" class="form-control" rows="10"
                                              style="resize: vertical">{{old('notes')}}</textarea>
                                </div>

                            </div>
                            <div class="col-md-8" style="font-size: 10px!important;">
                                <div class="text-center" style="background-color: transparent">
                                    <h2 class="text-github"> {{__('Job time frames')}}</h2>
                                </div>
                                <div class="card"
                                     style="border-radius:10px; border: 2px solid #eee;background-color: #fafafa">
                                    <div class="card-body" style="overflow-y: auto;max-height: 540px;scroll-behavior: smooth"   id="time_frames_wrapper">

                                    </div>
                                    <div class="card-footer text-center"
                                         style="background-color: transparent;border: 0px">
                                        <div class="row">
                                            <div class="col grid-margin">
                                                <div class="btn rounded btn-sm btn-github float-right" id="btn-add-time-frame"
                                                     >{{__('Add New Time Frame')}}</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"
                                                              id="basic-addon1">{{__('Additional Remuneration')}}</span>
                                                    </div>
                                                    <input class="form-control  @error('additional_remuneration') is-invalid @enderror"
                                                           name="additional_remuneration" type="number">
                                                    <div class="input-group-prepend">
                                                                       <span class="input-group-text" id="basic-addon1">
                                                                       {{__('€ ab	')}}
                                                                       </span>
                                                    </div>
                                                    @error('additional_remuneration')
                                                    <span class="invalid-feedback" role="alert">
                                                                                 <strong>{{ $message }}</strong>
                                                               </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group ">
                                                    <input class="form-control  @error('ab') is-invalid @enderror"
                                                           name="ab">
                                                    <div class="input-group-prepend">
                                                                   <span class="input-group-text" id="basic-addon1">
                                                                   . {{__('Patient')}}
                                                                   </span>
                                                    </div>
                                                    @error('ab')
                                                    <span class="invalid-feedback" role="alert">
                                                                                 <strong>{{ $message }}</strong>
                                                                            </span>
                                                    @enderror
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane  " id="fees" role="tabpanel" aria-labelledby="fees-tab">
                        <div class="row">
                            <div class="col-md-3">

                            </div>
                            <div class="col-md-6">
                                <div class="jumbotron text-center">
                                    Please save the Job data to proceed
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col grid-margin">
                        <button class="btn btn-github btn-rounded btn-rounded btn-sm float-right">{{__('Save')}}</button>
                    </div>
                </div>
            </div>
        </div>


    </form>
    <div style="display: none">
        <div data-id="0" id="time-frame-0" class="time-frame">
              <span class="mdi mdi-trash-can-outline float-right" style="font-size: 16px; cursor:pointer;" onclick="this.parentNode.parentNode.removeChild(this.parentNode);">

                </span>
            <div class="row">

                <div class="col">
                    <div class="input-group">
                        <input type="text" class="form-control dtp dt-from" name="from[]" id="dt-from-0"
                               required placeholder="{{__('from date')}}">
                        <input type="text" class="form-control dtp dt-to " name="to[]" id="dt-to-0" required
                               placeholder="{{__('to date')}}">
                        <input type="number" class="form-control" name="per_hour[]" required
                               placeholder="{{__('amount per hour')}}">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group mt-1">
                                    <textarea name="_notes[]" cols="30" rows="3"
                                              class="form-control"
                                              placeholder="{{__('additional comments')}}"
                                    ></textarea>
                    </div>
                </div>

            </div>
            <div class="dashed-divider mb-4"></div>
        </div>
    </div>

@endsection
@section('js')

    <script src="{{asset("js/datetimepicker.js")}}"></script>
    <script type="text/javascript">

        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('New message to ' + recipient)
            modal.find('.modal-body input').val(recipient)
        })

        function convertMS(milliseconds) {
            var day, hour, minute, seconds;
            seconds = Math.floor(milliseconds / 1000);
            minute = Math.floor(seconds / 60);
            seconds = seconds % 60;
            hour = Math.floor(minute / 60);
            minute = minute % 60;
            day = Math.floor(hour / 24);

            return {
                day: day,
                hour: hour,
                minute: minute,
                seconds: seconds
            };
        }

        function diff(dt1, dt2) {
            if (dt1 != null && dt2 != null) {
                let time1 = dt1.getTime();
                let time2 = dt2.getTime()
                if (time1 > time2) {
                    var time = time1 - time2
                    return convertMS(time);
                }

                return {
                    hour: 0
                }

            }

            return ''

        }

        $(document).ready(function () {

            let time_frame_wrapper = $('#time_frames_wrapper')

            $('#btn-add-time-frame').click(function () {
                var $div = $('#time-frame-0')


                $div.clone().appendTo('#time_frames_wrapper');
                var new_div = $('.time-frame').last();
                let id = parseInt(new_div.data('id'))
                new_div.data('id', id)
                new_div.find(".dt-to").attr("id", "dt-to-" + (id))
                new_div.find(".dt-from").attr("id", "dt-from-" + (id))
                $("[id^='dt-']").datetimepicker({
                    format: 'd.m.Y H:i',
                    onShow: function (ct) {
                        this.setOptions({
                            maxDate:$('#end_date').val()?new Date($('#end_date').datetimepicker('getValue')):false,
                            minDate:$('#start_date').val()?new Date($('#start_date').datetimepicker('getValue')):false,
                            maxDate:$('#end_date').val()?new Date($('#end_date').datetimepicker('getValue')):false,
                        })
                    },
                    onChangeDateTime(ct) {

                        var time = diff(new Date($('#end_date').datetimepicker('getValue')), ct)
                        $("#total-hours").html(time.hour + ' hour(s)')

                    }

                })
                $("#time_frames_wrapper").scrollTop($("#time_frames_wrapper")[0].scrollHeight)



            })

            $('#start_date').datetimepicker({
                format: 'd.m.Y H:i',
                minDate:new Date(),
                onShow: function (ct) {
                    this.setOptions({
                         maxDate:$('#end_date').val()?new Date($('#end_date').datetimepicker('getValue')):false
                    })
                },
                onChangeDateTime(ct) {

                    var time = diff(new Date($('#end_date').datetimepicker('getValue')), ct)
                    $("#total-hours").html(time.hour + ' hour(s)')

                }

            });
            $('#end_date').datetimepicker({
                format: 'd.m.Y H:i',

                onShow(ct) {
                    this.setOptions({
                        minDate:new Date($('#start_date').datetimepicker('getValue'))
                    })
                    var time = diff(ct, new Date($('#start_date').datetimepicker('getValue')))
                    $("#total-hours").html(time.hour + ' hour(s)')
                }
            });

        })

    </script>
@endsection
