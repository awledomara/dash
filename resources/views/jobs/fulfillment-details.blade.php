@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-header">
            {{__('Job Fulfillment details')}}
        </div>
        <strong>{{isset($message)?$message:''}}</strong>
        <div class="card-body">

            <div class="row">
                <div class="col-md-1">{{__("Start Date")}}:</div>
                <div class="col-md-2">{{$job->start_date->format('d.m.Y H:m')}}</div>
                <div class="col-md-1">{{__("End Date")}}:</div>
                <div class="col-md-2">{{$job->end_date->format('d.m.Y H:m')}}</div>
            </div>
            <div class="row">
                <div class="col-md-1">{{__("Service Area")}}:</div>
                <div class="col-md-2">{{$job->service_area->name}}</div>
                <div class="col-md-1">{{__("Hours")}}:</div>
                <div class="col-md-2">{{$job->total_hours()}} h</div>
            </div>
            <div class="row">
                <div class="col-md-1">{{__("Doctor Name")}}:</div>
                <div class="col-md-2">{{$job->doctor->user->first_name}} {{$job->doctor->user->last_name}}</div>
                <div class="col-md-1">{{__("Practice Name")}}:</div>
                <div class="col-md-2">{{$job->practice->first_name}} {{$job->practice->last_name}}</div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <form action="{{route('jobs.fulfillment.send-pdf',$job->id)}}" method="get">
                        <button type="submit" class="btn btn-github float-right">{{__('Send PDF')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

@endsection