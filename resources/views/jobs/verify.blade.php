@extends('layouts.dashboard')
@section("head")
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/af-2.3.3/b-1.5.6/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            @if($errors->count())
                <div class="alert alert-warning">
                  {{__('Please Choose a practice account and one or more jobs first')}}
                </div>

            @endif

            <div class="row">
                <div class="col">
                         @if($practice)
                             <p class="alert alert-info  border-0">
                                 <span class="mdi mdi-information"></span>
                                 <span>Displaying unverified jobs for practice <a href="{{route("practices.show",$practice)}}">  {{$practice->first_name}} {{$practice->last_name}}</a></span>
                             </p>

                         @endif
                </div>
            </div>

                <div class="row">
                    <div class="col-md-3">
                            <div style="padding:15px; box-shadow: 0px -1px 20px #eeeeeeba;">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="q" id="q" value="" placeholder="search for practices">
                                </div>
                                <div id="list-practices" style="max-height: 460px; overflow-y: auto; width: 100%; " >
                                </div>

                            </div>

                    </div>
                    <div class="col-md-9 table-responsive" style="">
                        @if($practice)
                            <form action="{{route('jobs.send_notification_to_practice',$practice)}}" method="POST">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col">

                                           <button type="submit" class="btn btn-sm btn-google float-right">{{__('Send Verification To practice')}}</button>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table id="data-table-jobs" class="table table-striped" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>
                                                {{__("Start Date")}}
                                            </th>
                                            <th>
                                                {{__("End Date")}}
                                            </th>
                                            <th>
                                                {{__('Service Area')}}
                                            </th>
                                            <th>
                                                {{__('Verified')}}
                                            </th>

                                            <th>

                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </form>
                        @else
                            <div class="jumbotron" style="vertical-align: middle;display: table-cell">
                                <div class="container">
                                    <h1 class="display-4">{{__('Please Choose a practice account first')}}</h1>
                                    <p class="lead">{{__("Here w'll load the data of unverified jobs and missions for the selected practice account")}}</p>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>



        </div>
    </div>



@endsection
@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/af-2.3.3/b-1.5.6/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>



    <script>
        $(document).ready( function () {

            @if($practice)
              $('#data-table-jobs').DataTable({
                "processing": true,
                "searching": false,
                "serverSide": true,
                "ajax": "{{ route('datatable.jobs.index')}}?verified=0&p_id={{$practice->id}}&verified=0",
                "columns": [
                    {
                        "data":"id",
                        "name":"id",
                        "render":function(d){
                            console.log(d)
                          return  `<div class="form-check mb-0 mt-0">
                                 <label class="form-check-label">
                                 <input type="checkbox" name="jobs_ids[]" class="form-check-input" value="${d}" >
                               <i class="input-helper"></i></label>
                            </div>`
                        }

                    },
                    {   "data": "start_date" ,
                        'name':"start_date",
                        "render": function(d) {

                            return moment(d).format("D.MM.Y H:mm")

                        }

                    },
                    {   "data": "end_date" ,
                        'name':"end_date",
                        "render": function(d) {

                            return moment(d).format("D.MM.Y H:mm")

                        }

                    },
                    {   "data": "service_area" ,
                        'name':"service_area.name",
                        "defaultContent":"Not Set",
                        "render": function(d) {

                            if(d!=null){
                                return  d.name
                            }

                        }

                    },
                    {   "data": "verified" ,
                        'name':"verified",
                        "render": function(d,vue,full) {

                            if(d!=null){

                                    if(full.verification_sent_at){
                                           return ` <p class="text-muted">
                                                {{__('Email sent at')}} ${moment(full.verification_sent_at).format("d.MM.Y H:mm")}
                                              </p>
                                             `


                                    }else{
                                       return '<span class="mdi mdi-check-circle"></span>No'
                                    }

                                return '<span class="mdi mdi-check-circle"></span>yes'
                            }

                        }

                    },

                    {
                        "data":null,
                        "name":"id",
                        "render":function (d) {

                            return '<a href="{{route('jobs.index')}}/'+d.id+'">{{__('Details')}}</a>';
                        }
                    }


                ]
            });
            @endif
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){

            $('#q').keyup(function(){
                loadPractices()
            })
            let query=$('#q').val();
            let list_practices=[]
            let append_list=function(){

                $("#list-practices").html('<p class="alert alert-warning text-center">Loading ...</p>')
                let dom_list_group=''

                    if(list_practices.length>0){

                        list_practices.forEach(practice=>{
                            dom_list_group+=
                                `<div class=""  style="padding:8px">
                                <a class="${practice.id} text-decoration-none text-github" href="{{route('practices.jobs_view')}}?p_id=${practice.id}">
                                    ${practice.first_name} ${practice.last_name}<br>${practice.primary_email}}
                                </a>
                                 </div>
                                   <div class="dashed-divider"></div>
                              `
                        })
                    }else{
                        $("#list-practices").html('<p class="alert alert-warning text-center">No data available</p>')
                        return ;
                    }



                $("#list-practices").html(dom_list_group)
                return dom_list_group
            }
            let currentRequest=null;
            let loadPractices=function(){
                if(currentRequest){
                    currentRequest.abort()
                }
                currentRequest=$.ajax({
                    url: `{{route('microservices.practices.search')}}?q=${$("#q").val()}`,
                    success: function (_data) {

                        list_practices = _data;

                        append_list()

                    }
                })
            }

            loadPractices()


        })
    </script>

@endsection
