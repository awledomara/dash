@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-body table-responsive">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>
                        {{__("Start Date")}}
                    </th>
                    <th>
                        {{__("End Date")}}
                    </th>
                    <th>
                        {{__("Service Area")}}
                    </th>
                    <th>
                        {{__("Doctor Name")}}
                    </th>
                    <th>
                        {{__("Practice Name")}}
                    </th>
                    <th>
                        {{__("Actions")}}
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($jobs as $job)
                    <tr>
                        <td>
                            {{$job->start_date->format('d.m.Y H:m')}}
                        </td>
                        <td>
                            {{$job->end_date->format('d.m.Y H:m')}}
                        </td>
                        <td>
                            {{$job->service_area->name}}
                        </td>
                        <td>
                            {{$job->doctor->user->first_name}} {{$job->doctor->user->last_name}}
                        </td>
                        <td>
                            {{$job->practice->first_name}} {{$job->practice->last_name}}
                        </td>
                        <td>
                            <a href="{{route('jobs.fulfillment.show',$job->id)}}">{{__("Details")}}</a>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="6" class="text-center">
                        {{$jobs->links()}}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('js')

@endsection