@extends('layouts.dashboard')
@section("head")
<style>
    .modal-dialog {
        width: 100%;
        padding: 0;
    }

    .modal-content {
        height: auto;
        min-height: 560px;
        border-radius: 0;
    }
</style>
@endsection
@section('content')

           <div class="row">
               <div class="col grid-margin">


                   @if(Auth::user()->hasAnyRole(['Admin','SuperAdmin','Agent']))


                       <a class="btn btn-sm btn-github btn-rounded float-right" href="{{route('jobs.edit',$job)}}">{{__('Edit')}}</a>
                       @if($job->practice and $job->service_area and $job->verified)

                            <button class="btn btn-sm btn-google float-right btn-rounded" data-toggle="modal" data-target="#notification-form">{{__('Notify Doctors')}}</button>

                       @endif

                   @elseif(Auth::user()->hasRole('Doctor') and $job->doesntHave("doctor"))
                       <form action="{{route('jobs.apply_for_doctors',$job)}}" method="POST">
                           {{csrf_field()}}
                           <button class="btn btn-sm btn-google float-right btn-rounded" >{{__('Apply For this job')}}</button>
                       </form>

                   @endif

               </div>
           </div>
            <div class="row">
                <div class="col grid-margin">
                    @if($job->verification_sent_at)
                        <span class="badge badge-info"></span>
                        @if($job->verified==1)
                            <span class="badge badge-info"> <i class="mdi mdi-check"></i> {{__('Verified at ')}}&nbsp;{{$job->verified_at->format('d.m.Y H:m')}}</span>
                        @else
                            <form action="{{route('jobs.send_notification_to_practice',$job->practice)}}" method="POST" id="form-confirmation">
                                {{csrf_field()}}
                                <input type="hidden" name="jobs_ids[]" value="{{$job->id}}">
                            <p class="alert alert-warning border-0">
                                <i class="mdi mdi-information-outline"></i> &nbsp;

                                    {{__('Job is waiting the practice confirmation')}} ,
                                    {{__('an email of confirmation already sent to the practice  at')}} {{$job->verification_sent_at->format('d.m.Y H:m')}},
                                {{__('click here to')}}  <a href="#"  onclick="document.getElementById('form-confirmation').submit()">{{__('resend confirmation')}}</a>
                                {{__('or')}} {{__('click here to')}} <a href="{{route('practices.confirm_mission',[$job->practice,$job])}}">{{__('confirm the job/mission')}}</a>

                            </p>
                            </form>



                        @endif

                    @else
                        <form action="{{route('jobs.send_notification_to_practice',$job->practice)}}" method="POST" id="form-confirmation">
                            {{csrf_field()}}
                            <input type="hidden" name="jobs_ids[]" value="{{$job->id}}">
                            <p class="alert alert-warning border-0">
                                <i class="mdi mdi-information-outline"></i> &nbsp;


                                {{__('Practice is not notified')}} , {{__('please click here to')}}
                                <a href="#" onclick="document.getElementById('form-confirmation').submit()">
                                    {{__('Send the Verification email')}}
                                </a>


                            </p>

                        </form>

                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 ">

                    <div class="card rounded border mb-2">

                        <div class="card-body">
                            <label class="badge badge-info float-right">{{__('Practice')}}</label>
                            @if($job->practice)
                                <h6 class=""><a href="{{route("practices.show",$job->practice)}}"> {{optional($job->practice)->first_name}} {{optional($job->practice)->last_name}} </a></h6>
                                <div class="">

                                    <p class="text-muted"><i class="mdi mdi-email-outline text-info"></i>&nbsp;&nbsp;{{$job->practice->primary_email}}</p>
                                    <p class="text-muted"><i class="mdi mdi-phone text-info"></i>&nbsp;&nbsp;{{$job->practice->primary_phone}}</p>
                                    <p class="text-muted"><i class="mdi mdi-fax text-info"></i>&nbsp;&nbsp;{{$job->practice->primary_phone}}</p>

                                </div>
                            @else
                                <div>
                                    <br>
                                    <p class="alert alert-info border-0">
                                        {{__('Not Set')}}
                                    </p>
                                </div>

                            @endif

                        </div>
                    </div>
                    <div class="card rounded border mb-2">

                        <div class="card-body">
                            <span class="badge-info badge float-right">{{__('Service Area')}}</span>
                            @if($job->service_area)
                                <h6 class=""><a href="{{route("services.show",$job->service_area)}}"> {{optional($job->service_area)->name}} </a></h6>
                                <div class="">
                                    <p class="text-muted"><i class="mdi mdi-phone text-info"></i>&nbsp;&nbsp;{{$job->service_area->phone}}</p>
                                    <p class="text-muted"><i class="mdi mdi-email-outline text-info"></i>&nbsp;&nbsp;{{$job->service_area->email_kv}}</p>
                                    <p class="text-muted"><i class="mdi mdi-google-maps text-info"></i>&nbsp;&nbsp;{{$job->service_area->post_code}}</p>
                                </div>
                            @else
                                <div>
                                    <br>
                                    <p class="alert alert-info border-0">
                                        {{__('Not Set')}}
                                    </p>
                                </div>

                            @endif

                        </div>
                    </div>
                    <div class="card rounded border mb-2">
                        <div class="card-body p-3">
                            <div class="media">
                                <div class="media-body">
                                    <h6 class="mb-1">{{__('Notes')}}</h6>


                                        <p class="text-muted mb-0 tx-13">
                                            {{$job->notes}}
                                        </p>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card rounded border mb-2">
                        <div class="card-body p-3">
                            <div class="media">
                                <div class="media-body">
                                    <h6 class="mb-1">{{__('Tags')}}</h6>
                                    @if($job->tags)
                                        @foreach($job->tags as $tag)
                                            <span class="badge badge-primary">{{$tag['value']}}</span>
                                        @endforeach
                                    @else
                                        <p>
                                            {{__('No tags')}}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-8">
                    <div class="card rounded border mb-2">
                        <div class="card-body">
                            <h4 class="card-title">  {{__('Job Time Frames')}}</h4>
                            <div class="float-right text-facebook">

                                <i class="mdi mdi-redo"></i>
                                {{__('Last Update')}} {{$job->updated_at->diffforhumans()}}
                            </div>
                            <ul class="bullet-line-list pt-2 mb-0">

                                <li>
                                    <h6 class="text-google">{{__('Starts On')}}: {{$job->start_date->format('d.m.Y H:m')}}</h6>

                                    <p class="text-muted mb-3 tx-12">
                                        <i class="mdi mdi-clock-outline text-facebook"></i>
                                        {{__('Created')}} {{$job->created_at->diffforhumans()}}
                                    </p>
                                </li>
                                @foreach($job->time_frames as $key=>$frame)
                                    <li>
                                        <h6>
                                            {{\Carbon\Carbon::createFromFormat('Y-m-d\TH:i:s.uP',$frame['from'])->format('d.m.Y H:m')}} /
                                            {{\Carbon\Carbon::createFromFormat('Y-m-d\TH:i:s.uP',$frame['to'])->format('d.m.Y H:m')}}
                                        </h6>
                                        <p>  {{$frame['notes']}} </p>
                                        <p class="text-muted mb-3 tx-12">
                                            <i class="mdi mdi-bank"></i>
                                            {{$frame['per_hour']}}€ {{__('per hour')}}
                                        </p>

                                    </li>

                                @endforeach

                                <li>
                                    <h6 class="text-google">{{__('Ends On')}}: {{$job->end_date->format('d.m.Y H:m')}}</h6>

                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>


           <div class="modal" id="notification-form" tabindex="-1" role="dialog" aria-labelledby="notification-form" aria-hidden="true">
               <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
                   <div class="modal-content">

                       <form action="{{route('jobs.send_notifications',$job)}}" method="POST">
                                   <div class="modal-header">
                                       <h5 class="modal-title" id="exampleModalLongTitle">{{__('Please choose the list of doctors to notify')}}</h5>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                           <span aria-hidden="true">&times;</span>
                                       </button>
                                   </div>
                                   <div class="modal-body">

                                           {{csrf_field()}}

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <list-doctors l-doctors="{{json_encode($doctors)}}"></list-doctors>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="card rounded border mb-2">
                                                                <div class="card-body p-3">
                                                                    <strong class="text-linkedin ">{{__('Practice')}}</strong><br>
                                                                    <div class="media">

                                                                        <div class="media-body">

                                                                            @if($job->practice)
                                                                                <h6 class="mb-1">{{optional($job->practice)->first_name}} {{optional($job->practice)->last_name}}</h6>
                                                                                <p class="text-muted mb-0 tx-13">

                                                                                    <b>{{__('Service Area')}}</b>:  {{optional($job->service_area)->name}}<br>
                                                                                    <b>{{__('Phone')}}</b>:  {{optional($job->practice)->primary_phone}}<br>
                                                                                    <b>{{__('Email')}}</b>:  {{optional($job->practice)->primary_email}}<br>

                                                                                </p>
                                                                            @else
                                                                                <p>
                                                                                    {{__('Not Set')}}
                                                                                </p>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="card rounded border mb-2">
                                                                <div class="card-body p-3">
                                                                    <strong class="text-linkedin float-right">{{__('Service Area')}}</strong><br>
                                                                    <div class="media">

                                                                        <div class="media-body">

                                                                            @if($job->service_area)
                                                                                <a class="mb-1"><a href="{{route("services.show",$job->service_area)}}"> {{optional($job->service_area)->name}} </a></h6>

                                                                                <p class="text-muted mb-0 tx-13">
                                                                                    <b>{{__('Designation')}}</b>:  {{optional($job->service_area)->designation}}<br>
                                                                                    <b>{{__('Phone')}}</b>:  {{optional($job->service_area)->phone}}<br>
                                                                                    <b>{{__('Email Lst')}}</b>:  {{optional($job->service_area)->email_lst}}<br>
                                                                                    <b>{{__('Email kv')}}</b>:  {{optional($job->service_area)->email_kv}}<br>
                                                                                </p>

                                                                            @else
                                                                                <p>
                                                                                    {{__('Not Set')}}
                                                                                </p>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="card rounded border mb-2">
                                                        <div class="card-body p-3">
                                                            <div class="media">
                                                                <div class="media-body">
                                                                    <h6 class="mb-1">{{__('Notes')}}</h6>


                                                                    <p class="text-muted mb-0 tx-13">
                                                                        {{$job->notes}}
                                                                    </p>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card rounded border mb-2">
                                                        <div class="card-body p-3">
                                                            <div class="media">
                                                                <div class="media-body">
                                                                    <h6 class="mb-1">{{__('Tags')}}</h6>
                                                                    @if($job->tags)
                                                                        @foreach($job->tags as $tag)
                                                                            <span class="badge badge-primary">{{$tag['value']}}</span>
                                                                        @endforeach
                                                                    @else
                                                                        <p>
                                                                            {{__('No tags')}}
                                                                        </p>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card rounded border mb-2">
                                                        <div class="card-body">
                                                            <h4 class="card-title">  {{__('Job Time Frames')}}</h4>
                                                            <div class="float-right text-facebook">

                                                                <i class="mdi mdi-redo"></i>
                                                                {{__('Last Update')}} {{$job->updated_at->diffforhumans()}}
                                                            </div>
                                                            <ul class="bullet-line-list pt-2 mb-0">

                                                                <li>
                                                                    <h6 class="text-google">{{__('Starts On')}}: {{$job->start_date->format('d.m.Y H:m')}}</h6>

                                                                    <p class="text-muted mb-3 tx-12">
                                                                        <i class="mdi mdi-clock-outline text-facebook"></i>
                                                                        {{__('Created')}} {{$job->created_at->diffforhumans()}}
                                                                    </p>
                                                                </li>
                                                                @foreach($job->time_frames as $key=>$frame)
                                                                    <li>
                                                                        <h6>
                                                                            {{\Carbon\Carbon::createFromFormat('Y-m-d\TH:i:s.uP',$frame['from'])->format('d.m.Y H:m')}} /
                                                                            {{\Carbon\Carbon::createFromFormat('Y-m-d\TH:i:s.uP',$frame['to'])->format('d.m.Y H:m')}}
                                                                        </h6>
                                                                        <p>  {{$frame['notes']}} </p>
                                                                        <p class="text-muted mb-3 tx-12">
                                                                            <i class="mdi mdi-bank"></i>
                                                                            {{$frame['per_hour']}}€ {{__('per hour')}}
                                                                        </p>

                                                                    </li>

                                                                @endforeach

                                                                <li>
                                                                    <h6 class="text-google">{{__('Ends On')}}: {{$job->end_date->format('d.m.Y H:m')}}</h6>

                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                   </div>
                                   <div class="modal-footer">
                                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       <button type="submit" class="btn btn-primary" >{{__('Send mail')}}</button>
                                   </div>
                       </form>
                  </div>

               </div>
           </div>

@endsection
