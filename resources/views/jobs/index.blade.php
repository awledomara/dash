@extends('layouts.dashboard')
@section('head')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/af-2.3.3/b-1.5.6/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />

    <style>
        .filter-wrapper.form-group.form-control {
            border-radius: 4px!important;
            border: 1px solid #eee !important;
        }

    </style>
@endsection
@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body ">
                    <form action="{{route("jobs.list")}}" method="get" id="form-filter">
                    <div class="filter-wrapper row" style="">
                        <div class="col-md-1 ">
                            <div class="form-group jobs_length">
                                <label for="">{{__('Show')}}</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-bottom: 1px">
                                <label for="">{{__('Practice')}}</label>
                                <select name="p_id" id="p_id" class="form-control">
                                    <option value=""></option>
                                    @if($practice)
                                        <option value="{{$practice->id}} " selected>{{$practice->fullName()}}</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-bottom: 1px">
                                <label for="">{{__('Service Area')}}</label>
                                <select name="s_id" id="s_id" class="form-control">

                                    @if($practice)

                                        @foreach($practice->service_areas as $service_area)
                                          <option value="{{$service_area->id}}" selected>{{$service_area->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group" style="margin-bottom: 1px">
                                <label for="">{{__('Job Status')}}</label>
                                <select name="status" id="j_status" class="form-control">
                                    <option value="" {{$status===null?'selected':''}}>{{__('All')}}</option>
                                    <option value="0" {{$status==='0'?'selected':''}}>{{__('Unverified Jobs')}}</option>
                                    <option value="1" {{$status==1?'selected':''}}>{{__('Verified Jobs')}}</option>
                                    <option value="2" {{$status==2?'selected':''}}>{{__('Assigned Jobs')}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                                <div class="form-group search-field">
                                    <label for="">{{__('Search')}}</label>
                                </div>
                        </div>


                    </div>
                    </form>
                    <table class="table  table-striped table-hover" id="jobs-datatable" style="width: 100%; ">
                             <thead>
                             <tr>
                                 <th>

                                 </th>
                                 <th>
                                     {{__('Start Date')}}
                                 </th>
                                 <th>
                                     {{__('End Date')}}
                                 </th>

                                 <th>
                                     {{__('Practice')}}
                                 </th>
                                 <th>
                                     {{__('Service Area')}}
                                 </th>
                                 <th>
                                     {{__('status')}}
                                 </th>
                                 <th>
                                     {{__('Updated At')}}
                                 </th>
                                 <th>

                                 </th>
                             </tr>
                             </thead>
                             <tbody>

                             </tbody>
                         </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/af-2.3.3/b-1.5.6/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <!--
   <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
 -->
    <script>

        var chunck=function(str, n) {
            var ret = [];
            var i;
            var len;

            for(i = 0, len = str.length; i < len; i += n) {
                ret.push(str.substr(i, n))
            }

            return ret
        };
        $(document).ready( function () {
            let p_name=$('#p-name').val()
            $('#jobs-datatable').DataTable({
                "processing": true,
                "iDisplayLength": 50,
                "serverSide": true,
                @if($practice)
                "ajax": "{{ route('datatable.jobs.index') }}?p_id={{$practice->id}}&status={{$status}}&s_id={{$service?$service->id:''}}",
                @else
                "ajax": "{{ route('datatable.jobs.index') }}?status={{$status}}",
                @endif
                "columns": [
                    {
                        "data":"id",
                        "name":"id",
                        "render":function(d){

                            return  `<div class="form-check mb-0 mt-0">
                                 <label class="form-check-label">
                                 <input type="checkbox" name="jobs_ids[]" class="form-check-input" value="${d}" >
                               <i class="input-helper"></i></label>
                            </div>`
                        }

                    },
                    {   "data": "start_date" ,
                        'name':"start_date",
                        "render": function(d) {

                            return moment(d).format("DD.MM.YYYY HH:mm ")

                        }

                    },
                    {   "data": "end_date" ,
                        'name':"end_date",
                        "render": function(d) {

                            return moment(d).format("DD.MM.YYYY HH:mm ")

                        }

                    },
                    {
                        "data": "practice" ,
                        'name':"practice.first_name",
                        "defaultContent":"<i class='text-google'>Not Set</i>",
                        "render": function(d) {

                           if(d!=null){

                              return "<a href='{{route('practices.index')}}/"+d.id+"/edit'>"+d.first_name + ' ' + d.last_name+"</a>"

                           }

                        }

                    },
                    {   "data": "service_area",
                        'name':"service_area.name",
                        "defaultContent":"<i class='text-google'>Not Set</i>",
                        "render": function(d) {

                            if(d!=null){
                                return  d.name
                            }

                        }

                    },
                    {
                        data:"status",
                        name:"status",
                        defaultContent:" <i class='mdi mdi-check'></i>",
                        "render":function(d){
                            /*
                            *
                            * const PENDING=0;//verification was send to Practice and wait the confirmation
                            *  const Verified=1;// Practice verified the mission
                            *     const Assigned=2
                            * */
                           if(d!=null){

                                if(status==='0') {
                                    return "Send Verification to practice?"
                                }
                                if(d==='1'){
                                    return "<span class='badge badge-info'><i class='mdi mdi-check'> {{__('Confirmed')}} </span>"
                                }
                               if(d==='2'){
                                   return "<span class='badge badge-danger'><i class='mdi mdi-check'>{{__('Assigned')}} </span>"
                               }

                           }
                          return `<button class="btn btn-xs btn-facebook">{{__('Send Verification')}}</button>`
                        }
                    },
                    {
                        "data":"updated_at",
                        "render":function (d) {
                            return moment(d).format("DD.MM.YYYY HH:mm")
                        }
                    },
                    {
                        "data":null,
                        "name":"id",
                        "render":function (d) {

                            var actions= '<a class=""  href="{{route('jobs.index')}}/'+d.id+'">{{__('Details')}}</a>';





                            return actions






                        }
                    }


                ]
            });

            $('#p_id').select2({
                theme: "classic",
                allowClear: true,
                placeholder: "{{__('Select a practice')}}",
                ajax: {
                    url: '{{route('microservices.practices.search')}}',
                    data: function (params) {
                        var query = {
                            q: params.term,

                        }

                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.first_name+' '+item.last_name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });


            $('#j_status').select2({
                theme:"classic",
                 minimumResultsForSearch: -1
            })

            $('#s_id').select2({
                theme:"classic",
                placeholder:'{{__('Select Service Area')}}',
                allowClear: true,

            })
            $('.jobs_length').append($("#jobs-datatable_length select"))

            $('.search-field').append($("#jobs-datatable_filter input"))
            $('#jobs-datatable_wrapper div:first').remove()
            $('.jobs_length select').removeClass("custom-select").removeClass("custom-select-sm").css('padding',"0px")
            $('[name="jobs-datatable_length"]').select2({
                theme:"classic",
                minimumResultsForSearch: -1
            })
            $('#s_id').on('select2:select', function (e) {
                $('#form-filter').submit()
            });
            $('#p_id').on('select2:select', function (e) {
               $('#form-filter').submit()
            });
            $('#p_id').on('select2:clear', function (e) {
                $('#form-filter').submit()
            });
            $('#j_status').on('select2:select', function (e) {
                $('#form-filter').submit()
            });
        });
    </script>
@endsection

