@extends('layouts.dashboard')
@section('head')

@endsection

@section('content')
   <div class="card">
       <div class="card-body">
           <a href="#">{{__('Send Emails to  Doctors')}}</a>
           <div class="row">
               <div class="col-md-8">

                   <div class="card rounded border mb-2">
                       <div class="card-body p-3">
                           <strong class="text-linkedin float-right">{{__('Practice')}}</strong><br>
                           <div class="media">

                               <div class="media-body">

                                   @if($job->practice)
                                       <h6 class="mb-1">{{$job->practice->first_name}} {{$job->practice->last_name}}</h6>
                                       <p class="text-muted mb-0 tx-13">

                                           <b>{{__('Service Area')}}</b>:  {{optional($job->practice)->serviceArea->name}}<br>

                                       </p>
                                   @else
                                       <p>
                                           {{__('Not Set')}}
                                       </p>
                                   @endif
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="card rounded border mb-2">
                       <div class="card-body p-3">
                           <div class="media">
                               <div class="media-body">
                                   <h6 class="mb-1">{{__('Notes')}}</h6>


                                   <p class="text-muted mb-0 tx-13">
                                       {{$job->notes}}
                                   </p>


                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="card rounded border mb-2">
                       <div class="card-body p-3">
                           <div class="media">
                               <div class="media-body">
                                   <h6 class="mb-1">{{__('Tags')}}</h6>
                                   @if($job->tags)
                                       @foreach($job->tags as $tag)
                                           <span class="badge badge-primary">{{$tag['value']}}</span>
                                       @endforeach
                                   @else
                                       <p>
                                           {{__('No tags')}}
                                       </p>
                                   @endif
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="col-md-4">
                   <div class="form-group">
                       <label for="">{{__('Group')}}</label>
                       <select class="form-control has-error">
                           <option value="all_with_interest" selected> {{__('All with interest')}}</option>
                           <option value="only available"> {{__('Only available')}}</option>
                           <option value="exclusively">{{__('Exclusively')}}</option>
                       </select>
                   </div>
               </div>
           </div>
       </div>
   </div>
@endsection