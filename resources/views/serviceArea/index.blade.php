@extends('layouts.dashboard')
@section('head')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/af-2.3.3/b-1.5.6/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body table-responsive">
                    <h4 class="card-title" >{{__('List of Service Areas')}}</h4>
                    <a class="btn btn-github btn-rounded btn-rounded float-right btn-sm btn-rounded" href="{{route('services.create')}}">{{__('Add New Service Area')}}</a>
                    <table class="table table-striped" id="data-table" style="width: 100%">
                        <thead>
                        <tr>
                            <th>
                                {{__("Service Area Name")}}
                            </th>
                            <th>
                                {{__("Designation")}}
                            </th>
                            <th>
                                {{__("Type")}}
                            </th>
                            <th>
                                {{__("Contact Person")}}
                            </th>
                            <th>
                                {{__("Created At")}}
                            </th>
                            <th>
                                Actions
                            </th>

                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/af-2.3.3/b-1.5.6/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>
    <!--
   <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
 -->
    <script>
        $(document).ready( function () {
            $('#data-table').DataTable({
                "processing": true,
                "serverSide": true,
                "searching": false,
                "ajax": "{{ route('datatable.services.index') }}",
                "columns": [
                    {   "data": "name" ,
                        'name':"name",
                    },
                    {   "data": "designation" ,
                        'name':"designation",
                        "render": function(data, type, full) {
                            return full['designation'] + '<br><span class="text-info txt-8"> ' + full['internal_name']+"</span>";
                        }

                    },
                    {   "data": "type" ,
                        'name':"type",
                        "render": function(d) {

                            if(d!=null){

                                switch (parseInt(d) ){
                                    case 0:return "Sitz";
                                    case 1:return "FD_Fahrer";
                                    case 2: return "FD_selbst";
                                }

                            }
                            return '--'
                        }

                    },
                    {   "data": "contact_person" ,
                        'name':"contact_person",
                        "render": function(d) {
                            return d
                        }

                    },
                   {
                        "data":"created_at",
                        "render":function (d) {
                            return moment(d).format("DD.MM.YYYY H:m")
                        }
                    },
                    {
                        "data":null,
                        "name":"id",
                        "render":function (d) {
                            var links='<form method="POST" action="{{route("services.index")}}/'+d.id+'" id="'+d.id+'">'+
                                    '{{csrf_field()}} {{method_field("DELETE")}}'+
                                '<a class="" href="{{route("services.index")}}/'+d.id+'/edit">{{__('Edit')}}</a> &nbsp;';
                              links+='<a class="" href="{{route("services.index")}}/'+d.id+'">{{__('Details')}}</a>  &nbsp;';
                              return links+='<a  href="#" onclick="document.getElementById(\''+d.id+'\').submit()">{{__('Delete')}}</a></form>';

                        }
                    }


                ]
            });
        });
    </script>
@endsection
