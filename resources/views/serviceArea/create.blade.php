@extends('layouts.dashboard')
@section('head')
    <style>
        #draggable {
            position: absolute;
            width: 460px;
            resize: both;
            z-index: 2000;
            top: 50%;
            right: 10%;
            font-size: 12px;

            /* bring your own prefixes */
            transform: translate(-50%, -50%);

        }

        #draggable-header {

            cursor: move;
            z-index: 10;
            background-color: #eee;

        }
    </style>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('services.store')}}" method="POST">
                        {{csrf_field()}}
                        <h4 class="card-title">{{__('Add New Service Area')}}</h4>
                        @if($errors->count())
                            <div class="alert alert-warning">
                                {{$errors->first()}}
                            </div>

                        @endif
                        <ul class="nav nav-tabs " role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">
                                    {{__('General Information')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">
                                    {{__('Additional Information')}}
                                </a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                                    <div class="col-lg-3 col-sm-12 col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="type"  value="0" id="r-surgery">
                                                    <i class="input-helper"></i>{{__('Surgery Service')}}</label>
                                            </div>
                                            @error('type')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" value="1" name="type" id="r-driving">
                                                    <i class="input-helper"></i> {{__('Driving Service with Driver')}}</label>
                                            </div>
                                            @error('type')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-12 col-md-8 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" value="2" name="type" id="r-w-driving">
                                                    <i class="input-helper"></i> {{__('Driving Service without Driver')}}</label>
                                            </div>
                                            @error('type')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                      <div class="row">
                                          <div class="col">

                                              <div class="form-group">
                                                  <label for="name">{{__('Service Name')}}</label>
                                                  <input class="form-control @error('name') is-invalid @enderror"
                                                         type="text"
                                                         name="name"
                                                         value="{{old('name')}}"
                                                  >
                                                  @error('name')
                                                  <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                                </span>
                                                  @enderror
                                              </div>
                                          </div>
                                          <div class="col">
                                              <div  class=" btn btn-sm btn-dribble float-right btn-address-book"  style="cursor: pointer">

                                                  {{__('address-book')}}
                                                  <i class="mdi mdi-arrow-expand-all text-facebook "></i>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row ">
                                          <div class="col-md-4 col-sm-12">

                                              <div class="form-group">
                                                  <label for="name">{{__('Control Center Designation')}}</label>
                                                  <input class="form-control @error('designation') is-invalid @enderror"
                                                         type="text"
                                                         name="designation"
                                                         value="{{old('designation')}}"
                                                  >
                                                  @error('designation')
                                                  <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                  @enderror
                                              </div>
                                          </div>
                                          <div class="col-md-4">

                                              <div class="form-group">
                                                  <label for="name">{{__('Internal Name')}}</label>
                                                  <input class="form-control @error('internal_name') is-invalid @enderror"
                                                         type="text"
                                                         name="internal_name"
                                                         value="{{old('internal_name')}}"
                                                  >
                                                  @error('internal_name')
                                                  <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                  @enderror
                                              </div>
                                          </div>
                                          <div class="col-md-4">

                                              <div class="form-group">
                                                  <label for="name">{{__('Internal Name Lst')}}</label>
                                                  <input class="form-control @error('internal_name_lst') is-invalid @enderror"
                                                         type="text"
                                                         name="internal_name_lst"
                                                         value="{{old('internal_name_lst')}}"
                                                  >
                                                  @error('internal_name_lst')
                                                  <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                  @enderror
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row ">
                                          <div class="col-md-12">
                                              <div class="form-group">
                                                  <label for="notes">{{__('Notes')}}</label>
                                                  <textarea name="notes"  cols="30" rows="10" class="form-control  @error('notes') is-invalid @enderror">{{old('notes')}}</textarea>
                                                  @error('notes')
                                                  <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                  @enderror
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row surgery-inputs">
                                          <div class="col-md-12">
                                              <div class="form-group">
                                                  <label for="phone">{{__('Phone')}}</label>
                                                  <input class="form-control @error('phone') is-invalid @enderror"
                                                         type="text"
                                                         name="phone"
                                                         value="{{old('phone')}}"
                                                  >
                                                  @error('phone')
                                                  <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                  @enderror
                                              </div>
                                          </div>
                                          <div class="col-md-3">

                                          </div>
                                      </div>
                                      <div class="row surgery-inputs">
                                          <div class="col-md-12">

                                              <div class="form-group">
                                                  <label for="street">{{__('Post Code')}}</label>
                                                  <input type="text" class="form-control @error('post_code') is-invalid @enderror"
                                                         name="post_code"
                                                         value="{{old('post_code')}}"
                                                  >
                                                  @error('post_code')
                                                  <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                  @enderror
                                              </div>
                                          </div>
                                      </div>







                            </div>
                            <div class="tab-pane fade" id="info" role="tabpanel" aria-labelledby="info-tab">
                               <div class="row">
                                   <div class="col-md-6">
                                       <div class="form-group">
                                           <label for="">{{__('Contact person')}}</label>
                                           <input type="text" class="form-control" name="contact_person" {{old('contact_person')}}>
                                       </div>
                                       <div class="form-group">
                                           <label for="">{{__('Phone Contact person')}}</label>
                                           <input type="text" class="form-control" name="phone_contact_person"  {{old('phone_contact_person')}}>
                                       </div>
                                   </div>
                                   <div class="col-md-6">
                                       <div  class=" btn btn-sm btn-dribble float-right btn-address-book"  style="cursor: pointer">

                                           {{__('address-book')}}
                                           <i class="mdi mdi-arrow-expand-all text-facebook "></i>
                                       </div>
                                   </div>
                               </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 ">
                                        <div class="form-group">
                                            <label for="">{{__('Email KV')}}</label>
                                            <input type="email" class="form-control" name="email_kv"  {{old('email_kv')}}>
                                        </div>
                                        <div class="form-group">
                                            <label for="">{{__('Phone KV')}}</label>
                                            <input type="text" class="form-control" name="phone_kv" {{old('phone_kv')}}>
                                        </div>
                                        <div class="form-group">
                                            <label for="">{{__('Fax KV')}}</label>
                                            <input type="text" name="fax_kv" class="form-control" {{old('fax_kv')}}>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="">{{__('Email Lst')}}</label>
                                            <input type="email" name="email_lst" class="form-control"  {{old('email_lst')}} >
                                        </div>
                                        <div class="form-group">
                                            <label for="">{{__('Phone Lst')}}</label>
                                            <input type="text" name="phone_lst" class="form-control" {{old('phone_lst')}}>
                                        </div>
                                        <div class="form-group">
                                            <label for="">{{__('Fax Lst')}}</label>
                                            <input type="text" name="fax_lst" class="form-control"  {{old('fax_lst')}} >
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-github btn-rounded float-right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

           <div class="card modal-address-book" id="draggable" style="display: none;">
               <div class="card-header"  id="draggable-header" style="padding: 8px;">
                    {{__('Select Address Book')}}
                   <span class="mdi mdi-close float-right" style="cursor: pointer" id="close">
               </span>
               </div>

               <div class="card-body" style="overflow-y: auto;max-height: 60vh;padding: 15px">
                   <div class="form-group">
                       <input type="text" class="form-control" id="q" value="" placeholder="{{__('Search for address-book')}}">
                   </div>
                   <div id="address-list-group" >

                   </div>
               </div>
           </div>

@endsection
@section('js')


@endsection
