@extends('layouts.dashboard')
@section('head')
    <style>
        #draggable {
            position: absolute;
            width: 460px;
            resize: both;
            z-index: 2000;
            margin: 0 auto;
            top: 70px;


            /* bring your own prefixes */
            /*transform: translate(-50%, -50%);*/

        }

        #draggable-header {

            cursor: move;
            z-index: 10;
            background-color: #eee;

        }
    </style>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('services.clone',$service)}}" method="POST">
                        {{csrf_field()}}
                        <button class="btn btn-sm btn-google float-right" >{{__('Clone This Service')}}</button>
                    </form>

                    <form action="{{route('services.update',$service)}}" method="POST">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <h4 class="card-title">{{__('Edit Service Area')}}</h4>
                        @if($errors->count())
                            <div class="alert alert-warning">
                                {{$errors->first()}}
                            </div>

                        @endif

                        <ul class="nav nav-tabs " role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">
                                    {{__('General Information')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">
                                    {{__('Additional Information')}}
                                </a>
                            </li>
                           <!--
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact-person" role="tab" aria-controls="contact-1" aria-selected="false">
                                    {{__('Contact Person')}}
                                </a>
                            </li>
                            -->
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
                                        <div class="form-group" >
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="type"
                                                           {{$service->type==0?'checked':''}}
                                                           value="0"
                                                    id="r-surgery"
                                                    >
                                                    <i class="input-helper"></i>{{__('Surgery Service')}}</label>
                                            </div>
                                            @error('type')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" value="1"
                                                           {{$service->type==1?'checked':''}}
                                                           id="r-driving"
                                                           name="type">
                                                    <i class="input-helper"></i> {{__('Driving Service with Drive')}}</label>
                                            </div>
                                            @error('type')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" value="2"
                                                           {{$service->type==2?'checked':''}}
                                                           id="r-w-driving"
                                                           name="type">
                                                    <i class="input-helper"></i> {{__('Driving Service without Drive')}}</label>
                                            </div>
                                            @error('type')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">

                                        <div class="form-group">
                                            <label for="name">{{__('Service Name')}}</label>
                                            <input class="form-control @error('name') is-invalid @enderror"
                                                   type="text"
                                                   name="name"
                                                   value="{{$service->name}}"
                                            >
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col">
                                            <div  class=" btn btn-sm btn-dribble float-right btn-address-book"  style="cursor: pointer">

                                                {{__('address-book')}}
                                                <i class="mdi mdi-arrow-expand-all text-facebook "></i>
                                            </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="name">{{__('Control Center Designation')}}</label>
                                            <input class="form-control @error('designation') is-invalid @enderror"
                                                   type="text"
                                                   name="designation"
                                                   value="{{$service->designation}}"
                                            >
                                            @error('designation')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="name">{{__('Internal Name')}}({{__('optional')}})</label>
                                            <input class="form-control @error('internal_name') is-invalid @enderror"
                                                   type="text"
                                                   name="internal_name"
                                                   value="{{$service->internal_name}}"
                                            >
                                            @error('internal_name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label for="name">{{__('Internal Name Lst')}}</label>
                                            <input class="form-control @error('internal_name_lst') is-invalid @enderror"
                                                   type="text"
                                                   name="internal_name_lst"
                                                   value="{{$service->internal_name_lst}}"
                                            >
                                            @error('internal_name_lst')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row notes">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="notes">{{__('Notes')}}</label>
                                            <textarea name="notes"  cols="30" rows="10" class="form-control  @error('notes') is-invalid @enderror">{{$service->notes}}</textarea>
                                            @error('notes')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row phone surgery-inputs"style="display:{{$service->type==1?'none':'block'}} ">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="phone">{{__('Phone')}}</label>
                                            <input class="form-control @error('phone') is-invalid @enderror"
                                                   type="text"
                                                   name="phone"
                                                   value="{{$service->phone}}"
                                            >
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col">
                                       <!--
                                        <div class="form-group">
                                                <label for="email">{{__('Email')}}</label>
                                                <input class="form-control @error('phone') is-invalid @enderror"
                                                       type="text"
                                                       name="email"
                                                       value="{{optional($service->contactPerson)->email}}"
                                                >
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                     <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                      -->
                                    </div>
                                </div>
                                <div class="row surgery-inputs" style="display:{{$service->type==1||$service->type==2?'none':'block'}}">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="street">{{__('Post Code')}}</label>
                                            <input type="text" class="form-control @error('post_code') is-invalid @enderror"
                                                   name="street"
                                                   value="{{$service->post_code}}"
                                            >
                                            @error('post_code')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div >

                            </div>

                            <div class="tab-pane fade" id="info" role="tabpanel" aria-labelledby="info-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">{{__('Contact person')}}</label>
                                            <input type="text" class="form-control" name="contact_person" value="{{$service->contact_person}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">{{__('Phone Contact person')}}</label>
                                            <input type="text" class="form-control" name="phone_contact_person"  value="{{$service->phone_contact_person}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div  class=" btn btn-sm btn-dribble float-right btn-address-book"  style="cursor: pointer">

                                            {{__('address-book')}}
                                            <i class="mdi mdi-arrow-expand-all text-facebook "></i>
                                        </div>
                                    </div>



                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 ">
                                        <div class="form-group">
                                            <label for="">{{__('Email KV')}}</label>
                                            <input type="email" class="form-control" name="email_kv"   value="{{$service->email_kv}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">{{__('Phone KV')}}</label>
                                            <input type="text" class="form-control" name="phone_kv" value="{{$service->phone_kv}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">{{__('Fax KV')}}</label>
                                            <input type="text" name="fax_kv" class="form-control" value="{{$service->fax_kv}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="">{{__('Email Lst')}}</label>
                                            <input type="email" name="email_lst" class="form-control"  value="{{$service->email_lst}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">{{__('Phone Lst')}}</label>
                                            <input type="text" name="phone_lst" class="form-control"  value="{{$service->phone_lst}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">{{__('Fax Lst')}}</label>
                                            <input type="text" name="fax_lst" class="form-control" value="{{$service->fax_lst}}" >
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row">
                            <div class="col grid-margin">
                                <button type="submit" class="btn btn-github btn-rounded btn-rounded btn-rounded btn-sm float-right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- Delete this div
    <div class="card modal-address-book" id="draggable" style="display: none;">
        <div class="card-header"  id="draggable-header" style="padding: 8px;">
            Select Address Book
            <span class="mdi mdi-close float-right" style="cursor: pointer" id="close">
            </span>
        </div>

        <div class="card-body" style=" resize: vertical">
            <div class="btn btn-link float-right" id="btn-show-add-form">
                {{__('Add New Address')}}
            </div>
            <br>
            <div class="form-new-address" style="display: none">

                <div class="form-group">
                    <label for="">{{__('New Address Book')}}</label>
                    <textarea type="text" class="form-control" id="address-content"  value="" placeholder="{{__('address book')}}">
                    </textarea>
                </div>
                <div class="btn btn-github btn-sm btn-rounded float-right" id="btn-save-address">{{__('Save')}}</div>
                <div class="btn btn-link btn-sm btn-rounded float-right" id="btn-cancel">{{__('Cancel')}}</div>
            </div>
            <div class="list-address">
                <div class="form-group">
                    <input type="text" class="form-control" id="q" value="">
                </div>

                <div id="address-list-group" style="overflow-y: auto;max-height: 460px">

                </div>
            </div>

        </div>
    </div>
    -->
@endsection
@section('js')


    <script type="application/javascript" defer>


        (function($) {
            'use strict';

               $(function() {

                $('#r-driving').on('click',function () {
                    $('.surgery-inputs').hide()

                })

                $('#r-w-driving').on('click',function () {
                    $('.surgery-inputs').hide()

                })
                $('#r-surgery').on('click',function () {
                    $('.surgery-inputs').show()
                })


            })
        })(jQuery);


    </script>


@endsection
