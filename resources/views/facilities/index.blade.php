<!doctype html>

<html lang="en-US">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>

    <link rel="shortcut icon" href="images/favicon.ico"/>

    <title>Consulmedics</title>

    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="all"/>

    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="all" />

    <link rel="stylesheet" href="css/font-materialdesign.css" type="text/css" media="all" />

    <link rel='stylesheet' href='css/animate.min.css' type='text/css' media='all' />

    <link rel="stylesheet" href="css/settings.css" type="text/css" media="all" />

    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css" media="all" />

    <link rel="stylesheet" href="css/owl.theme.css" type="text/css" media="all" />

    <link rel="stylesheet" href="css/owl.transitions.css" type="text/css" media="all" />

    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" media="all" />

    <link rel='stylesheet' href='css/prettyPhoto.css' type='text/css' media='all' />

    <link rel="stylesheet" href="css/style.css?v=3" type="text/css" media="all"/>

    <link rel="stylesheet" href="css/custom.css?v=3" type="text/css" media="all"/>

    <link rel="stylesheet" href="css/style-business.css" type="text/css" media="all"/>

    <link rel="stylesheet" href="css/colors/lightblue.css" type="text/css" media="all"/>

    <link href="https://fonts.googleapis.com/css?family=Calibri&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->




    <style type="text/css">

        /*body{

            font-family: 'calibri', sans-serif !impprtant;

        }*/

        #wrapper {

            height: 100%;

            width: 100%;

            min-height: 650px;

            min-width: 900px;

            padding-top: 1px;

        }

        #slider {

            margin: 100px 0 0 0;

            height: 100vh;

            overflow: hidden;

            background: url(imgages/lprettyPhoto/dark_rounded/loader.gif) center center no-repeat;

        }



        #slider .slide {

            position: relative;

            display: none;

            height: 500px;



            float: left;

            background-position: center right;

            cursor: pointer;



        }



        #slider .slide:first-child {

            border: none;

        }



        #slider .slide.active .slide-block {

            cursor: default;

            width: 550px



        }



        .slider-title{

            position: absolute;

            background-color: #1d22278a;

            z-index: 1;

            margin: 0px 0px 15px 40px;

            width: 90%;

            padding: 15px;

            font-size: 35px;

            line-height: 45px;

            top:106px;

        }



        #slider .slide-block {

            position: absolute;

            left: 10px;

            bottom:  70px;

            display: inline-block;

            width: 300px;

            padding: 15px;

            min-height: 140px;

            word-wrap: break-word;

            overflow: hidden;

            border-radius: 4px;

            background-color:#212427;

            opacity: 0.7;



            color: #000;



            -webkit-transition: all 0.3s; /* For Safari 3.1 to 6.0 */



            transition: all 0.3s;



            border:none;

        }



        #slider .slide-block h4 {



            font-weight: bold;

            margin: 0 0 10px 0;



            line-height: 1;



        }

        #slider .slide-block p{

            line-height: 15px;

            font-size: 13px;





            -webkit-transition: height 1s; /* For Safari 3.1 to 6.0 */



            transition: height 2s;

            -moz-transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1),cubic-bezier(0.215, 0.61, 0.355, 1),ease,ease;

            -o-transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1),cubic-bezier(0.215, 0.61, 0.355, 1),ease,ease;

            -webkit-transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1),cubic-bezier(0.215, 0.61, 0.355, 1),ease,ease;

            transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1),cubic-bezier(0.215, 0.61, 0.355, 1),ease,ease;

        }



        .bg-orange{

            background-color: orange

        }

        .bg-blue{

            background-color: #3694ca

        }

        .bg-blue,.bg-blue p {

            color: white!important

        }

        .orange-grow:hover,.orange-grow:focus{

            -moz-box-shadow: 0px 0px 20px orange;

            -webkit-box-shadow: 0px 0px 20px orange;

            box-shadow: 0px 0px 20px orange;

        }

        .blue-grow:hover,.blue-grow:focus{

            -moz-box-shadow: 0px 0px 20px #0099ff;

            -webkit-box-shadow: 0px 0px 20px #0099ff;

            box-shadow: 0px 0px 20px #0099ff;

        }



        .c-btn{







            /*Transition*/

            -webkit-transition: -webkit-box-shadow 0.5s ease-out;;

            -moz-transition: -moz-box-shadow 0.5s ease-out;

            -o-transition: box-shadow 0.5s ease-out;

        }





        .main-menu .main-nav {

            margin-top: 40px;

            font-size: 14px;

        }



        .main-menu .main-nav >li a{

            line-height: 30px;

            font-size: 14px;

        }



        .btn-2:after,

        .btn-2:before {

            -webkit-backface-visibility: hidden;

            backface-visibility: hidden;

            border: 0px solid #0099ff;

            bottom: 0px;

            content: " ";

            display: block;

            margin: 0 auto;

            position: relative;

            transition: all 280ms ease-in-out;

            width: 0;



        }



        .btn-2:hover:after,.btn-2:focus:after,

        .btn-2:hover:before,.btn-2:focus:before  {

            -webkit-backface-visibility: hidden;

            backface-visibility: hidden;

            border-color:#0099ff;

            transition: width 350ms ease-in-out;

            width: 100%;



            border: 1px solid #0099ff;

        }



        .btn-2:hover:before {

            bottom: auto;

            top: 0;

            width: 100%;

        }



        .header.header-transparent {



            height: 70px;

        }



        .header #logo {



            margin-top: 15px;

        }



        .main-menu{

            height: 70px;
            position:relative;

        }



        .module {



            margin: 0 0 1em 0;

            overflow: hidden;

        }

        .module p {

            margin: 0;

        }



        .line-clamp {

            display: -webkit-box;

            -webkit-line-clamp: 3;

            -webkit-box-orient: vertical;

        }



        .fade {

            position: relative;

            height: 3.6em; /* exactly three lines */

        }

        .fade:after {

            content: "";

            text-align: right;

            position: absolute;

            bottom: 0;

            right: 0;

            width: 70%;

            height: 1.2em;

            background: linear-gradient(to right, rgba(255, 255, 255, 0), rgba(255, 255, 255, 1) 50%);

        }



        .last-line {

            height: 3.6em; /* exactly three lines */

            text-overflow: -o-ellipsis-lastline;

        }



        .ftellipsis {

            height: 3.6em;

        }
        /*custom*/
        .main-menu .main-nav {
            margin-top: 0px;
        }
        .main-menu .main-nav >li a {
            line-height: 70px;
        }
        .top-btn-inner-tn{
            text-transform: none;
            background: #f37c23;
            color: #000;
            border: 1px solid #f37a23;
            padding: 12px 17px;
            margin-bottom: 40px;
            font-weight: bold;
            letter-spacing: .5px;
            font-size: 16px;
            transition: all .3s linear 0s;
            border-radius: 0px;
            padding-left: 17px;padding-right: 17px;
        }
        .top-btn-inner-tn:hover,.top-btn-inner-tn:focus{
            -moz-box-shadow: none;
            -webkit-box-shadow: none;
            box-shadow: none;
            background: #fff;
            text-shadow: none;
            border-color: #fff;
        }
        @media(min-width:200px) and (max-width:1023px){
            .main-menu .main-nav >li a{
                line-height: 30px;
            }
        }

    </style>

</head>

<body class="second-page">

<div class="noo-spinner">

    <div class="noo-spinner-inner">

        <img src="images/preloader/default.svg" width="150" alt="" />

    </div>

</div>



<div class="site">

    <header class="header header-transparent">

        <div class="header-inner">

            <div class="header-main">

                <div class="btn-navbar hidden" data-target=".main-nav"></div>

                <div id="logo">

                    <div id="logo-before">

                        <a href="demo-business-index.html"><img src="images/logo.png" alt="" style="width: 192px;
    height: auto;" /></a>

                    </div>

                    <div id="logo-after">

                        <a href="demo-business-index.html"><img src="images/log.png" alt="" style="width: 192px;
    height: auto;"/></a>

                    </div>

                </div>

                <div class="side-menu hidden-lg hidden-md hidden-sm">

                    <div class="sidebar-trigger hidden-bar-toggle">

                        <div class="line line-1"></div>

                        <div class="line line-2"></div>

                        <div class="line line-3"></div>

                    </div>

                </div>

                <div class="main-menu">

                    <div class="main-menu-inner second-page-menu">

                        <ul class="main-nav " data-offset="66">
                            <li><a class="onepage current btn-2" href="#fullscreen-banner">Home</a></li>

                            <li><a class="onepage btn-2" href="#vorteile">VORTEILE</a></li>

                            <li><a class="onepage btn-2" href="#ablauf">ABLAUF</a></li>
                            <li><a class="onepage btn-2" href="#testimonial">MEINUNGEN</a></li>
                            <li><a class="onepage btn-2" href="#about">ÜBER UNS</a></li>

                            <li><a class="onepage btn-2" href="#contact">Kontakt</a></li>


                            <li>

                                <a  href="/swenb/swenb_vertreter.html#contact" >

										 <span class="c-btn blue-grow top-btn-inner-tn"

                                               style="">JETZT INFOS ANFORDERN oder EINFACH FRAGEN

										</span>

                                </a>

                            </li>

                        </ul>



                    </div>

                </div>

            </div>

        </div>

    </header>

    <div id="main">

        <div id="fullscreen-banner" class="section" style="margin-top: 70px">

            <div class="container-fluid">

                <div class="inner-page-banner sec-page-banner" style="/*background: url(images/slider/banner-latest.jpg);*/">
                    <!-- <img src="images/slider/banner-latest.jpg"> -->
                    <img src="images/slider/second-page-banner.jpg">
                </div>

            </div>

        </div><!--  fullscreen-banner -->


        <!-- <div class="row">

            <div class="col-md-12" style="height: 100vh;background-image: url(images/background/bg2.png);

                        background-position: center;    min-height: 291px;

                        background-repeat: no-repeat;

                        background-size: contain;">



            </div>

        </div> -->
        <!-- Checklist Section Start-->
        <div id="vorteile" class="main-check-list-row">
            <div class="container">
                <div class="row custom-checklist">
                    <div class="col-md-12">
                        <ul>
                            <h6 class="dark-color">DAS BIETEN WIR IHNEN</h6>
                            <li>
                                <label for="checklist">
                                    <input type="checkbox" name="checklist-1" id="checklist1" checked>
                                    <p>
                                        Wir vermitteln Sie als Vertreter für Kassenärztliche Bereitschaftsdienste.
                                    </p>
                                </label>
                            </li>
                            <li>
                                <label for="checklist">
                                    <input type="checkbox" name="checklist-2" id="checklist2" checked>
                                    <p>
                                        Top-Vergütung: 600 € pro 12-Stunden-Dienst auf Honorarbasis.
                                    </p>
                                </label>
                            </li>
                            <li>
                                <label for="checklist">
                                    <input type="checkbox" name="checklist-3" id="checklist3" checked>
                                    <p>
                                        Abwechslungsreiche Tätigkeit. Flexible Arbeitszeiten.
                                    </p>
                                </label>
                            </li>
                            <li>
                                <label for="checklist">
                                    <input type="checkbox" name="checklist-4" id="checklist4" checked>
                                    <p>
                                        Abwicklung aller bürokratischen Angelegenheiten.
                                    </p>
                                </label>
                            </li>
                            <li>
                                <label for="checklist">
                                    <input type="checkbox" name="checklist" id="checklist5" checked>
                                    <p>
                                        Voraussetzung: abgeschl. 2. Weiterbildungsjahr (alle Fachrichtungen).
                                    </p>
                                </label>
                            </li>
                            <!-- 									<li>
                                                                    <label for="checklist">
                                                                        <input type="checkbox" name="checklist" id="checklist5" checked>
                                                                        <p>
                                                                            Wir bieten umfassende Unterstützung bei bürokratischen Angelegenheiten (z.B. Rechnung f. Totenschein).
                                                                        </p>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label for="checklist">
                                                                        <input type="checkbox" name="checklist" id="checklist5" checked>
                                                                        <p>
                                                                            Voraussetzung: abgeschlossenes 2. Weiterbildungsjahr (jede Fachrichtung).
                                                                        </p>
                                                                    </label>
                                                                </li> -->

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Checklist Section End -->



        <!-- 4 pages -->
        <div id="hexagon-imgs">

            <!-- services -->
            <div id="services" class="section pt-7 pb-5 news-with-service">

                <div class="container-fluid">

                    <div class="row">

                        <div class="col-md-10 col-md-offset-1">

                            <div data-wow-delay="0.2s" class="text-center mb-6 wow fadeInUp">

                                <h2 class="fw-800  dark-color section-title mb-1">Perfekter Rund-um-Service für Vertretungsärzte</h2>


                            </div>

                        </div>

                    </div>

                    <!-- News -->
                    <div class="news-section row">
                        <div class="container">
                            <div class="row news-section-vertreter vertreter-row">
                                <!-- news inner -->
                                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 news-section-inner">
                                    <img src="images/slider/hand-shake.png">
                                    <div>
                                        <h4>PERSÖNLICH </h4>
                                        <p> Wir garantieren Ihnen eine enge persönliche Betreuung durch unser Team aus erfahrenen ärztlichen Kollegen </p>
                                    </div>
                                </div>
                                <!-- news inner -->
                                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 news-section-inner">
                                    <img src="images/slider/top-spport-1.jpg">
                                    <div>
                                        <h4>PROFESSIONELL</h4>
                                        <p>Profi-Equipment (Notfalltasche, Tablet usw.) erhalten Sie selbstverständlich KOSTENLOS von uns. </p>
                                    </div>
                                </div>
                                <!-- news inner -->
                                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 news-section-inner">
                                    <img src="images/slider/schulung-1.jpg">
                                    <div>
                                        <h4>TOP-SUPPORT</h4>
                                        <p>Völlig unbürokratische Abwicklung und Abrechnung Ihrer geleisteten Dienste.</p>
                                    </div>
                                </div>
                                <!-- news inner -->
                                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12 news-section-inner">
                                    <img src="images/slider/personlich-1.jpg">
                                    <div>
                                        <h4>SCHULUNG</h4>
                                        <p>Umfassende Einarbeitung und optimales Training für neue Vertretungsärzte. (wenn gewünscht)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="geths" class="container colored-icons">
                        <div class="row swenb_vertreter_sericon">



                            <div class="col-sm-6">

                                <div data-wow-delay="0.4s" class="service-item mb-5 text-center wow fadeIn">

                                    <div class="service-icon">

                                        <!-- <i class="mdi-alarm color"></i> -->
                                        <img src="./images/vertreter/niveau-1-yello.png">

                                    </div>

                                    <div class="service-content text-center">

                                        <h4 class=" dark-color">Kollegialität</h4>

                                        <p>
                                            Wertschätzender und achtungsvoller Umgang sind für uns essentiell. Wir stehen Ihnen bei allen Fragen und Problemen hilfreich zur Seite.
                                        </p>

                                    </div>

                                </div>

                            </div>



                            <div class="col-sm-6">

                                <div data-wow-delay="0.5s" class="service-item mb-5 text-center wow fadeIn">

                                    <div class="service-icon">

                                        <!-- <i class="mdi-cloud-outline color"></i> -->

                                        <img src="./images/vertreter/team-1-yello.png">

                                    </div>

                                    <div class="service-content text-center">

                                        <h4 class=" dark-color">Team-Spirit</h4>

                                        <p>
                                            Wir sind ärztliche Kollegen auf Augenhöhe, mit professioneller und trotzdem frischer dynamischer Kommunikations-Atmosphäre. Werden Sie Teil unseres jugendlich-dynamischen Teams und gestalten Sie die Zukunft des Gesundheitswesens mit.

                                        </p>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="row swenb_vertreter_sericon">

                            <div class="col-sm-6">

                                <div data-wow-delay="0.2s" class="service-item mb-5 text-center wow fadeIn">

                                    <div class="service-icon">
                                        <img src="./images/vertreter/cap-yello.png">
                                    </div>

                                    <div class="service-content text-center">

                                        <h4 class=" dark-color">Fachwissen</h4>

                                        <p>
                                            Wir bieten allen Vertretungsärzten regelmäßige kostenlose interne Weiterbildungen an. Dazu gehören sowohl Online-Fachkonferenzen zu ausgewählten Fragestellungen aus dem Bereitschaftsdienst, als auch Konferenzen vor Ort. Ausserdem planen wir perspektivisch mit renomierten Partnern die Durchführung von externen Kursen.
                                        </p>

                                    </div>

                                </div>

                            </div>

                            <div class="col-sm-6">

                                <div data-wow-delay="0.4s" class="service-item mb-5 text-center wow fadeIn">

                                    <div class="service-icon">

                                        <img src="./images/vertreter/euro-yello.png">

                                    </div>

                                    <div class="service-content text-center">

                                        <h4 class=" dark-color">Keine Gebühr</h4>

                                        <p>
                                            Bei der Übernahme von Kassenärztlichen Bereitschaftsdiensten entstehen Ihnen als Vertreter selbstverständlich keinerlei Kosten für unsere umfassenden Services.
                                        </p>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="container">
                <div class="row hexagon-img-main">
                    <div class="col-md-12 new-design-row" >
                        <h2 class="fw-800  dark-color section-title mb-1">SO FUNKTIONIERT ES</h2>
                        <div class="col-sm-12 new-design-start">
                            <div class="garay_box box-1">
                                <div class="col-sm-1 emp-box"></div>
                                <div class="col-sm-2 colored-box neela">
                                    <img src="images/hexagon/pc.png" class="box-icon">
                                    <h4>1.<br>Anmeldung</h4>
                                </div>
                                <div class="col-sm-9 cont-box">
                                    <!-- <h3>Sample text here</h3> -->
                                    <p>Sie melden sich innerhalb weniger Minuten völlig kostenlos und unverbindlich bei uns an. </p>
                                </div>
                            </div><!--  garay_box box-1 -->
                            <div class="garay_box box-1">
                                <div class="col-sm-1 emp-box"></div>
                                <div class="col-sm-2 colored-box neela orange">
                                    <img src="images/hexagon/head.png" class="box-icon">
                                    <h4>2.<br>Einsatzorte</h4>
                                </div>
                                <div class="col-sm-9 cont-box">
                                    <!-- <h3>Sample text here</h3> -->
                                    <p>Sie teilen uns mit, in welchen Bundesländern/Regionen/Städten Sie gerne Dienste übernehmen möchten.</p>
                                </div>
                            </div>
                            <button class="tn-btn-new-for-inner">jetzt infos anfordern</button>
                            <!--  garay_box box-1 -->
                            <div class="garay_box box-1">
                                <div class="col-sm-1 emp-box"></div>
                                <div class="col-sm-2 colored-box neela kala">
                                    <img src="images/hexagon/bar.png" class="box-icon">
                                    <h4>3.<br>Info über Freie Dienste</h4>
                                </div>
                                <div class="col-sm-9 cont-box">
                                    <!-- <h3>Sample text here</h3> -->
                                    <p>Wir informieren Sie mehrmals wöchentlich über anstehende Dienste und Sie entscheiden völlig frei, ob eine Übernahme für Sie interessant ist.</p>
                                </div>
                            </div><!--  garay_box box-1 -->
                            <div class="garay_box box-1">
                                <div class="col-sm-1 emp-box"></div>
                                <div class="col-sm-2 colored-box neela">
                                    <img src="images/hexagon/pc.png" class="box-icon">
                                    <h4>4.<br>Dienst übernehmen</h4>
                                </div>
                                <div class="col-sm-9 cont-box">
                                    <!-- <h3>Sample text here</h3> -->
                                    <p>Sobald Sie einen Dienst übernehmen, erhalten Sie von uns alle benötigten Formulare und das medizinisch-technische Equipment.
                                        Wenn Sie es wünschen, stehen Ihnen erfahrene ärztliche Kollegen während Ihrer ersten übernommenen Dienste telefonisch zur Seite.
                                    </p>
                                </div>
                            </div>
                            <button class="tn-btn-new-for-inner">jetzt infos anfordern</button>
                            <!--  garay_box box-1 -->
                            <div class="garay_box box-1">
                                <div class="col-sm-1 emp-box"></div>
                                <div class="col-sm-2 colored-box neela orange">
                                    <img src="images/hexagon/head.png" class="box-icon">
                                    <h4>5.<br>Abrechnung</h4>
                                </div>
                                <div class="col-sm-9 cont-box">
                                    <!-- <h3>Sample text here</h3> -->
                                    <p>Nach dem Dienst kümmern wir uns um die gesamte Abrechnung und Sie erhalten zeitnah ihr Honorar ausgezahlt.</p>
                                </div>
                            </div><!--  garay_box box-1 -->
                            <!-- <div class="garay_box box-1">
                                <div class="col-sm-1 emp-box"></div>
                                <div class="col-sm-2 colored-box neela kala">
                                    <img src="images/hexagon/bar.png" class="box-icon">
                                    <h4>Step 3</h4>
                                </div>
                                <div class="col-sm-9 cont-box">
                                    <h3>Sample text here</h3>
                                    <p>Lorem losit amet, consectetur elit sed do eiusmod tempor ut labore et dolore magna Ut enim ad minim venia rud exercitation ullam</p>
                                </div>
                            </div> --><!--  garay_box box-1 -->


                        </div><!--  new-design-END -->
                    </div>
                    <!-- 							<div class="col-md-12 old-design">
                                                    <div class="hexagon-4-img-main">
                                                        <div class="hexagon-4-img-inner col-md-4 text-left info-graphic-blue">
                                                            <div class="needy-div">
                                                                <img src="images/hexagon/2-255.png">
                                                                <div class="content-in-hexa">
                                                                    <h4>INFOGRAPHICS</h4>
                                                                    <p>Lorem losit amet, consectetur<br> elit sed do eiusmod tempor<br> ut labore et dolore magna<br> Ut enim ad minim venia<br> rud exercitation ullam</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="hexagon-4-img-inner col-md-4 ">
                                                            <div class="needy-div">
                                                                <img src="images/hexagon/3-255.png">
                                                                <div class="content-in-hexa">
                                                                    <h4>INFOGRAPHICS</h4>
                                                                    <p>Lorem losit amet, consectetur<br> elit sed do eiusmod tempor<br> ut labore et dolore magna<br> Ut enim ad minim venia<br> rud exercitation ullam</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="hexagon-4-img-inner col-md-4 text-right info-graphic-gray">
                                                            <div class="needy-div">
                                                                <img src="images/hexagon/4-255.png">
                                                                <div class="content-in-hexa">
                                                                    <h4>INFOGRAPHICS</h4>
                                                                    <p>Lorem losit amet, consectetur<br> elit sed do eiusmod tempor<br> ut labore et dolore magna<br> Ut enim ad minim venia<br> rud exercitation ullam</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div> --> <!-- col-md-12 old-design -->
                </div>
            </div>
        </div>











        <div id="contact" class="footer-wrap section section-bg-4 section-fixed pt-7 pb-5">

            <div class="footer-top pt-0">

                <div class="container">

                    <div class="row">

                        <div class="col-md-8 col-md-offset-2">

                            <div data-wow-delay="0.2s" class="text-center wow fadeInUp title-area-contact">

                                <!-- <i class="mdi-google-pages white icon-title"></i> -->

                                <h2 class="fw-800  dark-color section-title mb-1 wr">JETZT INFOS ANFORDERN oder EINFACH FRAGEN</h2>

                                <!-- <p class="white">

                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua

                                </p> -->

                            </div>

                        </div>

                        <div class="col-sm-6 form-6-col">

                            <div data-wow-delay="0.2s" class="wow fadeInLeft">
                                <h2 class="f-36">Ihre Nachricht</h2>
                                <form class="contact-form" action="action.php" method="post">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Name:</label>
                                            <input type="text" class="highlighted" name="author" id="author"  />
                                        </div>

                                        <div class="col-sm-12">
                                            <label>Email:</label>
                                            <input type="text" class="highlighted" name="email" id="email"/>

                                        </div>

                                        <div class="col-sm-12">
                                            <label>Nachricht:</label>
                                            <textarea name="comment" id="comment" class="highlighted fullwidth">Nachricht: Ja, ich möchte mehr Info-Material, denn ich habe Interesse, Kassenärztliche Bereitschaftsdienste  zu übernehmen.</textarea>

                                        </div>

                                        <div class="col-sm-12">

                                            <button type="submit" class="btn btn-color uppercase send_btn_contact"><!-- <a href="https://www.tawk.to/javascript-api/"> -->Send<!-- </a> --></button>

                                        </div>

                                    </div>

                                </form>
                                <div class="show_response"></div>
                            </div>

                        </div>

                        <div class="col-sm-6 contact-right-detail">

                            <div data-wow-delay="0.2s" class="wow fadeInRight">

                                <div class="contact-detail-div">
                                    <i class="mdi-map-marker icon-title"></i>
                                    <div class="detail-contact-of-icon">
                                        <p>Consulmedics GmbH<br>
                                            Grubenstr. 501968<br> Senftenberg</p>
                                    </div>
                                </div>
                                <div class="contact-detail-div">
                                    <i class="mdi-email icon-title"></i>
                                    <div class="detail-contact-of-icon">
                                        <p>
												<span>
													<a href="mailto:info@consulmedics.de">
														info@consulmedics.de
													</a>
												</span>
                                            <!-- <span>
                                                <a href="mailto:info@consulmedics.de">
                                                    info@consulmedics.de
                                                </a>
                                            </span> -->
                                        </p>
                                    </div>
                                </div>
                                <div class="contact-detail-div">
                                    <i class="mdi-phone icon-title"></i>
                                    <div class="detail-contact-of-icon">
                                        <p>
												<span>
													Telefon:<a href="tel:0123-234544">03573-8769872</a>
												</span>
                                            <span>
													Fax:<a href="tel:123456789"> 0351-799989892</a>
												</span>
                                        </p>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                    <!-- <div class="row">

                        <div class="col-md-12">

                            <div class="mt-5 seperate-line"></div>

                        </div>

                    </div> -->

                </div>

            </div>

            <!-- 	<div class="footer-bottom p-0">

                    <div class="container">

                        <div class="row">

                            <div class="col-sm-6">

                                <p class="white">Follow us on:&nbsp;<a class="color" href="#" target="_blank">Twitter</a>&nbsp;|&nbsp;<a class="color" href="#" target="_blank">Facebook</a>&nbsp;|&nbsp;<a class="color" href="#" target="_blank">YouTube</a></p>

                            </div>

                            <div class="col-sm-6 text-right">

                                <p class="white">Proudly developed by <a class="color" href="//themeforest.net/user/tk-themes/portfolio"><strong>TK-Themes</strong></a></p>

                            </div>

                        </div>

                    </div>

                </div> -->

        </div>



        <div id="testimonial" class="section section-bg-1 pt-9 pb-8 testimonial-tn second-page-testi">

            <div class="container">

                <div class="row">

                    <div class="col-md-8 col-md-offset-2">

                        <div data-wow-delay="0.2s" class="text-center mb-5 wow fadeInUp">
                            <h2 class="fw-800  dark-color section-title mb-1" style="color: #fff !important;">DAS SAGEN UNSERE KUNDEN</h2>
                            <p class="white">
                                <!-- Stubb did but speak out for well nigh all that crew. The frenzies of the chase had by this time worked them bubblingly up, like old wine worked anew. Whatever pale fears and forebodings some of them might have felt before. -->
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-10 col-md-offset-1">

                        <div data-wow-delay="0.2s" class="wow fadeInUp">

                            <div class="testimonials-slider">

                                <div class="item text-center">

                                    <!-- <img src="images/avatar/avatar_150x150.jpg" alt="" /> -->

                                    <div class="tm-content">

                                        <h4 class="white">

                                            "Super Team, jederzeit wirklich klasse Service. Ich kann auch kurzfristig für mich passende Dienste übernehmen und alles klappt problemlos. Vielen Dank und weiter so."

                                        </h4>

                                        <div class="tm-title color">Frank L.</div>

                                        <div class="tm-subtitle white">Awesome Client</div>

                                        <div class="tm-stars">

                                            <i class="fa fa-star active"></i>

                                            <i class="fa fa-star active"></i>

                                            <i class="fa fa-star active"></i>

                                            <i class="fa fa-star active"></i>

                                            <i class="fa fa-star active"></i>

                                        </div>

                                    </div>

                                </div>

                                <div class="item text-center">

                                    <!-- <img src="images/avatar/avatar_150x150.jpg" alt="" /> -->

                                    <div class="tm-content">

                                        <h4 class="white">

                                            "Seit meinen 3 Kindern bin ich nur noch in Teilzeit an der Klinik tätig. Mit den Diensten von Consulmedics wird die Gehaltsdifferenz problemlos ausgeglichen.  Ich verdiene mehr und habe trotzdem genug Zeit für meine Familie. Perfekt".
                                            "

                                        </h4>

                                        <div class="tm-title color">Michal B.</div>

                                        <div class="tm-subtitle white">Gorgeous Manager</div>

                                        <div class="tm-stars">

                                            <i class="fa fa-star active"></i>

                                            <i class="fa fa-star active"></i>

                                            <i class="fa fa-star active"></i>

                                            <i class="fa fa-star active"></i>

                                            <i class="fa fa-star"></i>

                                        </div>

                                    </div>

                                </div>

                                <div class="item text-center">

                                    <!-- <img src="images/avatar/avatar_150x150.jpg" alt="" /> -->

                                    <div class="tm-content">

                                        <h4 class="white">

                                            "Nach über 15 Jahren Kliniktätigkeit möchte ich nun beruflich kürzer treten. Mit Consulmedics gelingt das problemlos, ohne Gehaltseinbußen. Und die Tätigkeit als Vertretungsarzt im Kassenärztlichen Bereitschaftsdienst öffnet mir nochmal neue Blickwinkel."

                                        </h4>

                                        <div class="tm-title color">Jana S.</div>

                                        <div class="tm-subtitle white">Main Entrepeneur</div>

                                        <div class="tm-stars">

                                            <i class="fa fa-star active"></i>

                                            <i class="fa fa-star active"></i>

                                            <i class="fa fa-star active"></i>

                                            <i class="fa fa-star active"></i>

                                            <i class="fa fa-star active"></i>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>
        <div class="section get-instant inner-page-get-instant">
            <div class="container">
                <div class="instant-cont">
                    <a href="#"><i class="fa fa-star-o"></i></i> Get Instant Access</a>
                    <p style="color: rgb(255, 255, 255); font-size: 21px;" class="man resp-p w-300">Now is the time! Pick up your copy of the ultimate WordPress theme today.</p>
                </div>
            </div>
        </div><!--  get-instant -->
        <!-- team section -->
        <div id="about" class="team-section-tn">
            <div class="container">
                <h2 class="fw-800  dark-color section-title mb-1" style="text-align: center; color: #fff !important;">TEAM - UNTERNEHMEN - PHILOSOPHIE</h2>
                <div class="row leftright-team-area second-page-team">
                    <div class="col-md-6 heli-side">
                        <div class="row left-team-area">
                            <!-- <h6 class="dark-color">über Uns: Team &amp; Unternehmen</h6> -->
                            <div class="bbb">
                                <div class="imgs-row">
                                    <div class="col-md-6 b1 popup-img">
                                        <img src="images/recent1.jpg">
                                    </div>
                                    <div class="col-md-6 b1">
                                        <img src="images/newimagebus.jpg">
                                    </div>
                                    <div class="col-md-6 b1">
                                        <img src="images/cockpit.jpg">
                                    </div>

                                </div> <!-- imgs row -->

                                <div class="popup-div">
                                    <div class="inner">
                                        <img src="images/recent1.jpg">
                                    </div>
                                </div>
                                <div class="cont-about-row">
                                    <div class="col-md-6 b2 about-text">
                                        <p>Die Consulmedics GmbH wurde im Jahr 2018 von Swen Burdack (Anästhesist und Intensivmediziner) gegründet.
                                            Später wurde das Team um weitere Partner aus den Bereichen Medizin, sowie Spezialisten aus Marketing,
                                            Terminmanagement und Banking/Recht erweitert und ist seit jeher fest in der Vermittlung von
                                            Kassenärztlichen Bereitschaftsdiensten etabliert.

                                            Der Gründer und Geschäftsführer Hr. Burdack verfügt über langjährige (seit 1997) Erfahrung in der
                                            Akut-/Notfallmedizin, ist regelmäßig tätig als Notarzt in Luftrettung und Bodenrettung und natürlich im
                                            Kassenärztlichen Bereitschaftsdienst engagiert aktiv.<br>
                                            Presseartikel:
                                            <a href="https://www.lr-online.de/lausitz/senftenberg/senftenberger-luftretter-fliegen-bei-hitze-im-hochbetrieb_aid-44833431" target="_blank"> https://www.lr-online.de/lausitz/senftenberg/senftenberger-luftretter-fliegen-bei-hitze-im-hochbetrieb_aid-44833431</a>
                                            <a href="https://presse.adac.de/meldungen/adac-stiftung/luftrettung/nightvision-christoph-brandenburg.html" target="_blank"> https://presse.adac.de/meldungen/adac-stiftung/luftrettung/nightvision-christoph-brandenburg.html</a>
                                            Wir sind personell hochqualifiziert aufgestellt und bieten als flexibles Startup innovative technologische Lösungen für
                                            sämtliche anfallende Geschäftsprozesse rund um den Kassenärztlichen Bereitschaftsdienst.

                                            Neben der digitalen Abwicklung der Patientendaten liegt unser Hauptaugenmerk auf zukunftsträchtige
                                            Projekte, wie zB. komplexe telemedizinische Lösungsansätze.

                                            Als ärztlich geleitetes Unternehmen verknüpfen wir somit technische Innovationen mit medizinischem Fachwissen und sind
                                            darin Pionier und Vorreiter für die gesamte Branche.
                                            Wir sehen uns als verlässlichen und vertrauensvoller Ansprechpartner für alle Beteiligten im Kassenärztlichen Bereitschaftsdienst.
                                            Unser Ziel ist es, zum einen optimalen professionellen Service für unsere Auftraggeber (Praxen, MVZs) zu gewährleisten.
                                            Andererseits werden auch unsere Vertretungsärzte bestmöglich bei den Dienstübernahmen betreut und maximal von administrativen
                                            Tätigkeiten entlastet
                                            Bei aller Komplexität: Unsere Prämisse ist stets eine freundliche, kollegiale und auf Augenhöhe stattfindende Kommunikation mit allen
                                            Beteiligten und Partnern.
                                        </p>
                                        <span>Read More...</span>
                                        <i style="font-style: normal;display: none;">Less</i>
                                    </div>
                                    <div class="col-md-6 b2 extra">
                                        <p>We are a growing company with a focus on the future. <span id="dots">...</span><span id="more">Our mission is to change the way people think about widgets and have fun while doing it.</span></p>  <a href="javascript:void(0)" onclick="myFunction()" id="myBtn" class="team-read-more">Read More...</a>
                                    </div>
                                    <div class="col-md-6 b2 extra">
                                        <p>We are a growing company with a focus on the future. Our mission is <!-- <span id="dots">...</span> --><span id="mores"  style="display: none"> to change the way people think about widgets and have fun while doing it.</span></p>  <a href="javascript:void(0)" id="myBtns" class="team-read-more">Read More... <span> Read Less</span></a>
                                    </div>


                                </div>	<!-- CONTENT -->
                                <!-- 	</div>
                                    <div class="bbb"> -->
                                <!-- 	    							</div>        close div
                                                                    <div  class="bbb">
                                 -->
                            </div> <!-- close div -->
                        </div>
                    </div>
                    <div class="col-md-6 grid-teams second-page-team">
                        <div class="row">
                            <div class="col-md-6 immg">
                                <div class="one">
                                    <img src="images/team/user_2.png" class="img-l">
                                    <h6 class="title-team img-l">Lorem | <span>Developer</span></h6>
                                    <!--<p>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                    </p>
                                    <a href=""><i class="fa fa-linkedin"></i> | Connect </a>
                                    <div class="social-teams">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                    </div> -->
                                </div>
                            </div>
                            <div class="col-md-6 immg">
                                <div>
                                    <img src="images/1.png">
                                    <h6 class="title-team">Lorem | <span>Developer</span></h6>
                                    <!--<p>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                    </p>
                                    <a href=""><i class="fa fa-linkedin"></i> | Connect </a>
                                    <div class="social-teams">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-google-plus"></i></a>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 immg">
                                <div class="one">
                                    <img src="images/2.png" class="img-l">
                                    <h6 class="title-team img-l">Lorem | <span>Developer</span></h6>
                                    <!--<p>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                        </p>
                                        <a href=""><i class="fa fa-linkedin"></i> | Connect </a>
                                        <div class="social-teams">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                        </div> -->
                                </div>
                            </div>
                            <div class="col-md-6 immg">
                                <div>
                                    <img src="images/3.png">
                                    <h6 class="title-team">Lorem | <span>Developer</span></h6>
                                    <!--	<p>
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                        </p>
                                        <a href=""><i class="fa fa-linkedin"></i> | Connect </a>
                                        <div class="social-teams">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                        </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="section">
            <!-- <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div  class="footer_text">
                            <h4 class="h-widget"><i class="fa fa-chevron-right"></i> Über uns: Team & Philosophies</h4>
                            <div class="textwidget">We are a growing company with a focus on the future. Our mission is to change the way people think about widgets and have fun while doing it. <a href="#">Read More</a>.
                            </div>
                        </div>
                    </div> <div class="col-sm-4">
                        <div  class="footer_text">
                            <h4 class="h-widget"><i class="fa fa-chevron-right"></i> Philosophy</h4>
                            <div class="textwidget">It's important to us to do unto others as we would have them do unto us - both personally and professionally. Learn more about how we do things.<a href="#">Read More</a>.
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div  class="footer_text">
                            <h4 class="h-widget"><i class="fa fa-chevron-right"></i> Corporate</h4>
                            <div class="textwidget">Our headquarters are located at 123 Interweb Way, and we invite you to stop by our showroom anytime. We're open 24 hours a day, 7 days a week. <a href="#">Read More</a>.
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- Footer bottom -->
            <div class="foot-bot">
                <div class="container">
                    <ul class="foot-nav-ul">
                        <li><a class="" href="#fullscreen-banner">Home</a></li>

                        <li><a class="" href="#vorteile">VORTEILE</a></li>

                        <li><a class="" href="#ablauf">ABLAUF</a></li>
                        <li><a class="" href="#testimonial">MEINUNGEN</a></li>
                        <li><a class="" href="#about">ÜBER UNS</a></li>

                        <li><a class="" href="#contact">Kontakt</a></li>
                    </ul>
                    <div class="social-global">
                        <!-- 					        	<a href="#" target="_blank">
                                                            <i class="fa fa-twitter-square"></i>
                                                        </a>
                                                        <a href="#" target="_blank">
                                                            <i class="fa fa-google-plus-square"></i>
                                                        </a>
                                                        <a href="#" target="_blank">
                                                            <i class="fa fa-linkedin-square"></i>
                                                        </a>
                                                        <a href="#" target="_blank">
                                                            <i class="fa fa-pinterest-square"></i>
                                                        </a>
                                                        <a href="#" target="_blank">
                                                            <i class="fa fa-dribbble"></i>
                                                        </a>
                                                        <a href="#" target="_blank">
                                                            <i class="fa fa-rss-square"></i>
                                                        </a> -->
                        <a href="#" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="#" target="_blank">
                            <i class="fa fa-rss-square"><img src="./images/vertreter/xing.png"></i>
                        </a>
                    </div>
                    <div class="colophon-content">
                        <p style="letter-spacing: 2px; text-transform: uppercase;">
                            <!-- Powered By The <a href="#">Swenb</a> -->
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<div class="go-to-top">

    <div class="arrows">
        <i class="fa fa-chevron-up"></i>
    </div>

</div>

<!-- popup start -->
<!-- Modal -->
<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pulse Video</h4>
            </div>
            <div class="modal-body">
                <iframe src="https://www.youtube.com/embed/G0dzLanYW1E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>

    </div>
</div>
<!--  -->

<script type="text/javascript" src="js/jquery.min.js"></script>

<script type="text/javascript" src="js/jquery-migrate.min.js"></script>

<script type="text/javascript" src="js/bootstrap.min.js"></script>

<script type="text/javascript" src="js/jquery.singlePageNav.js"></script>

<script type="text/javascript" src="js/modernizr-2.7.1.min.js"></script>

<script type="text/javascript" src="js/classie.js"></script>

<script type="text/javascript" src="js/owl.carousel.min.js"></script>

<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>

<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>

<script type="text/javascript" src="js/jquery.isotope.init.js"></script>

<script type="text/javascript" src="js/jquery.countTo.js"></script>

<script type="text/javascript" src="js/bootstrap-progressbar.min.js"></script>

<script type="text/javascript" src="js/jquery.equalheights.min.js"></script>

<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>

<script type='text/javascript' src='js/jquery.prettyPhoto.js'></script>

<script type="text/javascript" src="js/off-cavnas.js"></script>

<script type='text/javascript' src='js/waypoints.min.js'></script>

<script type='text/javascript' src='js/wow.min.js'></script>

<script type="text/javascript" src="js/functions.js?v=6"></script>

<script type="text/javascript" src="js/script.js?v=3"></script>





<script type="text/javascript" src="js/extensions/revolution.extension.actions.min.js"></script>

<script type="text/javascript" src="js/extensions/revolution.extension.layeranimation.min.js"></script>

<script type="text/javascript" src="js/extensions/revolution.extension.kenburn.min.js"></script>

<script type="text/javascript" src="js/extensions/revolution.extension.navigation.min.js"></script>

<script type="text/javascript" src="js/extensions/revolution.extension.migration.min.js"></script>

<script type="text/javascript" src="js/extensions/revolution.extension.parallax.min.js"></script>

<script src="js/jquery.carouFredSel-6.2.0-packed.js" type="text/javascript"></script>


<script type="text/javascript">

    $(function() {

        $('#slider').carouFredSel({
            responsive: true,

            width: '100%',
            align: false,

            items: 5,

            items: {

                width: $('#wrapper').width() *0.25,

                height: 500,

                visible: 1,

                minimum: 1

            },

            scroll: {

                items: 1,

                timeoutDuration : 1500,

                pauseOnHover:true,

                onBefore: function(data) {



                    //	find current and next slide

                    var currentSlide = $('.slide.active', this),

                        nextSlide = data.items.visible,

                        _width = $('#wrapper').width();



                    //	resize currentslide to small version

                    currentSlide.stop().animate({

                        width: _width *0.25

                    });

                    currentSlide.removeClass( 'active' );

                    currentSlide.find('.module').addClass('last-line')



                    //	hide current block

                    //data.items.old.add( data.items.visible ).find( '.slide-block' ).stop().fadeOut();



                    //	animate clicked slide to large size

                    nextSlide.addClass( 'active' );





                    nextSlide.stop().animate({

                        width: _width * 0.5

                    });









                },

                onAfter: function(data) {

                    //	show active slide block

                    data.items.visible.last().find( '.module' ).removeClass('last-line');

                }

            },

            onCreate: function(data){



                //	clone images for better sliding and insert them dynamacly in slider

                var newitems = $('.slide',this).clone( true ),

                    _width = $('#wrapper').width();



                $(this).trigger( 'insertItem', [newitems, newitems.length, false] );



                //	show images

                $('.slide', this).fadeIn();

                $('.slide:first-child', this).addClass( 'active' );

                $('.slide', this).width( _width *0.25 );



                //	enlarge first slide

                $('.slide:first-child', this).animate({

                    width: _width * 0.5

                });





                //	show first title block and hide the rest

                //$(this).find( '.slide-block' ).hide();





            }

        });



        //	Handle click events

        $('#slider').children().click(function(e) {









            $('#slider').trigger( 'slideTo', [this]);

        });



        //	Enable code below if you want to support browser resizing

        $(window).resize(function(){



            var slider = $('#slider'),

                _width = $('#wrapper').width();



            //	show images

            slider.find( '.slide' ).width( _width *0.25 );



            //	enlarge first slide

            slider.find( '.slide.active' ).width( _width * 0.5 );



            //	update item width config

            slider.trigger( 'configuration', ['items.width', _width *0.25] );

        });



    });

    $(document).ready(function(){
        $('.side-menu').click(function(){
            $('.main-menu').slideToggle();
        });
    });

    /*scroll top*/
    myID = document.getElementById("hexagon-imgs");

    var myScrollFunc = function() {
        var y = window.scrollY;
        if (y >= 600) {
            $('.go-to-top').show();
        } else {
            $('.go-to-top').hide();
        }
    };

    window.addEventListener("scroll", myScrollFunc);
</script>
<!--Start of Tawk.to Script-->
<!-- <script type="text/javascript">

    function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");
  var btnText = document.getElementById("myBtns");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more";
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less";
    moreText.style.display = "inline";
  }
}

var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d2066c522d70e36c2a46be6/1df3fd6ej'
;;
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script> -->
<!--End of Tawk.to Script-->

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5d6d0e8feb1a6b0be60a85ba/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    /*		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/564f634590d1bced690e0633/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })(); */

    /*to top*/
    $(document).ready(function(){
        /*$("div#contact h2.section-title").click(function() {
            $("html").animate({ scrollTop: 0 }, "slow");
        });*/
        $("a#myBtns").click(function(){
            $("span#mores").slideToggle(700);
            $(this).toggleClass("Read_Less");
        });
        // readmore

        $(".about-text span").click(function(){
            $(".about-text").addClass("active");
        });
        $(".about-text i").click(function(){
            $(".about-text").removeClass("active");
        });
        // Popuop
        $(".popup-img").click(function(){
            $(".popup-div").fadeIn(700);
        });
        $(".popup-div").click(function(){
            $(".popup-div").fadeOut(700);
        });


    });
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }

    jQuery(document).ready(function($){
        $(".contact-form").submit(function(e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.
            var firstname = jQuery("#author").val();
            var email = jQuery("#email").val();
            var comments = jQuery("#comment").val();
            jQuery(".show_response").html('');
            console.log(validateEmail(email));
            if (validateEmail(email) && email!="" && firstname!="" && comments!="") {
                var form = $(this);
                var url = form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), // serializes the form's elements.
                    success: function(data)
                    {
                        //alert(data); // show response from the php script.
                        console.log(data);
                        if(data="success"){
                            jQuery("#author").val('');
                            jQuery("#email").val('');
                            jQuery("#comment").val('');
                            jQuery(".show_response").html('Your request successfully sent. Thanks .....!');
                        }
                        else{
                            jQuery(".show_response").html('Something went wrong');
                        }
                    }
                });
            }
            else{
                jQuery(".show_response").html('Fill all fields and make sure email address is valid ....!');
            }

        });
    });
</script>
<!--End of Tawk.to Script-->

</body>

</html>
