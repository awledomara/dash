@extends('layouts.dashboard')

@section('content')
<div class="card">
    <div class="card-body table-responsive">
        <a class="btn btn-github float-right" href="{{route('users.create')}}">{{__('Add New System User')}}</a>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>
                    {{__("Name")}}
                </th>
                <th>
                    {{__("Email")}}
                </th>
                <th>
                    {{__("Role")}}
                </th>
                <th>
                    {{__("Address")}}
                </th>
                <th>
                    {{__('Status')}}
                </th>
                <th>
                    {{__("Created At")}}
                </th>
                <th>
                    {{__('Actions')}}
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>
                        {{$user->first_name}} {{$user->last_name}}
                    </td>
                    <td>
                        {{$user->email}}
                    </td>
                    <td>
                    {{$user->roles()->first()->name}}
                    </td>
                    <td>
                        @if($user->address)
                         {{$user->address->street}} ,{{$user->address->city}} {{$user->address->postal_code}}<br> {{$user->address->country}}
                        @endif
                    </td>
                    <td>
                        <label class="toggle-switch">
                            <input type="checkbox" checked="{{$user->status??0}}">
                            <span class="toggle-slider round"></span>
                        </label>
                    </td>
                    <td>
                        {{$user->created_at->format('d M Y h:m:s')}}
                    </td>
                    <td>
                        <a href="{{route('users.edit',['id'=>$user->id])}}" >
                            Edit
                        </a>

                    </td>
                </tr>
            @endforeach
            <tr>
                <td colspan="6" class="text-center">
                    {{$users->links()}}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection