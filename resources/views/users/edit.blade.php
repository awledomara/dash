@extends('layouts.dashboard')

@section('content')

    <div class="row">
        <div class="col-md-12 grid-margin ">
            <form action="{{route('users.update',$user)}}" method="POST">
                {{csrf_field()}}
                {{method_field('PUT')}}
            <div class="card">
                <div class="card-body">

                        <ul class="nav nav-tabs " role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">
                                    {{__('General Information')}}
                                </a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="home-1" role="tabpanel" aria-labelledby="home-tab">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="first_name ">{{__('First Name')}}</label>
                                            <input class="form-control  @error('first_name') is-invalid @enderror" type="text" name="first_name"
                                                   value="{{$user->first_name}}">
                                            @error('first_name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group ">
                                            <label for="last_name">{{__('Last Name')}}</label>
                                            <input class="form-control @error('last_name') is-invalid @enderror" type="text" name="last_name" value="{{$user->last_name}}">
                                            @error('last_name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">{{__('Email')}}</label>
                                            <input class="form-control @error('email') is-invalid @enderror" type="text" name="email"value="{{$user->email}}" >
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">{{__('Temporarily Password')}}</label>
                                            <input class="form-control @error('email') is-invalid @enderror" type="text" name="password" >
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="first_name ">{{__('Date Of Birth')}}</label>
                                            <input class="form-control  @error('date_of_birth') is-invalid @enderror"
                                                   type="text"
                                                   name="date_of_birth"
                                                   value="{{$user->date_of_birth}}">
                                            @error('date_of_birth')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group ">
                                            <label for="role">{{__('Role')}}</label>
                                            <select class="form-control @error('role') is-invalid @enderror"
                                                    type="text"
                                                    name="role" >
                                                @foreach($roles as $role)
                                                    <option value="{{$role->id}}" {{$user->hasRole($role->id)?'selected':''}}>{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('role')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card bg-light">
                                            <div class="card-body">
                                                <h2>Permissions</h2>
                                                <p>
                                                    @foreach($permissions->chunk(3) as $list)
                                                        <div class="row">
                                                             @foreach($list as $permission)
                                                                 <div class="col-md-4 col-sm-6">
                                                                       <div class="form-group">
                                                                             <div class="form-check">
                                                                                 <label class="form-check-label">
                                                                                     <input type="checkbox" name="permissions[]" class="form-check-input" value="{{$permission->id}}"
                                                                                           {{in_array($permission->id,$user_permissions)?'checked':''}}>
                                                                                     {{__($permission->name)}}
                                                                                     <i class="input-helper"></i></label>
                                                                             </div>

                                                                        </div>
                                                                 </div>
                                                             @endforeach
                                                        </div>
                                                    @endforeach
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>



                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-github float-right ">{{__('Update')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
@endsection