@extends('layouts.dashboard')

@section('content')

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('doctors.store')}}" method="POST">
                        {{csrf_field()}}

                        <h4 class="card-title">{{__('Add New Doctor Account')}}</h4>
                        @if($errors->count())
                            <div class="alert alert-warning">
                                {{$errors->first()}}
                            </div>

                        @endif
                        <ul class="nav nav-tabs " role="tablist">
                            <li class="nav-item">
                                <a class="nav-link " id="home-tab" data-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">
                                    {{__('General Information')}}
                                </a>
                            </li>


                        <!--
                            <li class="nav-item">
                                <a class="nav-link" id="contact-person-tab" data-toggle="tab" href="#contact-person" role="tab" aria-controls="contact-person-1" aria-selected="false">
                                    {{__('Contact Person')}}
                            </a>
                        </li>
-->
                            <li class="nav-item" disabled>
                                <a class="nav-link" id="address-tab" data-toggle="tab" href="#address-1" role="tab" aria-controls="address-1" aria-selected="false" >
                                    {{__('Addresses')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact-1" role="tab" aria-controls="contact-1" aria-selected="false">
                                    {{__('Contact Information')}}
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="financial-tab" data-toggle="tab" href="#financial-1" role="tab" aria-controls="contact-1" aria-selected="false">
                                    {{__('Financial Information')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="docs-tab" data-toggle="tab" href="#docs-1" role="tab" aria-controls="contact-1" aria-selected="false">
                                    {{__('Related Documents')}}
                                </a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="home-1" role="tabpanel" aria-labelledby="home-tab">

                            <!-- <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">{{__('Practice Name')}}</label>
                                            <input class="form-control   @error('name') is-invalid @enderror" type="text" name="name" value="{{old('name')}}">
                                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                </div>
                            </div>
                        </div>
-->
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="title">{{__("Title")}}</label>
                                                    <select name="title" id="title" class="form-control ">
                                                        <option value="Herr"  {{old('title')=='Herr'?'selected':''}}>Herr</option>
                                                        <option value="Frau"  {{old('title')=='Frau'?'selected':''}}>Frau</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="grade">{{__("Grade")}}</label>
                                                    <select name="grade" id="grade" class="form-control ">

                                                        @foreach($grades as $grade)
                                                            <option value="{{$grade->name}}" {{old('grade')==$grade->name?'selected':''}}>{{$grade->name}}</option>
                                                        @endforeach

                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="first_name ">{{__('First Name')}}</label>
                                                    <input class="form-control   @error('first_name') is-invalid @enderror"
                                                           type="text" name="first_name"
                                                           value="{{old('first_name')}}"
                                                    >
                                                    @error('first_name')
                                                    <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group ">
                                                    <label for="last_name">{{__('Last Name')}}</label>
                                                    <input class="form-control  @error('last_name') is-invalid @enderror"
                                                           type="text" name="last_name"
                                                           value="{{old('last_name')}}"
                                                    >
                                                    @error('last_name')
                                                    <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="speciality">{{__("Speciality")}}</label>
                                                    <select name="speciality_id" id="speciality" class="form-control ">
                                                        <option value=""></option>
                                                        @foreach($specialities as $speciality)
                                                            <option value="{{$speciality->id}}"  {{old('speciality_id')==$speciality->id?'selected':''}}>{{$speciality->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="notes">{{__('Notes')}}</label>
                                                    <textarea type="text" class="form-control " name="notes" rows="10">{{old('notes')}}</textarea>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <list-services l-services="{{$services}}" selected="[]"></list-services>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="contact-1" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="first_name">{{__('Primary Email')}}*</label>
                                            <input class="form-control   @error('email') is-invalid @enderror" type="text" value="{{old('email')}}" name="email">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">

                                        <div class="form-group">
                                            <label for="last_name">{{__('Secondary Email')}}({{__('optional')}})</label>
                                            <input class="form-control  @error('secondary_email') is-invalid @enderror"
                                                   type="text" name="secondary_email"
                                                   value="{{old('secondary_email')}}">
                                            @error('secondary_email')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="phone">{{__('Primary Phone')}}*</label>
                                            <input class="form-control  @error('phone1') is-invalid @enderror"
                                                   type="text" name="phone1"
                                                   value="{{old('phone1')}}" >
                                            @error('phone1')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="last_name">{{__('Secondary Phone')}}({{__('optional')}})</label>
                                            <input class="form-control   @error('phone2') is-invalid @enderror" type="text" name="phone2"
                                                   value="{{old('phone2')}}" >
                                            @error('phone2')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="fax">{{__('Fax')}}</label>
                                            <input class="form-control   @error('fax') is-invalid @enderror" type="text" value="{{old('fax')}}">
                                            @error('fax')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="financial-1" role="tabpanel" aria-labelledby="financial-1-tab">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="iban">IBAN</label>
                                            <input type="text" class="form-control  @error('iban') is-invalid @enderror" name="iban" value="{{old('iban')}}">
                                            @error('iban')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="owner">Owner</label>
                                            <input type="text" class="form-control   @error('owner') is-invalid @enderror" name="owner" value="{{old('owner')}}">
                                            @error('owner')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" name="debit_authorization" class="form-check-input" {{old('debit_authorization')==1?'checked':''}}>
                                                    {{__('Debit Authorization')}}
                                                    <i class="input-helper"></i></label>
                                            </div>

                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="tab-pane fade" id="docs-1" role="tabpanel" aria-labelledby="docs-tab">

                                <div class="row">
                                    <div class="col-md-3">

                                    </div>
                                    <div class="col-md-6">
                                        <div class="jumbotron text-center">
                                            Please save the Practice data first to upload documents
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="address-1" role="tabpanel" aria-labelledby="address-tab" >
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="street">{{__('Street Address')}}</label>
                                            <input type="text" class="form-control @error('street') is-invalid @enderror"
                                                   name="street"
                                            >
                                            @error('street')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="city">{{__('City')}}</label>
                                            <input type="text" class="form-control @error('city') is-invalid @enderror"
                                                   name="city"
                                            >
                                            @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="postalCode">{{__('Postal Code')}}</label>
                                            <input type="text" class="form-control @error('postal_code') is-invalid @enderror"
                                                   name="postal_code"

                                            >
                                            @error('postal_code')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-github float-right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">

        $(document).ready(function(){

            function mvz_fileds(){
                $("#mcc input").each((index,item)=>{

                    if(!document.getElementById("in_mvz").checked){
                        if(item.name!="in_mvz"){
                            item.setAttribute('disabled',true)
                        }
                    }


                })
            }
            mvz_fileds()

            $('#mcc').on('click','#in_mvz', function (event) {
                if(this.checked){
                    $("#mcc input").each((index,item)=>{

                        item.removeAttribute('disabled')
                    })
                }else{
                    $("#mcc input").each((index,item)=>{
                        if(item.name!="in_mvz"){
                            item.setAttribute('disabled',true)
                        }

                    })
                }

            })
        })
    </script>
@endsection
