@extends('layouts.dashboard')
@section('head')

@endsection
@section('content')

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                    <form action="{{route('doctors.store')}}" method="POST"  enctype="multipart/form-data">
                        {{csrf_field()}}

                        <h4 class="card-title">{{__('Update Doctor Account')}}</h4>

                        @if($errors->count())
                            <div class="alert alert-warning">
                                {{$errors->first()}}
                            </div>

                        @endif
                        <ul class="nav nav-tabs " role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">
                                    {{__('General Information')}}
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact-1" role="tab" aria-controls="contact-1" aria-selected="false">
                                    {{__('Contact Information')}}
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="docs-tab" data-toggle="tab" href="#docs-1" role="tab" aria-controls="contact-1" aria-selected="false">
                                    {{__('Related Documents')}}
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="title">{{__("Title")}}</label>
                                            <select name="title" id="title" class="form-control  @error('title') is-invalid @enderror">
                                                <option value="Herr" {{old('title')=='Herr'?'selected':''}}>Herr</option>
                                                <option value="Frau"  {{old('title')=='Frau'?'selected':''}}>Frau</option>
                                            </select>
                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="grade">{{__("Grade")}}</label>
                                            <select name="grade" id="grade" class="form-control @error('grade') is-invalid @enderror">

                                                @foreach($grades as $grade)
                                                    <option value="{{$grade->name}}" {{old('grade')==$grade->name?'selected':''}}>{{$grade->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('first_name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="first_name ">{{__('First Name')}}</label>
                                            <input class="form-control  @error('first_name') is-invalid @enderror"
                                                   type="text" name="first_name" value="{{old('first_name')}}">
                                            @error('first_name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group ">
                                            <label for="last_name">{{__('Last Name')}}</label>
                                            <input class="form-control @error('last_name') is-invalid @enderror" type="text"
                                                   name="last_name" value="{{old('last_name')}}">
                                            @error('last_name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="speciality">{{__("Speciality")}}</label>
                                            <select name="speciality_id" id="speciality" class="form-control  @error('speciality_id') is-invalid @enderror">
                                                <option value=""></option>
                                                @foreach($specialities as $speciality)
                                                    <option value="{{$speciality->id}}" {{$speciality->id==old('speciality_id')?'selected':''}}>{{$speciality->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('speciality_id')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="notes">{{__('Notes')}}</label>
                                            <textarea type="text" class="form-control" name="notes" rows="20">{{old('notes')}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="service_area_id">{{__("Service Area")}}</label>
                                            <select name="service_area_id" id="service" class="form-control  @error('service_area_id') is-invalid @enderror">
                                                <option value=""></option>
                                                @foreach($services as $service)
                                                    <option value="{{$service->id}}" {{$service->id==old('service_area_id')?'selected':''}}>{{$service->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('service_area_id')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="tab-pane fade" id="contact-1" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="primary_email">{{__('Primary Email')}}*</label>
                                            <input class="form-control  @error('email') is-invalid @enderror" type="text" name="email" value="{{old('email')}}">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">

                                        <div class="form-group">
                                            <label for="person_email">{{__('Secondary Email')}}</label>
                                            <input class="form-control @error('secondary_email') is-invalid @enderror"
                                                   type="text" name="secondary_email"  value="{{old('secondary_email')}}">
                                            @error('secondary_email')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="person_email">{{__('Primary Phone')}}</label>
                                            <input class="form-control @error('phone1') is-invalid @enderror"
                                                   type="text" name="phone1"
                                                   value="{{old('phone1')}}">
                                            @error('phone1')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="person_email">{{__('Secondary Phone')}} ({{__('Optional')}})</label>
                                            <input class="form-control @error('phone2') is-invalid @enderror" type="text" name="phone2"
                                                   value="{{old('phone2')}}">
                                            @error('phone2')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="street">{{__('Street Address')}}</label>
                                            <input type="text" class="form-control @error('street') is-invalid @enderror"
                                                   name="street"
                                                   value="{{old('street')}}"
                                            >
                                            @error('street')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="city">{{__('City')}}</label>
                                            <input type="text" class="form-control @error('city') is-invalid @enderror"
                                                   value="{{old('city')}}"
                                                   name="city"
                                            >
                                            @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="postalCode">{{__('Postal Code')}}</label>
                                            <input type="text" class="form-control @error('postal_code') is-invalid @enderror"
                                                   name="postal_code"
                                                   value="{{old('postal_code')}}"

                                            >
                                            @error('postal_code')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane fade" id="docs-1" role="tabpanel" aria-labelledby="docs-tab">


                                <div class="row">
                                    <div class="col-md-3">

                                    </div>
                                    <div class="col-md-6">
                                        <div class="jumbotron text-center">
                                            Please save the Doctor data first to upload documents
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-github btn-rounded btn-rounded float-right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('js')


@endsection
