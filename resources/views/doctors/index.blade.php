@extends('layouts.dashboard')
@section('head')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/af-2.3.3/b-1.5.6/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body table-responsive">
                    <h4 class="card-title" >{{__('List of Doctors')}}</h4>
                    <a class="btn btn-github btn-rounded float-right btn-sm" href="{{route('doctors.create')}}">{{__('Add New Doctor')}}</a>
                    <table class="table table-striped" id="data-table" style=" width: 100%">
                        <thead>
                        <tr>

                            <th>
                                {{__("Full Name")}}
                            </th>
                            <th>
                                {{__("Service Area")}}
                            </th>
                            <th>
                                {{__("Emails")}}
                            </th>
                            <th>
                                {{__("Address")}}
                            </th>
                            <th>
                                {{__('Created At')}}
                            </th>
                            <th>
                                {{__('Action')}}
                            </th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('js')
@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/af-2.3.3/b-1.5.6/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>
    <!--
   <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
 -->
    <script>
        $(document).ready( function () {
            $('#data-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('datatable.doctors.index') }}",
                "columns": [
                    {   "data": "user" ,
                        'name':"user.first_name",
                        "render": function(d) {
                            if(d!=null){
                                return d.first_name + ' ' + d.last_name;
                            }

                        }

                    },

                    {   "data": "service_area" ,
                        'name':"serviceArea.name",
                        "defaultContent":"Not Set",
                        "render": function(d) {

                            if(d!=null){
                                return  d.name
                            }

                        }

                    },
                    {   "data": "user" ,
                        'name':"user.id",
                        "render": function(d) {
                            return d.email
                        }

                    },

                    {   "data": "address" ,
                        'name':"address.id",
                        "render": function(d) {
                            return d.street+'<br> '+d.city+', '+d.postal_code
                        }

                    },
                    {
                        "data":"created_at",

                        "render":function (d) {
                            return moment(d).format("DD.MM.YYYY H:m")
                        }
                    },
                    {
                        "data":null,
                        "name":"id",
                        "render":function (d) {
                            var buttons='<a href="{{route('doctors.index')}}/'+d.id+'/edit">{{__('Edit')}}</a> &nbsp;';

                            return buttons+='<a href="{{route('doctors.index')}}/'+d.id+'">{{__('Details')}}</a>';
                        }
                    }


                ]
            });
        });
    </script>
@endsection
@endsection

