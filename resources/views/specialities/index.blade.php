@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-body table-responsive">
            <a class="btn btn-github float-right"
               href="{{route('specialities.create')}}">{{__('Add New Speciality')}}</a>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>
                        {{__("Name")}}
                    </th>

                    <th>
                        {{__("Created At")}}
                    </th>
                    <th>
                        {{__('Actions')}}
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($specialities as $speciality)
                    <tr>
                        <td>
                            {{$speciality->name}}
                        </td>

                        <td>
                            {{$speciality->created_at->format('d M Y h:m:s')}}
                        </td>
                        <td>
                            <a href="{{route('specialities.edit',['id'=>$speciality->id])}}">
                                Edit
                            </a>
                            <form method="POST" action="{{route('specialities.destroy',['id'=>$speciality->id])}}"
                                  id="id_{{$speciality->id}}">
                                {{csrf_field()}} {{method_field("DELETE")}}
                                <a href="#"
                                   onclick="document.getElementById('id_{{$speciality->id}}').submit()">{{__('Delete')}}</a>
                            </form>

                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="6" class="text-center">
                        {{$specialities->links()}}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('js')

@endsection