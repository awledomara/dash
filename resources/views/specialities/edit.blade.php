@extends('layouts.dashboard')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{__('Create New Speciality')}}
                    </div>

                    <div class="card-body">

                        <form action="{{route('specialities.update',$speciality->id)}}" method="POST">
                            {{csrf_field()}}
                            {{method_field('PATCH')}}
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="">{{__('Speciality Name')}}</label>
                                        <input type="text" class="form-control" name="name" value="{{$speciality->name}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-github float-right">{{__('Save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection