@extends("emails.layout")
@section('content')
    @foreach($missions as $mission)
             <table width="100%" style="background-color: #eee;padding: 15px">

        <tbody>
        <tr>
            <td>

            </td>
            <td class="content" width="80%">
                <table style="border-radius: 10px">

                    <tr>
                        <td>
                            {{$mission->total_hours()}} hours
                        </td>
                    </tr>
                    <tr>
                        <td>
                            *von: {{$mission->start_date->format('d.m.Y H:m')}}<br>
                            *bis: {{$mission->end_date->format('d.m.Y H:m')}}<br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            *KV-Gebiet {{optional($mission->service_area)->name}}<br>
                            *{{optional($mission->service_area)->type==0?'Sitzdienst':'Fahrdienst'}}
                        </td>
                    </tr>
                </table>
            </td>
            <td>

            </td>
        </tr>

        </tbody>
    </table>

    @endforeach

@endsection
