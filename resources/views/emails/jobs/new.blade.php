@extends("emails.layout")
@section('content')
<table width="100%" style="background-color: #eee">

   <tbody>
   <tr>
       <td>

       </td>
       <td class="content" width="80%">
           <table style="border-radius: 10px">
               <tr>
                   <td>
                       Wir suchen eine Vertretung für den KV-Bereitschaftsdienst im Gebiet {{optional($mission->service_area)->name}}
                   </td>
               </tr>
               <tr>
                   <td>
                       {{$mission->total_hours()}} h
                   </td>
               </tr>
               <tr>
                   <td>
                       *von: {{$mission->start_date->format('d.m.Y H:m')}}<br>
                       *bis: {{$mission->end_date->format('d.m.Y H:m')}}<br>
                   </td>
               </tr>
               <tr>
                   <td>
                       *{{optional($mission->service_area)->type==0?'Sitzdienst':'Fahrdienst'}}
                   </td>
               </tr>
               <tr>
                   <td>
                       *Honorar: 396€ + zusätzliche Eigenabrechnung aller Privatpatienten + alle Totenscheine +
                       50€ pro Patient ab dem 5. besuchten Patienten (bei 12 Std.-Dienst) oder
                       ab dem 7. besuchten Patienten (bei24 Std.-Dienst)
                   </td>
               </tr>
               <tr>
                   <td>
                       Haben Sie Zeit und Interesse ? Dann bitten wir um Rückruf unter xxx oder
                       klicken Sie einfach auf den unten stehenden Link, um sich online für einen Dienst anzumelden.
                       Bitte klicken Sie auf den nachfolgenden Link, um sich für einen der Dienste anzumelden oder um abzulehnen.
                       Falls sie den Link nicht aufrufen können, kopieren Sie diesen bitte vollständig in die Adresszeile Ihres Browsers.
                   </td>
               </tr>
               <tr>
                   <td>
                       <a href="{{route('jobs.apply_form_for_doctors',$mission)}}?_token={{Crypt::encrypt($user->id)}}">{{__('Apply Now')}}</a>
                   </td>
               </tr>
           </table>
       </td>
       <td>

       </td>
   </tr>

   </tbody>
</table>

@endsection
