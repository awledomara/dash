<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">


    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>YoraUI Admin</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- base:css -->
    <link rel="stylesheet" href="{{asset('css/vertical-layout-light/style.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

    <style>
        *::-webkit-scrollbar-track
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            border-radius: 0px;
            background-color: #F5F5F5;
        }

        *::-webkit-scrollbar
        {
            width: 4px;
            background-color: #F5F5F5;
        }

        *::-webkit-scrollbar-thumb
        {
            border-radius: 4px;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #555;
        }
        .hoverable:hover{
            background-color: #fafafa;
        }
        select.form-control,.form-control,.tags-input-root,.autocomplete{
            border: 3px solid #eee;
            border-radius: 10px;
            outline: none;
        }

        textarea{
            resize: vertical;
        }
        .txt-8{
            font-size: 10px;
        }
        .tags-input-typeahead-item-default{
            display: none;
        }
        .tags-input-wrapper-default {

            font-weight: 400;
            font-size: 0.875rem;

            border: none;

        }
        .mx-input-append{
            display: none;
        }
        .btn{
            cursor: pointer;
        }
        .autocomplete__box {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            background: transparent;
            border: 0px solid #ccc;
            border-radius: 10px;
            padding: 0 5px;
        }
        .card-header{
            background: transparent;
        }
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }
    </style>
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->

@yield('head')

<!-- endinject
    <link rel="shortcut icon" href="../../../../images/favicon.png" />
   -->
</head>

<body class="sidebar-dark ">
<div class="container-scroller" id="app">
    <!-- partial:../../partials/_navbar.html -->
@include('partials.top_navbar')
<!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_settings-panel.html -->
        <div class="theme-setting-wrapper">
            <div id="settings-trigger"><i class="mdi mdi-settings"></i></div>
            <div id="theme-settings" class="settings-panel">
                <i class="settings-close mdi mdi-close"></i>
                <p class="settings-heading">SIDEBAR SKINS</p>
                <div class="sidebar-bg-options" id="sidebar-light-theme"><div class="img-ss rounded-circle bg-light border mr-3"></div>Light</div>
                <div class="sidebar-bg-options selected" id="sidebar-dark-theme"><div class="img-ss rounded-circle bg-dark border mr-3"></div>Dark</div>
                <p class="settings-heading mt-2">HEADER SKINS</p>
                <div class="color-tiles mx-0 px-4">
                    <div class="tiles primary"></div>
                    <div class="tiles secondary"></div>
                    <div class="tiles dark"></div>
                    <div class="tiles default"></div>
                </div>
            </div>
        </div>
    @guest
    @else
        @include('partials.right_sidebar')
    @endguest


    <!-- partial -->
        <!-- partial:../../partials/_sidebar.html -->


    @include('partials.sidebar')

    <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">

                @if(session('message'))
                    <p class="alert alert-info border-0">
                        <span><i class="mdi mdi-information"></i></span>&nbsp;{{session('message')}}
                    </p>

                @endif
                @yield('content')
            </div>
            <!-- content-wrapper ends -->
            <!-- partial:../../partials/_footer.html -->
        @include('partials.footer')
        <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- base:js -->
{{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>--}}
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="{{ asset('js/app.js?v=2') }}" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js" > </script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->

<script src="{{ asset('js/off-canvas.js') }}" defer></script>
<script src="{{ asset('js/hoverable-collapse.js') }}" defer></script>
<script src="{{ asset('js/template.js?v=2') }}" defer></script>
<script src="{{ asset('js/settings.js') }}" defer></script>
<script src="{{ asset('js/todolist.js') }}" defer></script>

@yield('js')
//address book
<script type="application/javascript" defer>

    let _loadAddressBook;
    function showEditForm(address_id){
        $("[id^='a-']").hide()
        $("[class^='a-']").fadeIn()
        $("[id^='a-save']").hide()
        $("#a-"+address_id).fadeIn()
        $("#a-save-"+address_id).fadeIn()
        $(".a-"+address_id).hide()
        console.log(address_id)

    }

    function deleteEntry(address_id) {
        $.get(
            '{{route("microservices.address_books.index")}}/'+address_id+'/delete').done(function () {

            _loadAddressBook()
            // window.location.reload();
        })
    }

    function update_address_book(address_id) {

        $.post(
            '{{route("microservices.address_books.index")}}/'+address_id, {
                content:$("#a-"+address_id).val(),
                _token:$('_token').val()
            }).done(function () {

            $(".a-"+address_id).fadeIn()
            $(".a-"+address_id).html($("#a-"+address_id).val())
            $("#a-save-"+address_id).hide()
            $("#a-"+address_id).hide()
        })
    }

    function store_address_book() {
        let content= $('#address-content').val()

        $.post('{{route("microservices.address_books.index")}}',{
            content:content,
            _token:$('_token').val()
        }).done(function () {
            // loadAddressBook()
            _loadAddressBook()
        })
    }
    (function($) {
        'use strict';



        let list_addresses={}
        let query=''


        let append_list=function(){


            let dom_list_group=''
            if(list_addresses.hasOwnProperty('data')){
                if(list_addresses.data.length>0){

                    list_addresses.data.forEach(address=>{
                        dom_list_group+=
                            `<div class=" d-flex justify-content-between hoverable" style="padding: 8px;">

                                <span class="a-${address.id}">${address.content}</span>
                                 <textarea id="a-${address.id}" style="display: none" class="form-control"  rows="5" >${address.content}</textarea>
                                    <div>
                                     <span class="mdi mdi-delete text-facebook float-right"onclick="deleteEntry('${address.id}')" style="cursor: pointer"></span>
                                     <span class="mdi mdi-pencil text-facebook float-right"onclick="showEditForm('${address.id}')" style="cursor: pointer"></span>
                                     <span class="mdi mdi-content-save-all  float-right text-google " id="a-save-${address.id}"  onclick="update_address_book('${address.id}')" style="cursor: pointer;display: none"></span>
                                    </div>

                                </div>
                                    <div class="dashed-divider"></div>
`
                    })
                }else{
                    $("#address-list-group").html('<p class="alert alert-warning text-center">No data available</p>')
                }
            }


            $("#address-list-group").html(dom_list_group)
            return dom_list_group
        }
        let currentRequest=null;
        let loadAddressBook=function(){
            if(currentRequest){
                currentRequest.abort()
            }
            currentRequest=$.ajax({
                url: `{{route("microservices.address_books.index")}}?q=${$("#q-a").val()}`,
                success: function (_data) {

                    list_addresses = _data;

                    append_list()

                }
            })
        }
        _loadAddressBook = loadAddressBook;





        $(function() {
            //Page Add Service Area
            loadAddressBook()
            $(".btn-address-book").on("click", function() {
                $("#right-sidebar").toggleClass("open");
            });
            $("#btn-show-add-form").click(function () {
                $('.list-address').hide()
                $('.form-new-address').fadeIn()
            })
            $("#btn-save-address").click(function () {
                store_address_book()
                $('.form-new-address').hide()
                $('.list-address').fadeIn()

            })
            $("#btn-cancel").click(function () {

                $('.form-new-address').hide()
                $('.list-address').fadeIn()

            })
            $('.form-group').on('keyup',"#q-a",function () {
                loadAddressBook();
            })




        })


    })(jQuery);


</script>

<!-- endinject -->
<!-- plugin js for this page -->


<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<!-- End custom js for this page-->
</body>

</html>
