@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-header">
            {{__('Sent Bulk Emails')}}
        </div>
        <div class="card-body table-responsive">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>
                        {{__("Date")}}
                    </th>
                    <th>
                        {{__("Form")}}
                    </th>
                    <th>
                        {{__("Template")}}
                    </th>
                    <th>
                        {{__("Subject")}}
                    </th>
                    <th>
                        {{__("Recipients")}}
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($emails as $email)
                    <tr>
                        <td>
                            {{$email->created_at->format('d.m.Y H:m')}}
                        </td>
                        <td>
                            {{$email->form}}
                        </td>
                        <td>
                            {{$email->template->name}}
                        </td>
                        <td>
                            {{$email->subject}}
                        </td>
                        <td>
                            {{$email->recipients}}
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="6" class="text-center">
                        {{$emails->links()}}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('js')

@endsection