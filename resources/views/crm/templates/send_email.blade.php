@extends('layouts.dashboard')
@section('head')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{__('Send Email')}}
                    </div>

                    <div class="card-body">
                        <a class="btn btn-default float-right" href="{{route('templates.create')}}">{{__('Create new template')}}</a>
                        <form action="{{route('email.send')}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="">{{__('Existing Template')}}</label>
                                        <select name="template_id" id="template_id" class="form-control">

                                        </select>
                                    </div>

                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="template_subject">{{__('Template Subject')}}</label>
                                        <input name="subject" id="template_subject" class="form-control" value="{{old('subject')}}">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">{{__('Text - Use ##name## and ##email## as placeholder')}}</label>
                                        <textarea name="content" id="template_content" cols="30" rows="10" class="form-control">{!! old('content') !!}</textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">{{__('Select recipient(s) - Vertreter')}}</label>
                                        <select id="recipient_id" name="recipients[]" class="form-control">

                                        </select>
                                    </div>


                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-github float-right">{{__('Send Email(s)')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){

            tinymce.init({
                selector: '#template_content',
                menubar: 'file edit view insert format tools table tc help',
                plugins: 'code print preview fullpage importcss searchreplace autolink autosave save directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
                toolbar: 'code | undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | numlist bullist checklist | fontselect fontsizeselect formatselect | outdent indent | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | image link anchor codesample | a11ycheck ltr rtl | showcomments addcomment', // insertfile media pageembed template
                height: 400,
                width:1000,
                image_title: true,

                images_upload_url: 'postAcceptor.php',
                images_upload_handler: function (blobInfo, success, failure) {
                    let file = {
                        file_name: blobInfo.filename(),
                        file_type: blobInfo.filename().split('.').pop(),
                        value: blobInfo.base64(),
                    };
                    $.post('{{route('microservices.fileupload')}}', file)
                        .done(function (res) {
                            console.log("res:", res);
                            success(res);

                        });


                },
            });


            function appendFiles() {
                var input = document.getElementById('filesToUpload');
                var list = document.getElementById('fileList');


                while (list.hasChildNodes()) {
                    list.removeChild(list.firstChild);
                }

                //for every file...

                for (var x = 0; x < input.files.length; x++) {
                    //add to list
                    var li = document.createElement('li');
                    li.innerHTML = 'File ' + (x + 1) + ':  ' + input.files[x].name;
                    list.append(li);
                }
            }
            $('#filesToUpload').change(function(){
                appendFiles()
            })


            //Select 2

            $('#template_id').select2({
                theme: "classic",
                allowClear: true,
                placeholder: "{{__('Existing Template')}}",
                ajax: {
                    url: '{{route('microservices.email_templates.search')}}',
                    data: function (params) {
                        var query = {
                            q: params.term,

                        }

                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });
            $('#recipient_id').select2({
                theme: "classic",
                multiple: true,
                allowClear: true,

                ajax: {
                    url: '{{route('microservices.recipients.search')}}',
                    data: function (params) {
                        var query = {
                            q: params.term,

                        }

                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.first_name + ' ' + item.last_name + ' - ' + item.email,
                                    id: item.id
                                }
                            })
                        };
                    },
                    dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });
            $('#template_id').on('select2:select', function (e) {

                $.get('/api/templates/'+$('#template_id').val(),function (_data) {

                    $('#template_name').val(_data.name)
                    $('#template_subject').val(_data.subject)
                    $('#template_content').val(_data.content)

                    tinymce.activeEditor.setContent((_data.content));

                })
            });


        })
    </script>
@endsection