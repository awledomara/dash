@extends('layouts.dashboard')
@section('head')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{__('Templates')}}
                        <a class="btn btn-default btn-github float-right" href="{{route('templates.create')}}">{{__('Create new template')}}</a>


                    </div>

                    <div class="card-body">

                            @forelse ($templates as $template)
                            <div class="border-bottom py-2 is-hoverable" >

                                <div class="ml-3">
                                    <span class="float-right"><i class="mdi mdi-trash-can blue-gradient-button"></i></span>
                                    <h6 class="mb-1"><a href="{{route("templates.edit",$template)}}">{{$template->name}}</a></h6>
                                    <p class="text-muted mb-1"> {{$template->subject}}</p>
                                    <p class="text-justify">{{$template->content}}</p>

                                </div>

                            </div>


                            @empty
                              <p class="alert alert-warning">{{__('No data')}}                              </p>
                            @endforelse

                        <br>
                        {{$templates->links()}}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){

            function appendFiles() {
                var input = document.getElementById('filesToUpload');
                var list = document.getElementById('fileList');


                while (list.hasChildNodes()) {
                    list.removeChild(list.firstChild);
                }

                //for every file...

                for (var x = 0; x < input.files.length; x++) {
                    //add to list
                    var li = document.createElement('li');
                    li.innerHTML = 'File ' + (x + 1) + ':  ' + input.files[x].name;
                    list.append(li);
                }
            }
            $('#filesToUpload').change(function(){
                appendFiles()
            })


            //Select 2

            $('#template_id').select2({
                theme: "classic",
                allowClear: true,
                placeholder: "{{__('Existing Template')}}",
                ajax: {
                    url: '{{route('microservices.email_templates.search')}}',
                    data: function (params) {
                        var query = {
                            q: params.term,

                        }

                        // Query parameters will be ?search=[term]&type=public
                        return query;
                    },
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });
            $('#template_id').on('select2:select', function (e) {

                $.get('/api/templates/'+$('#template_id').val(),function (_data) {

                    $('#template_name').html(_data.name)
                    $('#template_subject').html(_data.subject)
                    $('#template_content').html(_data.content)

                })
            });

        })
    </script>
@endsection