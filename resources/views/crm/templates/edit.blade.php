@extends('layouts.dashboard')
@section('head')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-12">
                @if($errors)
                    {{$errors->first()}}
                @endif
                <div class="card">
                    <div class="card-header" style="padding: 10px">
                            {{__('Templates')}}
                        <a class="btn btn-default float-right btn-sm" href="{{route('templates.create')}}">{{__('Create new template')}}</a>
                    </div>

                    <div class="card-body">

                        <form action="{{route('templates.update')}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-8">


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">{{__('Existing Template')}}</label>
                                                <select name="template_id" id="template_id" class="form-control">
                                                    @if($template)
                                                        <option value="{{$template->id}}" selected >{{$template->name}}</option>
                                                    @endif
                                                </select>
                                            </div>

                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="template_name">{{__('Template Name')}}</label>
                                                <input name="name" id="template_name" class="form-control" value="{{$template->name}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="template_subject">{{__('Template Subject')}}</label>
                                                <input name="subject" id="template_subject" class="form-control" value="{{$template->subject}}">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">{{__('Text - Use ##name## and ##email## as placeholder')}}</label>
                                                <textarea name="content" id="template_content" cols="30" rows="10" class="form-control">{!! $template->content!!}</textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-4">

                                        <div class="card">
                                            <div class="card-header" style="padding: 10px">
                                                {{__('Upload more files')}}
                                            </div>
                                            <div class="card-body">
                                                <div class="form-group">

                                                    <input type="file" name="files[]" multiple="multiple" id="filesToUpload">
                                                </div>
                                                <ul id="fileList">

                                                </ul>
                                            </div>
                                        </div>
                                    <br>
                                        <div class="card">
                                            <div class="card-header" style="padding: 10px">
                                                {{__('Existing files')}}
                                            </div>
                                            <div class="card-body">

                                                <ul id="fileList_db">
                                                    @foreach($files as $file)
                                                    <li>{{($file->original_name)}}
                                                        <a style="color:red" href="{{route('templates.files.delete',[$template->id,$file->id])}}">Delete</a>
                                                    </li>

                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>


                                </div>
                            </div>



                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-github float-right">{{__('Save Changes at Template')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')

    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
        <script>
            $(document).ready(function(){




                tinymce.init({
                    selector: '#template_content',
                    menubar: 'file edit view insert format tools table tc help',
                    plugins: 'code print preview fullpage importcss searchreplace autolink autosave save directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
                    toolbar: 'code | undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | numlist bullist checklist | fontselect fontsizeselect formatselect | outdent indent | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | image link anchor codesample | a11ycheck ltr rtl | showcomments addcomment', // insertfile media pageembed template
                    height: 400,
                    image_title: true,

                    images_upload_url: 'postAcceptor.php',
                    images_upload_handler: function (blobInfo, success, failure) {
                        let file = {
                            file_name: blobInfo.filename(),
                            file_type: blobInfo.filename().split('.').pop(),
                            value: blobInfo.base64(),
                        };
                        $.post('{{route('microservices.fileupload')}}', file)
                            .done(function (res) {
                                console.log("res:", res);
                                success(res);

                            });


                    },
                });

                  function appendFiles() {
                    var input = document.getElementById('filesToUpload');
                    var list = document.getElementById('fileList');


                    while (list.hasChildNodes()) {
                        list.removeChild(list.firstChild);
                    }

                    //for every file...

                    for (var x = 0; x < input.files.length; x++) {
                        //add to list
                        var li = document.createElement('li');
                        li.innerHTML = 'File ' + (x + 1) + ':  ' + input.files[x].name;
                        list.append(li);
                    }
                }
                  $('#filesToUpload').change(function(){
                        appendFiles()
                    })


                //Select 2

                $('#template_id').select2({
                    theme: "classic",
                    allowClear: true,
                    placeholder: "{{__('Existing Template')}}",
                    ajax: {
                        url: '{{route('microservices.email_templates.search')}}',
                        data: function (params) {
                            var query = {
                                q: params.term,

                            }

                            // Query parameters will be ?search=[term]&type=public
                            return query;
                        },
                        processResults: function (data) {
                            // Transforms the top-level key of the response object from 'items' to 'results'
                            return {
                                results:  $.map(data, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id
                                    }
                                })
                            };
                        },
                        dataType: 'json'
                        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                    }
                });
                $('#template_id').on('select2:select', function (e) {

                   $.get('/api/templates/'+$('#template_id').val(),function (_data) {

                       $('#template_name').val(_data.name)
                       $('#template_subject').val(_data.subject)
                       $('#template_content').val(_data.content)

                       console.log(_data.content)
                       console.log($.parseHTML(_data.content))
                       tinymce.activeEditor.setContent((_data.content));

                   })
                });

            })
        </script>
@endsection