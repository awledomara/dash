@extends('layouts.dashboard')
@section('head')
    <style>
        .red {
            color: red;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{__('Create New Template')}}
                    </div>

                    <div class="card-body">

                        <form action="{{route('templates.store')}}" id="form" method="POST"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="">{{__('Template Name')}}</label>
                                        <input type="text" class="form-control" name="name">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">

                                    <div class="form-group">
                                        <label for="">{{__('Template Subject')}}</label>
                                        <input type="text" class="form-control" name="subject">
                                    </div>

                                    <div class="form-group">
                                        <label for="">{{__('Text - Use ##name## and ##email## as placeholder')}}</label>
                                        <textarea name="content" id="template_content" cols="30" rows="10"
                                                  class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="filesToUpload">{{__('Upload more files')}}</label><br>
                                        <input type="file" name="files[]" multiple="multiple" id="filesToUpload">
                                    </div>
                                    <ul id="fileList">

                                    </ul>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-github float-right">{{__('Save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        let fileList = [];


        $(document).ready(function () {

            tinymce.init({
                selector: '#template_content',
                menubar: 'file edit view insert format tools table tc help',
                plugins: 'code print preview fullpage importcss searchreplace autolink autosave save directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
                toolbar: 'code | undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | numlist bullist checklist | fontselect fontsizeselect formatselect | outdent indent | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | image link anchor codesample | a11ycheck ltr rtl | showcomments addcomment', // insertfile media pageembed template
                height: 400,
                image_title: true,

                images_upload_url: 'postAcceptor.php',
                images_upload_handler: function (blobInfo, success, failure) {
                    let file = {
                        file_name: blobInfo.filename(),
                        file_type: blobInfo.filename().split('.').pop(),
                        value: blobInfo.base64(),
                    };
                    $.post('{{route('microservices.fileupload')}}', file)
                        .done(function (res) {
                            console.log("res:", res);
                            success(res);

                        });


                },
            })
            ;

            function appendFiles() {
                let input = document.getElementById('filesToUpload');
                let list = document.getElementById('fileList');

                while (list.hasChildNodes()) {
                    list.removeChild(list.firstChild);
                }
                fileList = [];
                for (var x = 0; x < input.files.length; x++) {
                    fileList.push(input.files[x]);
                }

                renderFiles();
            }

            $('#filesToUpload').change(function () {
                appendFiles()
            })


        })
        ;

        function renderFiles() {
            let list = document.getElementById('fileList');
            list.innerHTML = '';

            fileList.forEach(function (file, index) {

                //add to list
                let li = document.createElement('li');
                li.innerHTML = 'File ' + (index + 1) + ':  ' + file.name;
                list.append(li);
            })
        }

    </script>
@endsection