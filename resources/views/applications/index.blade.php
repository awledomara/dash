@extends('layouts.dashboard')
@section('head')
    <style>
        tr.active {
            background-color: rgba(255, 165, 0, 0.38) !important;
            color: #000;
        }

        tr.active:hover {
            color: #000 !important;
        }

        .overlay {
            top: 0px;
            left: 0px;
            background: rgba(0, 0, 0, 0.37);
            height: 100%;
            position: absolute;
            height: 100%;
            width: 100%;
        }

        .accept-btn {
            cursor: pointer;
        }
    </style>

@endsection
@section('content')

    <div class="row">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body" style="overflow-y: auto;min-height: 70vh">
                    <div class="card-title">
                        {{__('List of Jobs')}}
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                        <th>{{__('Start Date')}}</th>
                        <th>{{__('End Date')}}</th>
                        <th>{{__('Total Hours')}}</th>

                        </thead>
                        <tbody>
                        @forelse ($jobs as $index=>$job)
                            <tr class="job-row {{$index==0?'active':''}}" data-id="{{$job->id}}"
                                style="cursor: pointer;">
                                <td>
                                    {{$job->start_date->format('d.m.Y H:i')}}
                                </td>
                                <td>
                                    {{$job->end_date->format('d.m.Y H:i')}}
                                </td>
                                <td>
                                    {{$job->start_date->diffInHours($job->end_date)}} hrs
                                </td>

                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">
                                    <p class="alert alert-warning border-0">There is no application at this time</p>
                                </td>
                            </tr>

                        @endforelse

                        </tbody>
                    </table>
                </div>
                <div class="card-footr text-center d-flex justify-content-between">
                    <div class="row">
                        <div class="col grid-margin">
                            <div class="float-right d-flex ">
                                {{$jobs->links()}}
                            </div>

                        </div>
                    </div>

                </div>
            </div>


        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <div class="input-group mb-0 ">

                        <input type="text" class="form-control form-control-sm" id="q-doctor"
                               placeholder="{{__('Search for doctors')}}">
                    </div>
                </div>

                <div class="card-body" style="overflow-y: auto; max-height: 70vh">
                    <div class="overlay" style=""></div>
                    <div class="job-application-details" style="display:block;">

                    </div>
                </div>
            </div>

        </div>
    </div>



@endsection
@section('js')
    <script>
        $(document).ready(function () {
            let job_id = $('.job-row:first').data('id')


            function loadDeatails(job_id) {

                $.get(`/api/ajax_jobs/${job_id}/applications?doctor_name=${$('#q-doctor').val()}`).done(function (view) {
                    $('.job-application-details').html(view)
                    $('.overlay').fadeOut("slow")
                })
            }

            loadDeatails(job_id)

            $('.card-body').on("click", ".job-row", function () {
                $('.overlay').show()
                job_id = $(this).data('id');
                $('#q-doctor').val('')

                $('.card-body tr.active').removeClass('active');
                $(this).addClass('active')
                loadDeatails(job_id)

            })

            $('.input-group').on('keyup', "#q-doctor", function () {
                loadDeatails(job_id)
            })
        })

        function accept(id) {

            $.get(`/api/applications/${id}/accept`).done(function (data) {
                console.log("data:", data);
                if (data === 'ok')
                    window.location.reload();
            })
        }
    </script>
@endsection
