
@foreach($jobApplications as $application)
    <div class=" d-flex align-items-center  border-bottom mb-2" style="">
        <img class="img-xs rounded-circle" src="{{asset('images/avatars/no-avatar.png')}}" alt="profile">
        <div class="ml-3">
            <h6 class="mb-1  text-capitalize">
                {{$application->doctor->user->first_name}} {{$application->doctor->user->last_name}}
            </h6>
            <p class="text-muted mb-0 tx-12"><i class="mdi mdi-mailbox mr-1"></i>
                {{$application->doctor->user->email}}
                <i class="text-dribbble float-right">{{__('Applied at')}} {{$application->created_at->format('d.m.Y H:i')}}</i>
            </p>
        </div>
        <i class="mdi mdi-check-circle-outline font-weight-bold ml-auto px-1 py-1 text-facebook mdi-24px accept-btn" onclick="accept({{$application->id}})" title="{{__('Accept Application?')}}"></i>
    </div>

@endforeach







