@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-body table-responsive">
            <a class="btn btn-github float-right" href="{{route('hotels.create')}}">{{__('Add New Hotel')}}</a>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>
                        {{__("Hotel Name")}}
                    </th>
                    <th>
                        {{__("Email")}}
                    </th>
                    <th>
                        {{__("Phones")}}
                    </th>
                    <th>
                        {{__("Address")}}
                    </th>

                    <th>
                        {{__("Created At")}}
                    </th>
                    <th>
                        {{__('Actions')}}
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($hotels as $hotel)
                    <tr>
                        <td>
                            {{$hotel->name}}
                        </td>
                        <td>
                            {{$hotel->email}}
                        </td>
                        <td>
                            <i class="mdi mdi-phone"></i>&nbsp;{{$hotel->primary_phone}}<br>
                            <i class="mdi mdi-phone"></i>&nbsp;{{$hotel->primary_phone}}<br>
                            <i class="mdi mdi-phone-classic"></i>&nbsp;{{$hotel->fax}}<br>

                        </td>
                        <td>
                            @if($hotel->addresses->first())
                                {{$hotel->addresses->first()->address->street}} ,{{$hotel->addresses->first()->address->city}} {{$hotel->addresses->first()->address->postal_code}}<br> {{$hotel->addresses->first()->address->country}}

                                <div class="dropdown" id="dropdown-{{$hotel->id}}">
                                    <a class="myDropdownHandle" data-toggle="dropdown" data-target="#" href="#">
                                        See more <span  class="caret"></span>
                                    </a>
                                    <div class="dropdown-menu " style="padding: 15px">


                                          @foreach($hotel->addresses as $index=> $add)
                                            <div class="card" style="margin: 2px">
                                                <div class="card-body">
                                                    <i class="mdi mdi-map"></i>&nbsp; {{$add->address->street}} <br> {{$add->address->postal_code}} {{$add->address->city}}<br> {{$add->address->country}}<br>
                                                </div>
                                            </div>



                                          @endforeach

                                    </div>
                                </div>

                            @endif
                        </td>

                        <td>
                            {{$hotel->created_at->format('d M Y h:m:s')}}
                        </td>
                        <td>
                            <a href="{{route('hotels.edit',['id'=>$hotel->id])}}" >
                                Edit
                            </a>

                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="6" class="text-center">
                        {{$hotels->links()}}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('js')

@endsection