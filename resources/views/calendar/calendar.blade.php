@extends('layouts.dashboard')
@section('head')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css"/>

    <link rel="stylesheet" href="{{asset("css/calendar/calendar.css")}}">
    <link rel="stylesheet" href="{{asset("context-menu/contextMenu.min.css")}}">
@endsection
@section('content')

    <div class="container calendar-main-container">
        <div class="customer-details">
            <div class="customer-info-section">
                FD/SD Zeit: 20.3. 19:00 – 21.3. 7:00 (xx h) DG: xxxxxxxxxxxxxxxxxxxxxx Kunde: xxxxxxxxxxxxxxxxxxxx <br>
                Honorar: xxxx €
                Vertreter:<br>
                Notes:<br>
            </div>
            <div class="buttons-container">
                <div>
                    <button class="btn btn-secondary">Send verification</button>
                    <button class="btn btn-primary">Verify Dienst</button>
                </div>
                <div>
                    <button class="btn btn-secondary" onclick="openModal_2()">Apply Dienst</button>
                    <button class="btn btn-primary">Dienst melden</button>
                </div>
            </div>
        </div>
        <div class="calendar">
            <div class="month-select-container">
                <div></div>
                <div class="month"></div>
                <div class="select-view-container">
                    <form action="" id="select-view-form">
                        <label for="select-view">View:</label>
                        <select name="ex_w" id="select-view" onchange="calendarViewChanged()" class="form-control-sm">
                            <option value="0">1 Week</option>
                            <option value="1">2 Weeks</option>
                            <option value="2">3 Weeks</option>
                            <option value="3">4 Weeks</option>
                        </select>
                    </form>
                </div>
            </div>
            <div class="calendar-container">
                <div class="arrow arrow-left" onclick="prevWeek()"></div>
                <div class="days-holder">
                    Loading...
                </div>
                <div class="arrow arrow-right" onclick="nextWeek()"></div>
            </div>
        </div>
    </div>
    <div id="modal_1" class="modal">
        <p id="modal-notes"></p>
        <a href="" id="link-a-popup">Link A</a>
        <br>
        <a href="" id="link-b-popup">Link B</a>
    </div>
    <div id="modal_2" class="modal">
        <p id="modal-2-notes">Hier stehen die Bewerber</p>
    </div>
@endsection
@section('js')

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <script src="{{asset("context-menu/contextMenu.min.js")}}"></script>

    <script type="text/javascript">
        let menu = [{
            name: 'Down to Bottom',
            fun: menuBtnDownToBottom
        }, {
            name: 'Down one Level',
            fun: menuBtnDownOneLevel
        }, {
            name: 'Duplicate Selected Element',
            fun: menuBtnDuplicateMission
        }, {
            name: 'Delete Element',
            fun: menuBtnDeleteMission
        }];

        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        const days = ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU'];
        const weekDayFixer = [6, 0, 1, 2, 3, 4, 5];
        const aDaySecs = 86400;
        const aDayMins = 1020; // from 7 in the morning
        const jobLevels = 15;
        const curDay = new Date();
        let curDaySec;
        let fromTime, fromDate;
        let toTime, toDate;
        let data;
        let urlParams = new URLSearchParams(window.location.search);
        let extendedWeeks = (urlParams.has('ex_w') ? parseInt(urlParams.get('ex_w')) : 0);
        const extendedWeeksSecs = (extendedWeeks * (7 * aDaySecs));

        const hoursElement = `<span class="hours"><span>7</span><span></span><span>14</span><span></span><span>19</span><span>0</span></span>`;

        $(document).ready(function () {
            $('#select-view').val(extendedWeeks);
        });

        $(document).ready(function () {
            window.addEventListener("resize", printCalendar);
            // $('.days-holder').on('mouseover', calendarMouseOver);//.on('mouseout', calendarMouseOut);
            document.getElementsByClassName('days-holder').item(0).onmouseover = calendarMouseOver;

            let date = new Date();
            curDaySec = Math.round(date.getTime() / 1000);

            fromTime = curDaySec - ((weekDayFixer[date.getDay()]) * aDaySecs); //getDay() - 1
            toTime = fromTime + (6 * aDaySecs) + extendedWeeksSecs;

            // console.log('curDaySec', curDaySec);
            // let t = new Date();
            // t.setTime(fromTime * 1000);
            // console.log('--------from', t);
            // t.setTime(toTime * 1000);
            // console.log('--------to', t);

            getDataAndPrint();
        });

        function nextWeek() {
            const tFrom = fromTime;
            const tTo = fromTime + (6 * aDaySecs);
            // const tTo = toTime;

            fromTime = tTo + aDaySecs;
            toTime = fromTime + (6 * aDaySecs) + extendedWeeksSecs;

            getDataAndPrint();
        }

        function prevWeek() {
            const tFrom = fromTime;
            const tTo = fromTime + (6 * aDaySecs);
            // const tTo = toTime;

            fromTime = tFrom - (7 * aDaySecs);
            toTime = fromTime + (6 * aDaySecs) + extendedWeeksSecs;

            getDataAndPrint();
        }

        function getDataAndPrint() {
            $('.days-holder').text('Loading...');

            let fromTimeObj = new Date();
            fromTimeObj.setTime(fromTime * 1000);
            console.log("fromTimeObj:", fromTimeObj);

            $.ajax({
                url: `/api/calendar/?from=${fromTime}&to=${toTime}`,
                success: function (_data) {
                    // console.log('res', _data);
                    data = _data;

                    printCalendar();
                }
            })
        }

        function printCalendar() {
            let daysHolder = $('.days-holder');

            daysHolder.html($());

            fromDate = new Date();
            fromDate.setTime(fromTime * 1000);
            toDate = new Date();
            toDate.setTime(toTime * 1000);

            let monthName = monthNames[fromDate.getMonth()] + " (" + fromDate.getFullYear() + ")";
            if (fromDate.getMonth() != toDate.getMonth())
                monthName = monthName + " - " + monthNames[toDate.getMonth()] + " (" + toDate.getFullYear() + ")";
            $(".month").text(monthName);

            for (let week = 0; week <= extendedWeeks; week++) {
                for (let i in days) {
                    const ii = parseInt(i);
                    const day = new Date();
                    day.setTime(fromDate.getTime() + ((ii + (7 * week)) * (aDaySecs * 1000)));
                    let curDayClass = "", weekendClass = "";

                    if (day.getFullYear() == curDay.getFullYear() &&
                        day.getMonth() == curDay.getMonth() &&
                        day.getDate() == curDay.getDate())
                        curDayClass = "current-day";

                    if (days[i] === 'SA' || days[i] === 'SU')
                        weekendClass = 'weekend-day';

                    let $day = $(`<div class="day ${curDayClass} ${weekendClass}" day-num="${day.getMonth() + 1}_${day.getDate()}"></div>`);

                    $day.append($(`<div class="day-heading">
                <span class="day-name">${days[i]}</span>
                <span class="day-num" id="day-num-${ii * (7 * week)}">${day.getDate()}</span>
                ${hoursElement}</div>`));

                    let $missions = $('<div class="missions-placeholder"></div>');
                    for (let j = 0; j < jobLevels; j++) {
                        let missionPlaceHolder = $(`<div class="mission-placeholder" mission-index="${j}"></div>`);

                        missionPlaceHolder.attr('day-num', `${day.getMonth() + 1}_${day.getDate()}`);
                        missionPlaceHolder.attr('ondrop', "drop(event)");
                        missionPlaceHolder.attr('ondragover', "allowDrop(event)");
                        // missionPlaceHolder.attr('ondragleave', "ondragexit(event)");
                        $missions.append(missionPlaceHolder)
                    }
                    $day.append($missions);
                    daysHolder.append($day);
                }
            }
            printMissions();
        }

        function printMissions() { // todo check for the order Overlap in one-day - and especially on more than one day missions
            for (let mission of data) {
                let startDate = new Date();
                startDate.setTime(Date.parse(mission.start_date));
                let endDate = new Date();
                endDate.setTime(Date.parse(mission.end_date));

                let missionTextElement = $('<span class="mission-text"></span>');
                let missionBlackElement = $('<span class="mission-black-area"></span>');
                let missionElement = $('<span class="mission"></span>');
                missionElement.attr('id', 'mission-id-' + mission.id);
                missionElement.attr('mission-id', mission.id);
                missionElement.attr('draggable', "true");
                missionElement.attr('ondragstart', "drag(event)");
                // missionElement.attr('day-num', startDate.getDate());
                missionElement.attr('day-num', `${startDate.getMonth() + 1}_${startDate.getDate()}`);
                missionElement.attr('title', mission.notes);
                missionElement.attr('start-time', startDate.getTime() / 1000);
                missionElement.attr('end-time', endDate.getTime() / 1000);
                missionElement.attr('notes', mission.notes);
                missionElement.attr('cur-order', mission.order);
                missionTextElement.on('click', openModal);


                let tStr = startDate.getDate() + "." + (startDate.getMonth() + 1) + ". " + startDate.getHours() + ":" + startDate.getMinutes().toString().padStart(2, '0');
                tStr += " – " + endDate.getDate() + "." + (endDate.getMonth() + 1) + ". " + endDate.getHours() + ":" + endDate.getMinutes().toString().padStart(2, '0');
                missionBlackElement.attr('title', 'Meyer, ' + tStr + '\nVertreter\n');

                missionTextElement.text(mission.practice.first_name + " " + mission.practice.last_name);
                missionElement.append(missionBlackElement);
                missionElement.append(missionTextElement);

                let startIsInScope = true;
                let endIsInScope = true;


                // check for Start
                let startMissionPlace = $(`.day[day-num=${startDate.getMonth() + 1}_${startDate.getDate()}] .mission-placeholder[mission-index=${mission.order}]`);
                // console.log("startMissionPlace:", startMissionPlace);
                if (startMissionPlace.length === 0) {
                    startMissionPlace = $(`.day[day-num=${fromDate.getMonth() + 1}_${fromDate.getDate()}] .mission-placeholder[mission-index=${mission.order}]`);
                    startIsInScope = false;
                }
                startMissionPlace.append(missionElement);

                // check for End
                let endMissionPlace = $(`.day[day-num=${endDate.getMonth() + 1}_${endDate.getDate()}] .mission-placeholder[mission-index=${mission.order}]`);
                if (endMissionPlace.length === 0) {
                    endMissionPlace = $(`.day[day-num=${toDate.getMonth() + 1}_${toDate.getDate()}] .mission-placeholder[mission-index=${mission.order}]`);
                    endIsInScope = false;
                }

                //-------------------------------------

                const startLeft = calcLeft(startDate, startIsInScope, startMissionPlace);
                let width = calcLeft(endDate, endIsInScope, endMissionPlace);

                missionElement.css({'left': startLeft + "px"});

                const fillStart = startMissionPlace.width() - startLeft;

                if (endIsInScope)
                    width += endMissionPlace.position().left - startMissionPlace.position().left - startMissionPlace.width() + fillStart;
                else if (startIsInScope && !endIsInScope)
                    width += endMissionPlace.position().left - startMissionPlace.position().left - startLeft + startMissionPlace.width();
                // width = ($($('.mission')[1]).parent().position().left + $($('.mission')[1]).position().left) - missionElement.position().left;

                missionElement.css({'width': width + "px"});
            }

            $('.missions-placeholder').on('mouseover', calendarMouseOver);
            $('.day').on('mouseover', calendarMouseOver);

            $('.mission-black-area').contextMenu(menu);

        }

        function calcLeft(date, isInScope, missionPlace) {
            let mins = 0;
            if (isInScope) {
                let hours = date.getHours();
                if (hours <= 7)
                    hours = 0;
                else
                    hours -= 7;

                mins = (hours * 60) + date.getMinutes();
            }
            return (missionPlace.width() / aDayMins) * mins;
        }

        function allowDrop(ev) {
            ev.preventDefault();
            // $(ev.target).css('background-color','#b3b3b3');
        }

        // function ondragleave(ev) {
        //     $(ev.target).css('background-color','#ffffff');
        //
        // }

        function drag(ev) {
            ev.dataTransfer.setData("text", ev.target.id);
        }

        function drop(ev) {
            ev.preventDefault();
            const data = ev.dataTransfer.getData("text");
            let mission = $(document.getElementById(data));
            let target = $(ev.target);
            if (target.attr('day-num') !== mission.attr('day-num')) {
                // console.log("not in");
                return;
            }
            // target.append(mission); todo is it correct?

            updateMissionOrder(mission.attr('mission-id'), target.attr('mission-index'), mission.attr('start-time'), mission.attr('end-time'));

            // var data = window.ClipboardEvent.getData('text');
            // ev.target.appendChild(mission);
        }

        function updateMissionOrder(id, newOrder, startTime, endTime) {
            $.ajax({
                url: `/api/calendar/${id}?order=${newOrder}&start-time=${startTime}&end-time=${endTime}`,
                method: 'patch',
                success: function (_data) {
                    console.log('_data', _data);

                    getDataAndPrint()
                }
            })
        }

        function openModal(ev) {
            let target = $(ev.target);
            const id = target.attr('mission-id');
            $('#modal-notes').text(target.attr('notes'));
            $('#link-a-popup').attr('href', '/page-a/' + id);
            $('#link-b-popup').attr('href', '/page-b/' + id);

            $('#modal_1').modal();
        }

        function openModal_2(ev) {

            $('#modal_2').modal();
        }

        function calendarViewChanged($event) {
            $('#select-view-form').submit();
        }

        let line = $('<div id="calendar-indicator-line"></div>');

        function calendarMouseOver($event) {
            if ($('#calendar-indicator-line').length === 0) {
                $('.days-holder').append(line);
            }
            line.css('left', ($event.clientX - $('.days-holder').offset().left) + 'px');
        }

        function calendarMouseOut($event) {
            if (!checkParent($event.target, 'days-holder'))
                $('#calendar-indicator-line').hide();
        }

        function checkParent(element, className) {
            if ($(element.parentElement).hasClass(className))
                return true;
            else if (element.parentElement)
                return checkParent(element.parentElement);
            else
                return false;
        }

        // -----------------------------

        function menuBtnDownToBottom(ev) {
            const mission = $(ev.trigger[0].parentElement);
            updateMissionOrder(mission.attr('mission-id'), jobLevels - 1, mission.attr('start-time'), mission.attr('end-time'));

        }

        function menuBtnDownOneLevel(ev) {
            const mission = $(ev.trigger[0].parentElement);
            let level = mission.attr('cur-order');
            if (level < jobLevels - 1)
                level++;
            updateMissionOrder(mission.attr('mission-id'), level, mission.attr('start-time'), mission.attr('end-time'));
        }

        function menuBtnDuplicateMission(ev) {
            const mission = $(ev.trigger[0].parentElement);
            let level = mission.attr('cur-order');

            if (level < jobLevels - 1)
                level++;

            $.ajax({
                url: `/api/calendar/${mission.attr('mission-id')}?order=${level}`,
                method: 'post',
                success: function (_data) {
                    console.log('_data', _data);

                    getDataAndPrint()
                }
            })
        }

        function menuBtnDeleteMission(ev) {
            const mission = $(ev.trigger[0].parentElement);

            $.ajax({
                url: `/api/calendar/${mission.attr('mission-id')}`,
                method: 'delete',
                success: function (_data) {
                    console.log('_data', _data);

                    getDataAndPrint()
                }
            })
        }

    </script>
@endsection
