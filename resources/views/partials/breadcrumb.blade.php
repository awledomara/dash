

<nav aria-label="breadcrumb">
    <ol class="breadcrumb ">
        @foreach(explode('/',Request::path()) as $nav )
            <li class="breadcrumb-item">
                <a href="#" >{{$nav}}</a>
            </li>
        @endforeach
    </ol>
</nav>