<nav class="sidebar sidebar-offcanvas" id="sidebar">
    @guest

    @else
        @if(Auth::user()->hasAnyRole(['Admin','SuperAdmin']))
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="/dashboard">
                        <i class="mdi mdi-clipboard-text-outline menu-icon"></i>
                        <span class="menu-title">{{__('Dashboard')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('users.index')}}">
                        <i class="mdi mdi-airplay menu-icon"></i>
                        <span class="menu-title">{{__('Users')}}</span>
                    </a>
                </li>
                <!-- Practices nav-->
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#practices" aria-expanded="false" aria-controls="practices">
                        <i class="mdi mdi-clipboard-text-outline menu-icon"></i>
                        <span class="menu-title">{{__('Practices')}}</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="practices">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link" href="{{route('practices.create')}}">{{__('Add')}}</a></li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('practices.list')}}">{{__('Manage')}}</a></li>
                        </ul>
                    </div>
                </li>
                <!-- End Practices nav-->
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#crm" aria-expanded="false" aria-controls="crm">
                        <i class="mdi mdi-invert-colors menu-icon"></i>
                        <span class="menu-title">{{__('CRM')}}</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="crm">
                        <ul class="nav flex-column sub-menu">

                            <li class="nav-item"> <a class="nav-link" href="{{route('templates.index')}}">{{__('Message templates')}}</a></li>
                            <li class="nav-item"> <a class="nav-link"  href="{{route('email.show')}}">{{__('Send messages')}}</a></li>
                            <li class="nav-item"> <a class="nav-link"  href="{{route('service_email.show')}}">{{__('Dienste ausschreiben')}}</a></li>
                            <li class="nav-item"> <a class="nav-link"  href="{{route('sent-emails.index')}}">{{__('Sent Emails')}}</a></li>
                        </ul>
                    </div>
                </li>

                <!-- Doctors nav-->
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#doctors" aria-expanded="false" aria-controls="doctors">
                        <i class="mdi mdi-invert-colors menu-icon"></i>
                        <span class="menu-title">{{__('Doctors')}}</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="doctors">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link" href="{{route('doctors.create')}}">{{__('Add')}}</a></li>
                            <li class="nav-item"> <a class="nav-link"  href="{{route('doctors.index')}}">{{__('Manage')}}</a></li>

                        </ul>
                    </div>
                </li>
                <!-- End Doctors nav-->
                <li class="nav-item">
                    <a class="nav-link" href="{{route('calendar.view')}}">
                        <i class="mdi mdi-clipboard-text-outline menu-icon"></i>
                        <span class="menu-title">{{__('Job-Calendar')}}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#job-settings" aria-expanded="false" aria-controls="job-settings">
                        <i class="mdi mdi-grid-large menu-icon"></i>
                        <span class="menu-title">{{__('Job-Settings')}}</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="job-settings">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link" href="{{route('jobs.create')}}">{{__('Add')}}</a></li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('jobs.list')}}">{{__('Manage')}}</a></li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#jobs" aria-expanded="false" aria-controls="jobs">
                        <i class="mdi mdi-chart-donut menu-icon"></i>
                        <span class="menu-title">{{__('Job-Spreading')}}</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="jobs">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link" href="{{route('jobs.overview')}}">{{__('Overview')}}</a></li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('practices.jobs_view')}}">{{__('Practice-Verifications')}}</a></li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('practices.verification_new')}}">{{__('Practice-Verification_new')}}</a></li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('doctors.notifications.view')}}">{{__('Doctors-notifications')}}</a></li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('jobs.applications')}}">{{__('Applications')}}</a></li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('jobs.fulfillment')}}">{{__('Job-Fulfillment')}}</a></li>

                        </ul>
                    </div>
                </li>

                <!--Medical Service Areas-->
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#services" aria-expanded="false" aria-controls="services">
                        <i class="mdi mdi-invert-colors menu-icon"></i>
                        <span class="menu-title">{{__('Medical Service Area')}}</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="services">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link" href="{{route('services.create')}}">{{__('Add')}}</a></li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('services.list')}}">{{__('Manage')}}</a></li>

                        </ul>
                    </div>
                </li>
                <!--  Hotels nav-->
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#hotels" aria-expanded="false" aria-controls="hotels">
                        <i class="mdi mdi-flask-outline menu-icon"></i>
                        <span class="menu-title">{{__("Hotels")}}</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="hotels">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link" href="{{route('hotels.create')}}">{{__('Add')}}</a></li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('hotels.index')}}">{{__('Manage')}}</a></li>

                        </ul>
                    </div>
                </li>
                <!--  specialities nav-->
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#specialities" aria-expanded="false" aria-controls="specialities">
                        <i class="mdi mdi-flask-outline menu-icon"></i>
                        <span class="menu-title">{{__("Specialities")}}</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="specialities">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link" href="{{route('specialities.create')}}">{{__('Add')}}</a></li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('specialities.index')}}">{{__('Manage')}}</a></li>

                        </ul>
                    </div>
                </li>
                <!--  recipients nav-->
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#recipients" aria-expanded="false" aria-controls="recipients">
                        <i class="mdi mdi-flask-outline menu-icon"></i>
                        <span class="menu-title">{{__("Recipients - Vertreter")}}</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="recipients">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link" href="{{route('recipients.create')}}">{{__('Add')}}</a></li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('recipients.index')}}">{{__('Manage')}}</a></li>

                        </ul>
                    </div>
                </li>
                <!--  recipients-2nd nav-->
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#recipients-2nd" aria-expanded="false" aria-controls="recipients-2nd">
                        <i class="mdi mdi-flask-outline menu-icon"></i>
                        <span class="menu-title">{{__("Recipients - Interessenten")}}</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="recipients-2nd">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link" href="{{route('recipients-2nd.create')}}">{{__('Add')}}</a></li>
                            <li class="nav-item"> <a class="nav-link" href="{{route('recipients-2nd.index')}}">{{__('Manage')}}</a></li>

                        </ul>
                    </div>
                </li>



            </ul>
        @else
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="/dashboard">
                        <i class="mdi mdi-clipboard-text-outline menu-icon"></i>
                        <span class="menu-title">{{__('My Profile')}}</span>
                    </a>
                </li>
            </ul>
        @endif
    @endguest



</nav>
