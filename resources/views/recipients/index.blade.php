@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-body table-responsive">
            <a class="btn btn-github float-right"
               href="{{route('recipients.create')}}">{{__('Add New Recipient - Vertreter')}}</a>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>
                        {{__("First Name")}}
                    </th>
                    <th>
                        {{__("Last Name")}}
                    </th>
                    <th>
                        {{__("Email")}}
                    </th>

                    <th>
                        {{__("Created At")}}
                    </th>
                    <th>
                        {{__('Actions')}}
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($recipients as $recipient)
                    <tr>
                        <td>
                            {{$recipient->first_name}}
                        </td>
                        <td>
                            {{$recipient->last_name}}
                        </td>
                        <td>
                            {{$recipient->email}}
                        </td>

                        <td>
                            {{$recipient->created_at ? $recipient->created_at->format('d M Y h:m:s') : ''}}
                        </td>
                        <td>
                            <form method="POST" action="{{route('recipients.destroy',['id'=>$recipient->id])}}"
                                  id="id_{{$recipient->id}}">
                                {{csrf_field()}} {{method_field("DELETE")}}
                                <a href="#"
                                   onclick="document.getElementById('id_{{$recipient->id}}').submit()">{{__('Delete')}}</a>
                            </form>

                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="6" class="text-center">
                        {{$recipients->links()}}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('js')

@endsection