@extends('layouts.dashboard')

@section('content')
    @if($errors->count())
        <div class="alert alert-warning">
            {{$errors->first()}}
        </div>

    @endif
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{__('Create New Recipient - Vertreter')}}
                    </div>

                    <div class="card-body">

                        <form action="{{route('recipients.store')}}" method="POST">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="">{{__('First Name')}}</label>
                                        <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="">{{__('Last Name')}}</label>
                                        <input type="text" class="form-control" name="last_name" value="{{old('last_name')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <label for="">{{__('Email')}}</label>
                                        <input type="text" class="form-control" name="email" value="{{old('email')}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-github float-right">{{__('Save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection