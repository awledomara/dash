@extends('layouts.dashboard')
@section('head')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/af-2.3.3/b-1.5.6/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('practices.update',['id'=>$practice->id])}}" method="POST" autocomplete="off">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <h4 class="card-title">{{__('Add New Practice Account')}}</h4>
                        @if($errors->count())
                            <div class="alert alert-warning">
                                {{$errors->first()}}
                            </div>

                        @endif
                        <ul class="nav nav-tabs " role="tablist">
                            <li class="nav-item">
                                <a class="nav-link " id="home-tab" data-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">
                                    {{__('General Information')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact-1" role="tab" aria-controls="contact-1" aria-selected="false">
                                    {{__('Contact Information')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="address-tab" data-toggle="tab" href="#address-1" role="tab" aria-controls="address-1" aria-selected="false">
                                    {{__('Addresses')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#mcc" class="nav-link" id="mcc-tab" data-toggle="tab"role="tab" aria-controls="mcc" aria-selected="false">
                                    MVZ-Daten
                                </a>
                            </li>
                            <!--
                            <li class="nav-item">
                                <a class="nav-link" id="contact-person-tab" data-toggle="tab" href="#contact-person" role="tab" aria-controls="contact-person-1" aria-selected="false">
                                    {{__('Contact Person')}}
                                </a>
                            </li>
                            -->

                            <li class="nav-item">
                                <a class="nav-link" id="financial-tab" data-toggle="tab" href="#financial-1" role="tab" aria-controls="contact-1" aria-selected="false">
                                    {{__('Financial Information')}}
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="docs-tab" data-toggle="tab" href="#docs-1" role="tab" aria-controls="contact-1" aria-selected="false">
                                    {{__('Related Documents')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="jobs-tab-link" data-toggle="tab" href="#jobs-tab" role="tab" aria-controls="jobs-1" aria-selected="false">

                                    {{__('Jobs of this Practice')}}
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                               <div class="row">
                                   <div class="col-md-8">
                                       <div class="row">
                                           <div class="col">
                                               <div class="form-group">
                                                   <label for="id">ID</label>
                                                   {{$practice->id}}
                                               </div>
                                           </div>
                                       </div>
                                       <div class="row">
                                           <div class="col-md-2 col-sm-4">
                                               <div class="form-group">
                                                   <label for="title">{{__("Title")}}</label>
                                                   <select name="title" id="title" class="form-control">
                                                       <option value="Herr"  {{$practice->title=='Herr'?'selected':''}}>Herr</option>
                                                       <option value="Frau"  {{$practice->title=='Frau'?'selected':''}}>Frau</option>
                                                   </select>
                                               </div>
                                           </div>
                                           <div class="col-md-2 col-sm-4">
                                               <div class="form-group">
                                                   <label for="grade">{{__("Grade")}}</label>
                                                   <select name="grade" id="grade" class="form-control">

                                                       @foreach($grades as $grade)

                                                           <option value="{{$grade->name}}" {{$practice->grade==$grade->name?'selected':''}}>{{$grade->name}}</option>
                                                       @endforeach
                                                   </select>

                                               </div>
                                           </div>
                                       </div>
                                       <div class="row">
                                           <div class="col-md-4 col-xs-12">
                                               <div class="form-group">
                                                   <label for="first_name ">{{__('First Name')}}</label>
                                                   <input class="form-control  @error('first_name') is-invalid @enderror"
                                                          type="text" name="first_name"
                                                          value="{{$practice->first_name}}"
                                                   >
                                                   @error('first_name')
                                                   <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                   @enderror
                                               </div>
                                           </div>
                                           <div class="col-md-4 col-xs-12">
                                               <div class="form-group ">
                                                   <label for="last_name">{{__('Last Name')}}</label>
                                                   <input class="form-control @error('last_name') is-invalid @enderror"
                                                          type="text" name="last_name"
                                                          value="{{$practice->last_name}}"
                                                   >
                                                   @error('last_name')
                                                   <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                   @enderror
                                               </div>
                                           </div>
                                       </div>
                                       <div class="row">
                                           <div class="col-md-8">
                                               <div class="form-group">
                                                   <label for="speciality">{{__("Speciality")}}</label>
                                                   <select name="speciality_id" id="speciality" class="form-control">
                                                       <option value=""></option>
                                                       @foreach($specialities as $speciality)
                                                           <option value="{{$speciality->id}}"  {{$practice->speciality_id==$speciality->id?'selected':''}}>{{$speciality->name}}</option>
                                                       @endforeach
                                                   </select>
                                               </div>
                                           </div>

                                       </div>
                                       <div class="row">
                                           <div class="col">
                                               <div class="form-group">
                                                   <label for="notes">{{__('Notes')}}</label>
                                                   <textarea type="text" class="form-control" name="notes" rows="10">{{$practice->notes}}</textarea>
                                               </div>
                                           </div>


                                       </div>
                                   </div>
                                   <div class="col-md-4">
                                            <list-services l-services="{{json_encode($services)}}" selected="{{$practice->service_areas}}"></list-services>
                                   </div>
                               </div>
                            </div>
                            <div class="tab-pane fade" id="mcc" role="tabpanel" aria-labelledby="mcc-daten-tab">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" name="in_mvz"
                                                           id="in_mvz"
                                                           class="form-check-input"
                                                            {{$practice->in_mvz==1?'checked':''}}
                                                    >
                                                    {{__('Praxis in einem MVZ')}}
                                                    <i class="input-helper"></i></label>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name2">Name</label>
                                            <input type="text"
                                                   class="form-control   @error('mcc_name') is-invalid @enderror"
                                                   name="mcc_name"
                                                   value="{{optional($practice->mcc)->mcc_name}}">
                                            @error('mcc_name')
                                                <span class="invalid-feedback" role="alert">
                                                     <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name2">{{__('Contact Name')}}</label>
                                            <input type="text" class="form-control   @error('mcc_contact_name') is-invalid @enderror"
                                                   name="mcc_contact_name"
                                                   value="{{optional($practice->mcc)->mcc_contact_name}}">
                                            @error('mcc_contact_name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name2">{{__('Email')}}</label>
                                            <input type="text" class="form-control   @error('mcc_email') is-invalid @enderror" name="mcc_email" value="{{optional($practice->mcc)->mcc_email}}">
                                            @error('mcc_email')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name2">{{__('Fax')}}</label>
                                            <input type="text" class="form-control   @error('mcc_fax') is-invalid @enderror" name="mcc_fax" value="{{optional($practice->mcc)->mcc_fax}}">
                                            @error('mcc_fax')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name2">{{__('street number')}}</label>
                                            <input type="text" class="form-control   @error('mcc_contact_name') is-invalid @enderror"
                                                   name="mcc_street_number"
                                                   value="{{optional($practice->mcc)->mcc_street_number}}">
                                            @error('mcc_street_number')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name2">{{__('City')}}</label>
                                            <input type="text" class="form-control   @error('mcc_city') is-invalid @enderror"
                                                   name="mcc_city"
                                                   value="{{optional($practice->mcc)->mcc_city}}">
                                            @error('mcc_city')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name2">{{__('Post Office')}}</label>
                                            <input type="text" class="form-control   @error('mcc_post_office') is-invalid @enderror" name="mcc_post_office"
                                                   value="{{optional($practice->mcc)->mcc_post_office}}">
                                            @error('mcc_post_office')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="name2">Phone</label>
                                            <input type="text" class="form-control   @error('mcc_phone') is-invalid @enderror" name="mcc_phone"
                                                   value="{{optional($practice->mcc)->mcc_phone}}">
                                            @error('mcc_phone')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--
                            <div class="tab-pane fade" id="contact-person" role="tabpanel" aria-labelledby="address-tab">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="title">{{__("Title")}}</label>
                                            <select name="title" id="title" class="form-control">
                                                <option value="Herr"  {{$practice->title=='Herr'?'selected':''}}>Herr</option>
                                                <option value="Frau"  {{$practice->title=='Frau'?'selected':''}}>Frau</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="grade">{{__("Grade")}}</label>
                                            <select name="grade" id="grade" class="form-control">

                                                @foreach($grades as $grade)

                                                    <option value="{{$grade->name}}" {{$practice->grade==$grade->name?'selected':''}}>{{$grade->name}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="first_name ">{{__('First Name')}}</label>
                                            <input class="form-control  @error('first_name') is-invalid @enderror"
                                                   type="text" name="first_name"
                                                   value="{{$practice->first_name}}"
                                            >
                                            @error('first_name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group ">
                                            <label for="last_name">{{__('Last Name')}}</label>
                                            <input class="form-control @error('last_name') is-invalid @enderror"
                                                   type="text" name="last_name"
                                                   value="{{$practice->last_name}}"
                                            >
                                            @error('last_name')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="phone">{{__('Phone')}}</label>
                                            <input class="form-control @error('phone') is-invalid @enderror"
                                                   type="text"
                                                   name="phone"
                                                   value="{{$practice->phone}}"

                                            >
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="person_email">{{__('Email')}}</label>
                                            <input class="form-control @error('person_email') is-invalid @enderror" type="text"
                                                   name="email"
                                                   value="{{$practice->email}}"
                                            >
                                            @error('person_email')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            -->
                            <div class="tab-pane fade" id="contact-1" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="first_name">{{__('Primary Email')}}*</label>
                                            <input class="form-control  @error('primary_email') is-invalid @enderror" type="text" value="{{$practice->primary_email}}" name="primary_email">
                                            @error('primary_email')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">

                                        <div class="form-group">
                                            <label for="last_name">{{__('Secondary Email')}}({{__('optional')}})</label>
                                            <input class="form-control @error('secondary_email') is-invalid @enderror"
                                                   type="text" name="secondary_email"
                                                   value="{{$practice->secondary_email}}">
                                            @error('secondary_email')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="phone">{{__('Primary Phone')}}*</label>
                                            <input class="form-control @error('primary_phone') is-invalid @enderror"
                                                   type="text" name="primary_phone"
                                                   value="{{$practice->primary_phone}}" >
                                            @error('primary_phone')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="last_name">{{__('Secondary Phone')}}({{__('optional')}})</label>
                                            <input class="form-control  @error('secondary_phone') is-invalid @enderror" type="text" name="secondary_phone"
                                                   value="{{$practice->secondary_phone}}" >
                                            @error('secondary_phone')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="fax">{{__('Fax')}}</label>
                                            <input class="form-control  @error('fax') is-invalid @enderror" type="text" value="{{$practice->fax}}">
                                            @error('fax')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="financial-1" role="tabpanel" aria-labelledby="financial-1-tab">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="fee">Monthly Fee</label>
                                            <input type="number" class="form-control @error('monthly_fee') is-invalid @enderror" name="monthly_fee" value="{{$practice->monthly_fee}}">
                                            @error('monthly_fee')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="pay_period">Pay Period</label>
                                            <select name="pay_period" class="form-control @error('monthly_fee') is-invalid @enderror">
                                                <option value="m" {{$practice->pay_period!="m"?'selected':''}}>Monthly</option>
                                                <option value="q" {{$practice->pay_period!="q"?'selected':''}}>Quarterly</option>
                                            </select>
                                            @error('pay_period')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="iban">IBAN</label>
                                            <input type="text" class="form-control @error('iban') is-invalid @enderror" name="iban" value="{{$practice->iban}}">
                                            @error('iban')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="owner">Owner</label>
                                            <input type="text" class="form-control  @error('owner') is-invalid @enderror" name="owner" value="{{$practice->owner}}">
                                            @error('owner')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input type="checkbox" name="debit_authorization" class="form-check-input" {{$practice->debit_authorization==1?'checked':''}}>
                                                    {{__('Debit Authorization')}}
                                                    <i class="input-helper"></i></label>
                                            </div>

                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="tab-pane fade" id="address-1" role="tabpanel" aria-labelledby="address-tab">

                                <addresses
                                        list="{{json_encode($practice->addresses)}}"
                                        host="/api/practices/{{$practice->id}}"
                                >

                                </addresses>
                            </div>
                            <div class="tab-pane fade" id="docs-1" role="tabpanel" aria-labelledby="docs-tab">

                                <files-zone host="/api/practices/{{$practice->id}}" files="{{json_encode($practice->documents)}}"></files-zone>

                            </div>
                            <div class="tab-pane fade" id="jobs-tab" role="tabpanel" aria-labelledby="jobs-tab-link">

                                    <table id="data-table-jobs" class="table table-striped" style="width: 100%">
                                        <thead>
                                           <tr>
                                               <th>
                                                   {{__("From")}}
                                               </th>
                                               <th>
                                                   {{__("To")}}
                                               </th>
                                               <th>
                                                   {{__('Service Area')}}
                                               </th>
                                               <th>
                                                   {{__('Verified')}}
                                               </th>
                                               <th>
                                                   {{__('Created_at')}}
                                               </th>
                                               <th>

                                               </th>
                                           </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 grid-margin">
                               <div class="card">
                                   <div class="card-body">
                                       <button type="submit" class="btn btn-github float-right">Save</button>
                                   </div>
                               </div>

                            </div>
                        </div>


                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/af-2.3.3/b-1.5.6/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>
    <!--
   <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
 -->
    <script>
        $(document).ready( function () {


            $('#data-table-jobs').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('datatable.practices.jobs',$practice) }}",
                "columns": [
                    {   "data": "start_date" ,
                        'name':"start_date",
                        "render": function(d) {

                            return moment(d).format("DD.MM.YYYY H:m")

                        }

                    },
                    {   "data": "end_date" ,
                        'name':"end_date",
                        "render": function(d) {

                            return moment(d).format("DD.MM.YYYY H:m")

                        }

                    },
                    {   "data": "service_area" ,
                        'name':"service_area.name",
                        "defaultContent":"Not Set",
                        "render": function(d) {

                            if(d!=null){
                                return  d.name
                            }

                        }

                    },
                    {   "data": "verified" ,
                        'name':"verified",
                        "render": function(d) {

                            if(d!=null){


                                if(d==0){
                                    return '<span class="mdi mdi-checkbox-blank-circle"></span>No'
                                }
                                return '<span class="mdi mdi-check-circle"></span>yes'
                            }

                        }

                    },
                    {
                        "data":"created_at",
                        "render":function (d) {
                            return moment(d).format("DD.MM.YYYY H:m")
                        }
                    },
                    {
                        "data":null,
                        "name":"id",
                        "render":function (d) {

                            return '<a href="{{route('jobs.index')}}/'+d.id+'">{{__('Details')}}</a>';
                        }
                    }


                ]
            });
        });
    </script>

    <script type="text/javascript">

        $(document).ready(function(){

            function mvz_fileds(){
                $("#mcc input").each((index,item)=>{

                    if(!document.getElementById("in_mvz").checked){
                        if(item.name!="in_mvz"){
                            item.setAttribute('disabled',true)
                        }
                    }


                })
            }
            mvz_fileds()

            $('#mcc').on('click','#in_mvz', function (event) {
                if(this.checked){
                    $("#mcc input").each((index,item)=>{

                        item.removeAttribute('disabled')
                    })
                }else{
                    $("#mcc input").each((index,item)=>{
                        if(item.name!="in_mvz"){
                            item.setAttribute('disabled',true)
                        }

                    })
                }

            })
        })
    </script>

@endsection
