@extends('layouts.dashboard')
@section('head')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/af-2.3.3/b-1.5.6/sc-2.0.0/sl-1.3.0/datatables.min.css"/>
@endsection
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body table-responsive">
                <h4 class="card-title" >{{__('List of Practices')}}</h4>
                <a class="btn btn-light float-right btn-sm" href="{{route('practices.create')}}">{{__('Add New Practice')}}</a>

                    <table class="table table-striped" width="100%" id="data-table">
                        <thead>
                        <tr>

                            <th>
                                {{__("Practice Name")}}
                            </th>
                            <th>
                                {{__("Speciality")}}
                            </th>

                            <th>
                                {{__("Emails")}}
                            </th>
                            <th>
                                {{__('Phones')}}
                            </th>
                            <th>
                                {{__('Created At')}}
                            </th>
                            <th>

                            </th>

                        </tr>
                        </thead>
                        <tbody>


                        </tbody>
                    </table>



            </div>
        </div>
    </div>
</div>


@endsection
@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/af-2.3.3/b-1.5.6/sc-2.0.0/sl-1.3.0/datatables.min.js"></script>
    <!--
   <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
 -->
    <script>
        $(document).ready( function () {
            $('#data-table').DataTable({
                "processing": true,
                "serverSide": true,
                "searching": false,
                "ajax": "{{ route('datatable.practices.index') }}",
                "columns": [
                    {   "data": "name" ,
                        'name':"first_name",
                        "render": function(data, type, full) {


                            return full['first_name'] + ' ' + full['last_name'];
                        }

                    },

                    {   "data": "speciality" ,
                        'name':"speciality.name",
                        "defaultContent":"<i class='text-google'>Not Set</i>",
                        "render": function(d) {

                            if(d!=null){
                                return  d.name
                            }

                        }

                    },
                    {   "data": "email" ,
                        'name':"primary_email",
                        "render": function(data, type, full) {
                            return full['primary_email'] + '<br> ' + full['secondary_email'];
                        }

                    },
                    {   "data": "phone" ,
                        'name':"primary_phone",
                        "render": function(data, type, full) {
                            return full['primary_phone'] + '<br> ' + full['secondary_phone'];
                        }

                    },

                    {
                        "data":"created_at",
                        "render":function (d) {
                            return moment(d).format("DD.MM.YYYY H:m")
                        }
                    },
                    {
                        "data":null,
                        "name":"id",
                        "render":function (d) {
                          var buttons='<a href="{{route('practices.index')}}/'+d.id+'/edit">{{__('Edit')}}</a> &nbsp;';

                          return buttons+='<a href="{{route('practices.index')}}/'+d.id+'">{{__('Details')}}</a>';
                        }
                    }


                ]
            });
        });
    </script>
@endsection
