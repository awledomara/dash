@extends('layouts.dashboard')
@section('head')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
<style>
    .select2-results__option[aria-selected="true"] {color:gray !important;}


</style>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-header">
                    {{__('Doctors Notifications')}}
                </div>
                <div class="card-body">
                    @if($errors->count())
                        <div class="alert alert-warning">
                            {{$errors->first()}}
                        </div>

                    @endif
                    <form action="{{route('doctors.notifications.send')}}" method="post">
                        {{csrf_field()}}
                    <h4 class="card-title">{{__('Doctors:')}}</h4>
                    - All (Don't add any record in the list blow) <br>
                    <div class="row">
                        <div class="col-md-1">
                            - Exclusively:
                        </div>
                        <div class="col-md-3">
                            <select id="doctor_id" name="doctors[]" class="form-control"></select>
                        </div>
                    </div>
                    <h4 class="card-title">{{__('Jobs:')}}</h4>
                    <div class="row">
                        <div class="col-md-8">
                            <select id="job_id" name="jobs[]" class="form-control"></select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-github float-right">{{__('Send')}}</button>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

    <script>
        $(document).ready(function () {

            $('#doctor_id').select2({
                theme: "classic",
                multiple: true,
                allowClear: true,

                ajax: {
                    url: '{{route('microservices.doctors.search')}}',
                    data: function (params) {
                        var query = {
                            q: params.term,
                        }
                        return query;
                    },
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.first_name + ' ' + item.last_name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });

            $('#job_id').select2({
                theme: "classic",
                multiple: true,
                allowClear: true,
                closeOnSelect:false,
                hideSelected:true,
                ajax: {
                    url: '{{route('microservices.jobs.search')}}',
                    data: function (params) {
                        var query = {
                            q: params.term,
                        }
                        return query;
                    },
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        return {
                            results: $.map(data, function (item) {
                                let dr = '', practice = '';
                                if (item.doctor_first_name != null)
                                    dr = 'Doctor: ' +
                                        item.doctor_first_name + ' ' +
                                        item.doctor_last_name + ' - ';
                                return {
                                    text: dr + 'Practice: ' +
                                        (item.practice_name != null ? item.practice_name + ' - ' : '') +
                                        item.practice_first_name + ' ' +
                                        item.practice_last_name + ' - ServiceArea: ' +
                                        item.service_area_name + ' - StartDate: ' +
                                        item.start_date,
                                    id: item.id
                                }
                            })
                        };
                    },
                    dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });

        });
    </script>
@endsection
