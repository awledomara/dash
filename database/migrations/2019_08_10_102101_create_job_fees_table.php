<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\JobFee;

class CreateJobFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_fees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('from');
            $table->decimal('per_hour')->default(0.0);

            $table->unsignedSmallInteger('status')->default(JobFee::PENDING);
            $table->unsignedBigInteger("job_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_fees');
    }
}
