<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalCareCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_care_centers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("mcc_name")->nullable();
            $table->string('mcc_contact_name')->nullable();
            $table->string('mcc_phone')->nullable();
            $table->string('mcc_street_number')->nullable();
            $table->string('mcc_post_office')->nullable();
            $table->string('mcc_city')->nullable();
            $table->string("mcc_email")->nullable();
            $table->string('mcc_fax')->nullable();
            $table->unsignedBigInteger('practice_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_care_centers');
    }
}
