<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableServiceArea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_areas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedSmallInteger('type')->default(0);//1:Driving Service, 2: Surgery Service
            $table->string("name");
            $table->string("designation")->nullable();
            $table->string("internal_name")->nullable();
            $table->string("internal_name_lst")->nullable();

            $table->mediumText("notes")->nullable();
            $table->mediumText("phone")->nullable();

            $table->string("contact_person")->nullable();
            $table->string("phone_contact_person")->nullable();



            $table->string("email_lst")->nullable();
            $table->string("phone_lst")->nullable();
            $table->string("fax_lst")->nullable();

            $table->string("email_kv")->nullable();
            $table->string("fax_kv")->nullable();
            $table->string("phone_kv")->nullable();

            $table->string("post_code")->nullable();
            $table->softDeletes();

           // $table->unsignedBigInteger('address_id')->nullable();
            //$table->unsignedBigInteger('contact_person_id')->nullable();//references table people

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_areas');
    }
}
