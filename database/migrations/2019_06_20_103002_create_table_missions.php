<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->longText("time_frames")->nullable();// json string contains objects
            $table->mediumText("tags")->nullable();
            $table->mediumText("notes")->nullable();

            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();

            $table->decimal("additional_remuneration")->nullable();
            $table->string("ab")->nullable();
            $table->boolean("verified")->default(0);
            $table->dateTime("verified_at")->nullable();
            $table->softDeletes();

            $table->unsignedBigInteger("service_area_id")->nullable();
            $table->unsignedBigInteger("practice_id")->nullable();
            $table->unsignedBigInteger("created_by")->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('missions');
    }
}
