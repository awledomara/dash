<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSentBulkEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sent_bulk_emails', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('form');
            $table->unsignedInteger('template_id')->nullable(true);
            $table->string('subject',1000);
            $table->text('recipients');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sent_bulk_emails');
    }
}
