<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->mediumText('title');
            $table->mediumText('description')->nullable();
            $table->dateTimeTz('start')->default(\Carbon\Carbon::now());
            $table->boolean('allDay')->default(false);
            $table->boolean('editable')->default(true);
            $table->string('color')->default("#7571f9");

            $table->string('backgroundColor')->default("#7571f9");
            $table->string('textColor')->default("#fff");
            $table->dateTimeTz('end')->default(\Carbon\Carbon::now());
            $table->unsignedBigInteger("mission_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_events');
    }
}
