<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHotelServiceAreas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_service_areas', function (Blueprint $table) {

            $table->unsignedBigInteger('hotel_id');
            $table->unsignedBigInteger('service_area_id');
            $table->primary(['hotel_id','service_area_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_service_areas');
    }
}
