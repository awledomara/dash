<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDoctors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("title")->nullable();//Herr/Frau
            $table->string("grade")->nullable();//Dr., DM
            $table->string("secondary_email")->nullable();
            $table->string("phone1");
            $table->string('phone2')->nullable();
            $table->mediumText('notes')->nullable();
            $table->date('approval_date')->nullable();

            $table->softDeletes();

            $table->unsignedBigInteger('facility_id')->nullable();
            $table->unsignedBigInteger('speciality_id')->nullable();
            $table->unsignedBigInteger('service_area_id')->nullable();

            $table->unsignedBigInteger("address_id")->nullable();
            $table->unsignedBigInteger("user_id")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
