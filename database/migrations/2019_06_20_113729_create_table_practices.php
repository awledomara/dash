<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePractices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("name")->nullable();

            $table->string("primary_email")->nullable();
            $table->string("secondary_email")->nullable();
            $table->string("fax")->nullable();
            $table->string("primary_phone")->nullable();
            $table->string("secondary_phone")->nullable();

            $table->string("first_name")->nullable();
            $table->string("last_name")->nullable();
            $table->string("title")->nullable();
            $table->string("grade")->nullable();

            $table->boolean('in_mvz')->default(false);
            $table->string("pay_period")->nullable();//1:Monthly,2:quarterly
            $table->decimal("monthly_fee")->default(0);
            $table->string("iban")->nullable();
            $table->mediumText("notes")->nullable();
            $table->string("owner")->nullable();//TODO: need some clarifications
            $table->boolean("debit_authorization")->default(0);
            $table->softDeletes();

            $table->boolean("contract_uploaded")->default(0);
            $table->date("date_contact_begin")->nullable();

            $table->unsignedBigInteger("speciality_id")->nullable();
            $table->unsignedBigInteger("facility_id")->nullable();
            $table->unsignedBigInteger("service_area_id")->nullable();
         //   $table->unsignedBigInteger("contact_person_id")->nullable();//references people

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('practices');
    }
}
