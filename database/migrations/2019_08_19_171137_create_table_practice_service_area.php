<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePracticeServiceArea extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('practice_service_areas');
        Schema::create('practice_service_areas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("practice_id");
            $table->unsignedBigInteger("service_area_id");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('practice_service_areas');
    }
}
