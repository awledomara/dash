<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemplateIdFieldToEmailTemplateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_template_files', function (Blueprint $table) {
            $table->bigInteger('email_template_id');
            $table->string('original_name',1000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_template_files', function (Blueprint $table) {
            $table->dropColumn('email_template_id');
            $table->dropColumn('original_name');
        });
    }
}
