<?php

use App\Models\Speciality;
use Illuminate\Database\Seeder;

class SpecialitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sps=[
            'Allergist or Immunologist ','Anesthesiologist','Cardiologist','Dermatologist','Neurologist','Obstetrician'
        ];
        foreach ($sps as $sp){
            Speciality::create([
                'name'=>$sp
            ]);
        }
    }
}
