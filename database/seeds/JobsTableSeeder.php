<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker\Factory::create();
        $verified=true;
        foreach (range(0,2) as $i){

            $m=random_int(1,12);
            $d=random_int(1,20);
            $minute=random_int(0,30);
            $hour=random_int(0,24);
            $hour=random_int(0,24);
            $start_date=\Carbon\Carbon::create(2019,$m,$d,$hour,$minute);
            $end_date=\Carbon\Carbon::create(2019,$m,$d,$hour,$minute)->addHours(random_int(2,48));
            $practice=\App\Models\Practice::inRandomOrder()->first();
            $status=$faker->randomElement([\App\Models\Job::Assigned,\App\Models\Job::PENDING,\App\Models\Job::Verified]);

            \App\Models\Job::create([
                "start_date"=>$start_date,
                "end_date"=>$end_date,
                "practice_id"=>$practice->id,
                "service_area_id"=>$practice->service_areas()->first()->id,
                "verified"=>$status==\App\Models\Job::Verified?1:0,
                "verified_at"=>$status==\App\Models\Job::Verified?\Carbon\Carbon::now():null,
                "status"=>$status,
                "doctor_id"=>$status==\App\Models\Job::Assigned?\App\Models\Doctor::inRandomOrder()->first()->id:null,
                "notes"=>$faker->paragraph(3),
                "tags"=>[],
                "time_frames"=>[
                    [
                    "from"=>$start_date,
                    "to"=>$end_date,
                    "notes"=>$faker->paragraph(3),
                    "per_hour"=>random_int(20,30)
                     ],
                    [
                        "from"=>$start_date,
                        "to"=>$end_date,
                        "notes"=>$faker->paragraph(3),
                        "per_hour"=>random_int(20,30)
                    ]]

            ]);
            $verified=!$verified;
        }
    }
}
