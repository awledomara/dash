<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class RecipientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker\Factory::create();
        foreach (range(0,5) as $i) {
            \App\Models\Recipient::create([
                'email'=>$faker->safeEmail,
                'first_name'=>$faker->firstName,
                'last_name'=>$faker->lastName
            ]);
        }
    }
}
