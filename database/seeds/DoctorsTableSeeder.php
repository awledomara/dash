<?php

use App\Models\Doctor;
use App\Models\Facility;
use App\Models\ServiceArea;
use App\Models\Speciality;
use Illuminate\Database\Seeder;

class DoctorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $faker=Faker\Factory::create();

foreach (range(1,1) as $index) {

    $user = \App\User::create([
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'date_of_birth' => \Carbon\Carbon::now()->subYears(random_int(24, 50)),
        'password' => bcrypt("password")
    ]);

    $user->assignRole('Doctor');

    $address=\App\Models\Address::create([
        'company_name'=>$faker->company,
        'street'=>$faker->streetAddress,
        'postal_code'=>$faker->postcode,
        'city'=>$faker->city,
        'country'=>$faker->country
    ]);
    $doctor = Doctor::create([
        'title' => $faker->randomElement(['Herr', 'Frau']),
        'grade' => $faker->randomElement(['Dr.', 'DM']),
        'secondary_email' => $faker->email,
        'phone1' => $faker->tollFreePhoneNumber,
        'phone2' => $faker->tollFreePhoneNumber,
        'notes' => $faker->paragraph(3),
        'approval_date' => null,
        "address_id"=>$address->id,
        'facility_id' => Facility::inRandomOrder()->first()->id,
        'service_area_id' => ServiceArea::inRandomOrder()->first()->id,
        'speciality_id' => Speciality::inRandomOrder()->first()->id,
        'user_id' => $user->id
    ]);
    /* foreach (range(0,2) as $r){
         $address=\App\Models\Address::create([
             'company_name'=>$faker->company,
             'street'=>$faker->streetAddress,
             'postal_code'=>$faker->postcode,
             'city'=>$faker->city,
             'country'=>$faker->country
         ]);
         \App\Models\DoctorAddress::create([
             'doctor_id'=>$doctor->id,
             'address_id'=>$address->id
         ]);
         $document=\App\Models\Document::create([
             'title'=>$faker->sentence(4),
             'description'=>$faker->paragraph(3),
             'path'=>$faker->image(),
             'uploaded_by'=>\App\User::all()->first()->id
         ]);
         \App\Models\DoctorDocument::create([
             'doctor_id'=>$doctor->id,
             'document_id'=>$document->id
         ]);

     }
    */

}

    }
}
