<?php

use Illuminate\Database\Seeder;

class JobApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Models\Doctor::all() as $doctor){
            foreach (\App\Models\Job::all() as $job) {
                \App\Models\JobApplication::create([
                    "doctor_id" => $doctor->id,
                    "mission_id" => $job->id,
                ]);
            }
        }
    }
}
