<?php

use Illuminate\Database\Seeder;

class SuperAdminTableseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $faker=Faker\Factory::create();

       $user=\App\User::create([
           'first_name'=>$faker->firstName,
           'last_name'=>$faker->lastName,
           'email'=>"admin@consultmedic.com",
           'date_of_birth'=>\Carbon\Carbon::now()->subYears(45),
           'password'=>bcrypt("admin")
       ]);

       $user->assignRole('SuperAdmin');


    }
}
