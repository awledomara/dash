<?php

use Illuminate\Database\Seeder;

class PracticesTableseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker\Factory::create();

        foreach (range(1,1) as $index){
            $practice=\App\Models\Practice::create([
                'name'=>$faker->company,
                'first_name'=>$faker->firstName,
                'last_name'=>$faker->lastName,
                'primary_email'=>$faker->companyEmail,
                'secondary_email'=>$faker->companyEmail,
                'pay_period'=>$faker->randomElement(['m','q']),
                'monthly_fee'=>$faker->randomFloat(3,10,200),
                'iban'=>$faker->randomNumber(7),
                'secondary_phone'=>$faker->tollFreePhoneNumber,
                'primary_phone'=>$faker->tollFreePhoneNumber,
                'fax'=>$faker->tollFreePhoneNumber,
                'debit_authorization'=>random_int(0,1),
                'notes'=>$faker->paragraph(3),
                'owner'=>$faker->firstName,
                'speciality_id'=>\App\Models\Speciality::inRandomOrder()->first()->id,
                'service_area_id'=>\App\Models\ServiceArea::inRandomOrder()->first()->id,
                'facility_id'=>\App\Models\Facility::inRandomOrder()->first()->id,

            ]);
            foreach (range(0,2) as $r){
                $address=\App\Models\Address::create([
                    'company_name'=>$faker->company,
                    'street'=>$faker->streetAddress,
                    'postal_code'=>$faker->postcode,
                    'city'=>$faker->city,
                    'country'=>$faker->country
                ]);
                \App\Models\PracticeAddress::create([
                    'practice_id'=>$practice->id,
                    'address_id'=>$address->id
                ]);
                /*$document=\App\Models\Document::create([
                    'title'=>$faker->sentence(4),
                    'description'=>$faker->paragraph(3),
                    'path'=>$faker->image(),
                    'uploaded_by'=>\App\User::all()->first()->id
                ]);
                \App\Models\PracticeDocument::create([
                    'practice_id'=>$practice->id,
                    'document_id'=>$document->id
                ]);
                */
            }
        }








    }
}
