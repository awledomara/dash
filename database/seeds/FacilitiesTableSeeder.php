<?php

use App\Models\Facility;
use Illuminate\Database\Seeder;

class FacilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sps=[
            'Birth centers'
        ];
        foreach ($sps as $sp){
            Facility::create([
                'name'=>$sp
            ]);
        }
    }
}
