<?php

use Illuminate\Database\Seeder;

class HotelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker\Factory::create();
        foreach (range(0,1) as $i){


            $hotel=\App\Models\Hotel::create([
                'website'=>$faker->url,
                'email'=>$faker->companyEmail,
                'name'=>$faker->companyEmail,
                'fax'=>$faker->tollFreePhoneNumber,
                'primary_phone'=>$faker->tollFreePhoneNumber,
                'secondary_phone'=>$faker->tollFreePhoneNumber,
            ]);


            foreach (range(0,random_int(2,5) ) as $c){

                $address=\App\Models\Address::create([
                    'company_name'=>$faker->company,
                    'street'=>$faker->streetAddress,
                    'postal_code'=>$faker->postcode,
                    'city'=>$faker->city,
                    'country'=>$faker->country
                ]);
               \App\Models\HotelAddress::create([
                   "address_id"=>$address->id,
                   "hotel_id"=>$hotel->id
               ]);
               try{
                   \Illuminate\Support\Facades\DB::transaction(function() use($hotel){
                       \App\Models\HotelServiceArea::create([
                           'service_area_id'=>\App\Models\ServiceArea::inRandomOrder()->first()->id,
                           'hotel_id'=>$hotel->id
                       ]);
                   });

               }catch (Exception $e){

               }

            }



        }

    }
}
