<?php

use Illuminate\Database\Seeder;

class ServiceAreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = [
            "South Australian Ambulance Service",
           ];

        $faker=Faker\Factory::create();
        foreach (range(1,1) as $area){
            $type=random_int(0,2);

           /* $address=\App\Models\Address::create([
                'company_name'=>$faker->company,
                'street'=>$faker->streetAddress,
                'postal_code'=>$faker->postcode,
                'city'=>$faker->city,
                'country'=>$faker->country
            ]);
           */
           /* $person=\App\Models\Person::create([
                'first_name'=>$faker->firstName,
                'last_name'=>$faker->lastName,
                'email'=>$faker->companyEmail,
                'phone'=>$faker->tollFreePhoneNumber     ,
                'fax'=>$faker->tollFreePhoneNumber     
            ]);*/
           \App\Models\ServiceArea::create([
               "name"=>$faker->company,
               "designation"=>$faker->company,
               "internal_name"=>$faker->company,
               "internal_name_lst"=>$faker->company,
               "type"=>0,
               "notes"=>($type==1)?$faker->paragraph(2):null,
               'phone'=>$faker->tollFreePhoneNumber     ,
               'phone_contact_person'=>$faker->tollFreePhoneNumber     ,
               'contact_person'=>$faker->firstName,
               'email_lst'=>$faker->companyEmail,
               'phone_lst'=>$faker->tollFreePhoneNumber     ,
               'fax_lst'=>$faker->tollFreePhoneNumber     ,
               'email_kv'=>$faker->companyEmail,
               'phone_kv'=>$faker->tollFreePhoneNumber     ,
               'fax_kv'=>$faker->tollFreePhoneNumber     ,
               'post_code'=>$faker->postcode,
               //"address_id"=>$address->id
             //  "contact_person_id"=>$person->id
           ]) ;
        }
    }
}
