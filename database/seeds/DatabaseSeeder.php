<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

       $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(SuperAdminTableseeder::class);

        $this->call(GradesTableSeeder::class);
        $this->call(TagsTableSeeder::class);

        $this->call(SpecialitiesTableSeeder::class);
        $this->call(FacilitiesTableSeeder::class);
        $this->call(ServiceAreaTableSeeder::class);
        $this->call(PracticesTableseeder::class);
        $this->call(PracticeServiceAreasSeeder::class);

        $this->call(DoctorsTableSeeder::class);
        $this->call(HotelsTableSeeder::class);

        $this->call(JobsTableSeeder::class);

        $this->call(JobApplicationSeeder::class);

        $this->call(AddressBookSeeder::class);

        $this->call(EmailTemplateTableSeeder::class);
        $this->call(RecipientsTableSeeder::class);


    }
}
