<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class PracticeServiceAreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker\Factory::create();

        foreach (\App\Models\Practice::all() as $practice){

            foreach (\App\Models\ServiceArea::take(random_int(random_int(0,20),random_int(30,100)))->get() as $service){

                \App\Models\PracticeServiceArea::create([
                        "practice_id"=>$practice->id,
                        "service_area_id"=>$service->id,
                ]);

            }

        }
    }
}
