<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class AddressBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Faker\Factory::create();

        foreach (range(1,100) as $index){
            \App\Models\AddressBook::create([
                'content'=>$faker->paragraph(2)
            ]);
        }
    }
}
