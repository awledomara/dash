<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Permission::create([
            'name'=>"Manage Doctors"
        ]);
        \Spatie\Permission\Models\Permission::create([
            'name'=>"Manage Practices"
        ]);
        \Spatie\Permission\Models\Permission::create([
            'name'=>"Assign Jobs To Doctors"
        ]);
        \Spatie\Permission\Models\Permission::create([
            'name'=>"Upload Documents"

        ]);

        \Spatie\Permission\Models\Permission::create([
            'name'=>"Manage Users"
        ]);
    }
}
