<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class EmailTemplateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker=Faker\Factory::create();
        foreach (range(0,1) as $i) {
            \App\Models\EmailTemplate::create([
                'subject' => $faker->sentence,
                'name'=>$faker->sentence,
                'content'=>$faker->paragraph(4),
               ]);
        }
    }
}
